DROP PROCEDURE [dbo].[GetCommentsForFeed]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCommentsForFeed]
@p_UserId			BIGINT,
@p_FeedId			BIGINT,
@p_IsFlashTag		BIGINT,
@p_PageIndex		INT,
@p_PageSize			SMALLINT,
@p_LastUpdatedTime	DATETIME,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: GetCommentsForFeed 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	BEGIN TRY 	
	DECLARE @UserTable AS TABLE
	(
	UserId BIGINT,
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	UserName VARCHAR(50),
	ProfilePic VARCHAR(50),
	BannerImage VARCHAR(50),
	EmailAddress VARCHAR(50),
	Location NVARCHAR(MAX),
	Id			BIGINT,
	Comment NVARCHAR(MAX),
	CommentDate DATETIME,
	RowID			INT
	)
	

	IF(@p_IsFlashTag = 1)
	INSERT INTO @UserTable
	SELECT U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate, ROW_NUMBER() OVER(ORDER BY C.Id DESC) AS RowID FROM tblFlashTagComments  C 
	JOIN tblUser AS U ON C.UserId = U.Id
	WHERE FlashTagId = @p_FeedId

	ELSE
	INSERT INTO @UserTable
	SELECT U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate, ROW_NUMBER() OVER(ORDER BY C.Id DESC) AS RowID FROM tblComment  C 
	JOIN tblUser AS U ON C.UserId = U.Id
	WHERE FeedId = @p_FeedId

	SELECT UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,Id, Comment, CommentDate, RowID FROM @UserTable
	WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	AND ISNULL(@p_LastUpdatedTime, CONVERT(DATETIME, '1753-01-01 00:00:00')) <= CommentDate
	ORDER BY CommentDate DESC
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
