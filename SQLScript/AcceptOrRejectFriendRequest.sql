
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[AcceptOrRejectFriendRequest]       
(     
	@p_UserId			BIGINT,
	@p_RequestedUserIds	VARCHAR(MAX),	
	@p_Status			SMALLINT,
	@p_Error			INT	OUTPUT
)      
AS   

/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Accept or reject friend requests
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  
10011				: Failed to insert friend request

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/10/2015
************************************************************************************************/
     
BEGIN  
	SET NOCOUNT ON       
	
	SET @p_Error=0
	BEGIN TRY 		
		BEGIN
		DECLARE @l_RequestedUserIds TABLE
		(
			UserId		INT
		)
		
		IF ISNULL(@p_RequestedUserIds,'')<>''
			INSERT INTO @l_RequestedUserIds(UserId )
			SELECT Id FROM [dbo].[GetSplittedString_fn](@p_RequestedUserIds,',')
					
		DECLARE @RequestedUserId INT
		DECLARE sourceDBCoursor cursor 
	    FOR SELECT UserId FROM @l_RequestedUserIds		
		OPEN sourceDBCoursor
		FETCH next FROM sourceDBCoursor INTO @RequestedUserId
		WHILE(@@FETCH_STATUS = 0)		
			BEGIN
				DELETE FROM tblFriendRequests WHERE ReceivedUserId = @p_UserId AND UserId = @RequestedUserId
		
				IF @p_Status = 1
					INSERT INTO tblFriends (UserId, FriendId,CreatedTime) VALUES (@p_UserId, @RequestedUserId,GETUTCDATE())	
					INSERT INTO tblFriends (UserId, FriendId,CreatedTime) VALUES (@RequestedUserId, @p_UserId,GETUTCDATE())			
				FETCH next FROM sourceDBCoursor INTO @RequestedUserId
			END																
		END
			
	 END TRY  
	 BEGIN CATCH
		SELECT	 @p_Error 	= 10011
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	 END CATCH

END 


