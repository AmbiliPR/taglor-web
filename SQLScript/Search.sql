GO
/****** Object:  StoredProcedure [dbo].[Search]    Script Date: 07/03/2015 12:53:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Search]
@p_UserId			BIGINT,
@p_PageIndex		INT,
@p_PageSize			SMALLINT,
@pSearchParams		INT,
@p_SearchText		NVARCHAR(50),
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Search 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0

	DECLARE @UserTable AS TABLE
	(
		Id INT,
		FirstName VARCHAR(50),
		LastName VARCHAR(50),
		UserName VARCHAR(50),
		ProfilePic VARCHAR(50),
		BannerImage VARCHAR(50),
		EmailAddress VARCHAR(50),
		Location VARCHAR(50),
		RowID INT
	)
		
	DECLARE @FeedTable AS TABLE
	(
		FeedId			INT,
		Title			VARCHAR(50),
		[Description]	VARCHAR(MAX),
		Location		VARCHAR(MAX),
		UserId			INT,
		PostTypeId		SMALLINT,
		PrivacySettingsId SMALLINT,
		CreatedTime		DATETIME,
		ModifiedTime	DATETIME,		
		RowID			INT		
	)	

	DECLARE @WishTable AS TABLE
	(
		FeedId			INT,
		Title			VARCHAR(50),
		[Description]	VARCHAR(MAX),
		Location		VARCHAR(MAX),
		UserId			INT,
		PostTypeId		SMALLINT,
		PrivacySettingsId SMALLINT,
		CreatedTime		DATETIME,
		ModifiedTime	DATETIME,		
		RowID			INT		
	)	
	BEGIN TRY 	
	BEGIN
	IF(@pSearchParams = 1) --All tables
	BEGIN
	INSERT INTO @UserTable
	SELECT Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,ROW_NUMBER() OVER(ORDER BY Id DESC) AS RowID 
	FROM tblUser 
	WHERE (UserName LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR FirstName LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR LastName LIKE '%'+ISNULL(@p_SearchText,'')+'%')

	INSERT INTO @FeedTable	
	SELECT Feed.Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime,ROW_NUMBER() OVER(ORDER BY Feed.Id DESC) AS RowID			
	FROM tblFeed AS Feed
	LEFT OUTER JOIN tblFeedTags FT ON FT.FeedId = Feed.Id 
	LEFT OUTER JOIN tblTags T ON FT.TagId = T.Id
	WHERE UserId = @p_UserId AND Feed.WishList = 0 AND (Title LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR T.Tags LIKE '%'+ISNULL(@p_SearchText,'')+'%')

	INSERT INTO @WishTable	
	SELECT Feed.Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId,ModifiedTime, CreatedTime,ROW_NUMBER() OVER(ORDER BY Feed.Id DESC) AS RowID			
	FROM tblFeed AS Feed
	LEFT OUTER JOIN tblFeedTags FT ON FT.FeedId = Feed.Id 
	LEFT OUTER JOIN tblTags T ON FT.TagId = T.Id
	WHERE UserId = @p_UserId AND Feed.WishList = 1 AND (Title LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR T.Tags LIKE '%'+ISNULL(@p_SearchText,'')+'%')

	END
	ELSE IF(@pSearchParams = 2) -- Only user table
	BEGIN
	INSERT INTO @UserTable
	SELECT Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,ROW_NUMBER() OVER(ORDER BY Id DESC) AS RowID 
	FROM tblUser 
	WHERE (UserName LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR FirstName LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR LastName LIKE '%'+ISNULL(@p_SearchText,'')+'%')

	END
		
	ELSE IF(@pSearchParams = 3) -- Only inventory table	
	BEGIN
	INSERT INTO @FeedTable	
	SELECT Feed.Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime,ROW_NUMBER() OVER(ORDER BY Feed.Id DESC) AS RowID			
	FROM tblFeed AS Feed
	LEFT OUTER JOIN tblFeedTags FT ON FT.FeedId = Feed.Id 
	LEFT OUTER JOIN tblTags T ON FT.TagId = T.Id
	WHERE UserId = @p_UserId AND Feed.WishList = 0 AND (Title LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR T.Tags LIKE '%'+ISNULL(@p_SearchText,'')+'%')
	GROUP BY Feed.Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId, ModifiedTime, CreatedTime
	END

	ELSE IF(@pSearchParams = 4) -- Only wishlist
	BEGIN
	INSERT INTO @WishTable	
	SELECT Feed.Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime,ROW_NUMBER() OVER(ORDER BY Feed.Id DESC) AS RowID			
	FROM tblFeed AS Feed
	LEFT JOIN tblFeedTags FT ON FT.FeedId = Feed.Id 
	LEFT JOIN tblTags T ON FT.TagId = T.Id
	WHERE UserId = @p_UserId AND Feed.WishList = 1 AND (Title LIKE '%'+ISNULL(@p_SearchText,'')+'%' OR T.Tags LIKE '%'+ISNULL(@p_SearchText,'')+'%')
	GROUP BY Feed.Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId,ModifiedTime, CreatedTime
	END

	SELECT  UT.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location, CASE WHEN (F.FriendId IS NULL) THEN 'False'ELSE 'True' END AS IsFriend,CASE WHEN (FO.FollowerId IS NULL) THEN 'False'ELSE 'True' END AS IsFollower FROM @UserTable UT
	LEFT OUTER JOIN tblFriends F ON ( F.FriendId = UT.Id AND F.UserId = @p_UserId)
	LEFT OUTER JOIN tblFollower FO ON (FO.FollowerId = UT.Id AND FO.UserId = @p_UserId)
	 
	
	WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)

	SELECT F.FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber,F.PostTypeId,F.PrivacySettingsId, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic,U.Location AS UserLocation,
	(SELECT '<FeedImages>' + (SELECT Id, ImageUrl, CASE WHEN (IsReceipt IS NULL OR IsReceipt=0) THEN 'False' ELSE 'True' END AS IsReceipt  FROM tblImageData WHERE FeedId = F.FeedId
				FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
	(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
					INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
					WHERE FT.FeedId = F.FeedId
				FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
	ISNULL((SELECT SUM(CASE WHEN IsLiked IS NULL OR IsLiked = 0 THEN 0 ELSE 1 END) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId),0) AS LikedCount,
	ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId),0) AS Rating,
	ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS IsLikedByUser,
	ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS RatingByUser,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId) AS CommentCount,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId AND UserID = @p_UserID) AS CommentCountByUser
	FROM @FeedTable F
	LEFT JOIN tblFeedDetails FD ON F.FeedId = FD.FeedId
	INNER JOIN tblUser U ON U.Id = F.UserId
	WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	ORDER BY F.FeedID DESC

	SELECT F.FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber,F.PostTypeId,F.PrivacySettingsId, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic,U.Location AS UserLocation,
	(SELECT '<FeedImages>' + (SELECT Id, ImageUrl, CASE WHEN (IsReceipt IS NULL OR IsReceipt=0) THEN 'False' ELSE 'True' END AS IsReceipt  FROM tblImageData WHERE FeedId = F.FeedId
				FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
	(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
					INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
					WHERE FT.FeedId = F.FeedId
				FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
	(SELECT COUNT(IsLiked) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId) AS LikedCount,
	ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId),0) AS Rating,
	ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS IsLikedByUser,
	ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS RatingByUser,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId) AS CommentCount,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId AND UserID = @p_UserID) AS CommentCountByUser
	FROM @WishTable F
	LEFT JOIN tblFeedDetails FD ON F.FeedId = FD.FeedId
	INNER JOIN tblUser U ON U.Id = F.UserId
	WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	ORDER BY F.FeedID DESC
	--SELECT * FROM @FeedTable WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	--SELECT * FROM @WishTable WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	END
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
