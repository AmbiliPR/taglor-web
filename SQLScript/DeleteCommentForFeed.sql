GO
/****** Object:  StoredProcedure [dbo].[DeleteCommentForFeed]    Script Date: 07/03/2015 13:34:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[DeleteCommentForFeed]
@p_CommentId		BIGINT,
@p_IsFlashTag		BIGINT,
@p_FeedId			BIGINT,
@p_UserID			BIGINT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: DeleteCommentForFeed 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	BEGIN TRY 	
	BEGIN TRANSACTION
	IF(@p_IsFlashTag = 1)
	BEGIN
	DELETE FROM tblUserFlashTagActivity WHERE FlashTagCommentId = @p_CommentId
	DELETE FROM tblFlashTagComments WHERE Id = @p_CommentId
	SELECT F.Id AS FlashtagId, Question, EndTime, UserId, CreatedTime, ModifiedTime,F.Location, 
			U.Id, U.FirstName, U.LastName, U.ProfilePic, U.Location AS UserLocation,

	(SELECT '<FlashTagImages>' + 
	
	(SELECT FTI.Id, FlashtagImageUrl,'FALSE' AS IsReceipt,	
	ISNULL((SELECT SUM(CASE WHEN UTFI.IsLiked IS NULL OR UTFI.IsLiked = 0 THEN 0 ELSE 1 END)),0) AS LikedCount,  
	ISNULL((SELECT SUM(CASE WHEN UTFI.IsDisLiked IS NULL OR UTFI.IsDisLiked = 0 THEN 0 ELSE 1 END)),0) AS DisLikedCount,
	 ISNULL(AVG(UTFI.Rating), 0) AS Rating,
	 ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId  = FTI.Id AND UserID = @p_UserID),0) AS IsLikedByUser,
	 ISNULL((SELECT ISNULL(IsDisLiked, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId = FTI.Id AND UserID = @p_UserID),0) AS IsDisLikedByUser,
	 ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId =  FTI.Id AND UserID = @p_UserID),0) AS RatingByUser
	FROM tblFlashTagImageData FTI 
	LEFT OUTER JOIN tblUserFlashTagInteraction UTFI ON UTFI.FlashTagImageId = FTI.Id 
	WHERE FTI.FlashTagId = F.Id	GROUP BY FTI.Id,FlashtagImageUrl FOR XML PATH('Images')) + '</FlashTagImages>') AS FlashTagImages,
	
	(SELECT '<FeedComments>' + (SELECT U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblFlashTagComments  C 
	JOIN tblUser AS U ON C.UserId = U.Id WHERE C.FlashTagId = F.Id ORDER BY C.CommentDate DESC FOR XML PATH('Comments')) + '</FeedComments>') AS FeedComments,
		
	(SELECT COUNT(Id) FROM tblFlashTagComments WHERE FlashTagId = F.Id) AS CommentCount,
	(SELECT COUNT(Id) FROM tblFlashTagComments WHERE FlashTagId = F.Id AND UserID = @p_UserID) AS CommentCountByUser,
	STUFF((SELECT ', ' + CAST(SharedUserIds AS VARCHAR(10)) [text()]
         FROM tblFlashTagShares  
         WHERE FlashtagId = F.Id
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') SharedUserIds
	FROM tblFlashTags F	
	INNER JOIN tblUser U ON U.Id = F.UserId
	WHERE 
	F.Id = @p_FeedId
	GROUP BY F.Id, Question, EndTime, UserId, CreatedTime, ModifiedTime,F.Location, 
			U.Id, U.FirstName, U.LastName, U.ProfilePic, U.Location

	END
	ELSE
	BEGIN
	DELETE FROM tblUserActivity WHERE CommentId = @p_CommentId
	DELETE FROM tblComment WHERE Id = @p_CommentId
	SELECT F.Id AS FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber,F.PostTypeId, F.PrivacySettingsId, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic,U.Location AS UserLocation,
	(SELECT '<FeedImages>' + (SELECT Id, ImageUrl, CASE WHEN (IsReceipt IS NULL OR IsReceipt=0) THEN 'False' ELSE 'True' END AS IsReceipt  FROM tblImageData WHERE FeedId = F.Id
				FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
	(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
					INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
					WHERE FT.FeedId = F.Id
				FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
		(SELECT '<FeedComments>' + (SELECT  U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblComment  C 
	JOIN tblUser AS U ON C.UserId = U.Id WHERE C.FeedId = @p_FeedId ORDER BY C.CommentDate DESC FOR XML PATH('Comments')) + '</FeedComments>') AS FeedComments,

	(SELECT SUM(CASE IsLiked WHEN 1 THEN 1 ELSE 0 END) FROM tblUserFeedInteraction WHERE FeedId = F.Id) AS LikedCount,
	ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS Rating,
	ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID), 0) AS IsLikedByUser,
	ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID), 0) AS RatingByUser,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id) AS CommentCount,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id AND UserID = @p_UserID) AS CommentCountByUser
	FROM tblFeed F
	LEFT JOIN tblFeedDetails FD ON F.Id = FD.FeedId
	INNER JOIN tblUser U ON U.Id = F.UserId
	LEFT OUTER JOIN tblUserActivity UA ON UA.FeedId = F.Id
	WHERE F.Id  = @p_FeedId
	GROUP BY F.Id, Title, [Description], F.Location, Brand, Model, SerialNumber,F.PostTypeId, F.PrivacySettingsId, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic,U.Location

	END
	COMMIT TRANSACTION
	END TRY  
	BEGIN CATCH
	ROLLBACK TRANSACTION
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
