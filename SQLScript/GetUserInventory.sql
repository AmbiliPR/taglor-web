GO
/****** Object:  StoredProcedure [dbo].[GetUserInventory]    Script Date: 07/03/2015 13:13:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER PROCEDURE [dbo].[GetUserInventory]
@p_UserId			BIGINT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Get User Inventory - Tags Recordset, Feeds Recordset

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/18/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
		
	BEGIN TRY 	
	
	
		SELECT DISTINCT T.Id AS TagId, T.Tags, COUNT(FeedId) AS FeedsCount
		FROM tblTags AS T
		INNER JOIN tblFeedTags AS FT ON T.Id = FT.TagId
		INNER JOIN tblFeed AS F ON F.Id = FT.FeedId		
		WHERE F.UserId = @p_UserId
		GROUP BY T.Id, t.Tags

		SELECT TOP 5 F.Id AS FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
					U.Id, U.FirstName, U.LastName, U.ProfilePic, F.PostTypeId,F.PrivacySettingsId,ROW_NUMBER() OVER(ORDER BY F.Id DESC) AS RowID,
					(SELECT '<FeedImages>' + (SELECT Id, ImageUrl FROM tblImageData WHERE FeedId = F.Id
								FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
					(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
									INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
									WHERE FT.FeedId = F.Id
								FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
					(SELECT '<FeedComments>' + (SELECT TOP 3 U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblComment  C 
					JOIN tblUser AS U ON C.UserId = U.Id WHERE C.FeedId = F.Id ORDER BY C.CommentDate DESC FOR XML PATH('Comments')) + '</FeedComments>') AS Comments,
					ISNULL((SELECT SUM(CASE WHEN IsLiked IS NULL OR IsLiked = 0 THEN 0 ELSE 1 END) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS LikedCount,
					ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS Rating,
					ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserId),0) AS IsLikedByUser,
					ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserId),0) AS RatingByUser,
					(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id) AS CommentCount,
					(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id AND UserID = @p_UserId) AS CommentCountByUser,
					FT.TagId
			FROM tblFeedTags FT
				INNER JOIN tblFeed F ON F.Id = FT.FeedId
				LEFT JOIN tblFeedDetails FD ON F.Id = FD.FeedId
				INNER JOIN tblUser U ON U.Id = F.UserId
			WHERE UserId = @p_UserId AND WishList = 0
			ORDER BY F.ModifiedTime	DESC
	
		
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
