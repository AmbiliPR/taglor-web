GO
/****** Object:  StoredProcedure [dbo].[GetLikedOrDislikedUsersFromFlashTag]    Script Date: 07/06/2015 04:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetLikedOrDislikedUsersFromFlashTag]
@p_UserId			BIGINT,
@p_FeedId			BIGINT,
@p_IsLike			BIT,
@p_ImageId			BIGINT,
@p_PageIndex		INT,
@p_PageSize			SMALLINT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: GetLikedOrDislikedUsersFromFlashTag 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	BEGIN TRY 	

		DECLARE @UserTable AS TABLE
	(
		Id INT,
		FirstName VARCHAR(50),
		LastName VARCHAR(50),
		UserName VARCHAR(50),
		ProfilePic VARCHAR(50),
		BannerImage VARCHAR(50),
		EmailAddress VARCHAR(50),
		Location VARCHAR(50),
		RowID INT
	)
	

	IF(@p_IsLike = 1)
	BEGIN
	INSERT INTO @UserTable
	SELECT U.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,ROW_NUMBER() OVER(ORDER BY U.Id DESC) AS RowID FROM tblUserFlashTagInteraction  C 
	JOIN tblUser AS U ON C.UserId = U.Id
	WHERE FlashTagImageId = @p_ImageId AND C.IsLiked = 1
	END
	ELSE
	BEGIN
	INSERT INTO @UserTable
	SELECT U.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,ROW_NUMBER() OVER(ORDER BY U.Id DESC) AS RowID FROM tblUserFlashTagInteraction  C 
	JOIN tblUser AS U ON C.UserId = U.Id
	WHERE FlashTagImageId = @p_ImageId AND C.IsDisLiked = 1
	END

	SELECT  UT.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location, CASE WHEN (F.FriendId IS NULL) THEN 'False'ELSE 'True' END AS IsFriend,CASE WHEN (FO.FollowerId IS NULL) THEN 'False'ELSE 'True' END AS IsFollower FROM @UserTable UT
	LEFT OUTER JOIN tblFriends F ON ( F.FriendId = UT.Id AND F.UserId = @p_UserId)
	LEFT OUTER JOIN tblFollower FO ON (FO.FollowerId = UT.Id AND FO.UserId = @p_UserId)
	WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	END TRY  
	BEGIN CATCH

	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
