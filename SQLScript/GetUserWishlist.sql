GO
/****** Object:  StoredProcedure [dbo].[GetUserWishlist]    Script Date: 07/03/2015 13:08:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER PROCEDURE [dbo].[GetUserWishlist]
@p_UserId			BIGINT,
@p_PageIndex		INT,
@p_PageSize			SMALLINT,
@p_LastUpdateTime	DATETIME = NULL OUTPUT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Get User Feeds that is added to Wishlist
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
		
	BEGIN TRY 	
	BEGIN
		
	IF @p_LastUpdateTime IS NULL
	BEGIN	
		SELECT	FeedId AS FeedId, Title, [Description], Location, Brand, Model, SerialNumber, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
				Id, FirstName, LastName, ProfilePic,UserLocation, PostTypeId, PrivacySettingsId, RowID, FeedImages, FeedTags, LikedCount, CommentCount, Rating, IsLikedByUser, RatingByUser, CommentCount, CommentCountByUser  FROM
			(SELECT F.Id AS FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
					U.Id, U.FirstName, U.LastName, U.ProfilePic, U.Location AS UserLocation,F.PostTypeId,F.PrivacySettingsId, ROW_NUMBER() OVER(ORDER BY F.Id DESC) AS RowID,
					(SELECT '<FeedImages>' + (SELECT Id, ImageUrl,CASE WHEN (IsReceipt IS NULL OR IsReceipt=0) THEN 'False' ELSE 'True' END AS IsReceipt FROM tblImageData WHERE FeedId = F.Id
								FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
					(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
									INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
									WHERE FT.FeedId = F.Id
								FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
					ISNULL((SELECT SUM(CASE WHEN IsLiked IS NULL OR IsLiked = 0 THEN 0 ELSE 1 END) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS LikedCount,
					ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS Rating,
					ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID),0) AS IsLikedByUser,
					ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID),0) AS RatingByUser,
					(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id) AS CommentCount,
					(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id AND UserID = @p_UserID) AS CommentCountByUser
			FROM tblFeed F
				INNER JOIN tblFeedDetails FD ON F.Id = FD.FeedId
				INNER JOIN tblUser U ON U.Id = F.UserId
			WHERE F.WishList = 1 AND F.UserId = @p_UserId) 		
		AS DT
		WHERE DT.RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
		ORDER BY FeedID DESC
	END
	ELSE
	BEGIN	
		SELECT	FeedId AS FeedId, Title, [Description], Location, Brand, Model, SerialNumber, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
				Id, FirstName, LastName, ProfilePic, UserLocation,PostTypeId,PrivacySettingsId, RowID, FeedImages, FeedTags, LikedCount, CommentCount, Rating, IsLikedByUser, RatingByUser, CommentCount, CommentCountByUser  FROM
			(SELECT F.Id AS FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
					U.Id, U.FirstName, U.LastName, U.ProfilePic, U.Location AS UserLocation,F.PostTypeId, F.PrivacySettingsId, ROW_NUMBER() OVER(ORDER BY F.Id DESC) AS RowID,
					(SELECT '<FeedImages>' + (SELECT Id, ImageUrl FROM tblImageData WHERE FeedId = F.Id
								FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
					(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
									INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
									WHERE FT.FeedId = F.Id
								FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
					(SELECT SUM(CASE IsLiked WHEN 1 THEN 1 ELSE 0 END) FROM tblUserFeedInteraction WHERE FeedId = F.Id) AS LikedCount,
					ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS Rating,
					ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID),0) AS IsLikedByUser,
					ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID),0) AS RatingByUser,
					(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id) AS CommentCount,
					(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id AND UserID = @p_UserID) AS CommentCountByUser
			FROM tblFeed F
				INNER JOIN tblFeedDetails FD ON F.Id = FD.FeedId
				INNER JOIN tblUser U ON U.Id = F.UserId
			WHERE  F.WishList = 1  AND F.UserId = @p_UserId AND F.ModifiedTime > @p_LastUpdateTime) 		
		AS DT
		WHERE DT.RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
		ORDER BY FeedID DESC
	END
	
	/* Get last modified from feed table and set to @p_LastUpdateTime */
	SELECT TOP 1 @p_LastUpdateTime = ModifiedTime FROM tblFeed ORDER BY ModifiedTime DESC

	END
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
