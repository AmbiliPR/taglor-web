
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SaveFriendRequest]       
(     
	@p_UserId			BIGINT,
	@p_RequestedUserIds	VARCHAR(MAX),		
	@p_Error			INT	OUTPUT
)      
AS   

/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Save friend requests
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  
10010				: Failed to insert friend request

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/10/2015
************************************************************************************************/
     
BEGIN  
	SET NOCOUNT ON       
	
	SET @p_Error=0
	BEGIN TRY 		
		BEGIN

		DECLARE @l_RequestedUserIds TABLE
		(
			UserId		INT
		)
		
		IF ISNULL(@p_RequestedUserIds,'')<>''
			INSERT INTO @l_RequestedUserIds(UserId )
			SELECT Id FROM [dbo].[GetSplittedString_fn](@p_RequestedUserIds,',')
					
		DECLARE @RequestedUserId INT
		DECLARE sourceDBCoursor cursor 
	    FOR SELECT UserId FROM @l_RequestedUserIds		
		OPEN sourceDBCoursor
		FETCH next FROM sourceDBCoursor INTO @RequestedUserId
		WHILE(@@FETCH_STATUS = 0)		
			BEGIN
				IF NOT EXISTS (SELECT 1 FROM tblFriendRequests WHERE UserId = @p_UserId AND ReceivedUserId = @RequestedUserId)
					INSERT INTO tblFriendRequests (UserId, ReceivedUserId,CreatedTime) VALUES (@p_UserId, @RequestedUserId,GETUTCDATE())		
				FETCH next FROM sourceDBCoursor INTO @RequestedUserId
			END																
		END
	 END TRY  
	 BEGIN CATCH
		SELECT	 @p_Error 	= 10010
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	 END CATCH

END 


