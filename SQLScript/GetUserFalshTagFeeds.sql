GO
/****** Object:  StoredProcedure [dbo].[GetUserFlashtagFeeds]    Script Date: 07/03/2015 13:14:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetUserFlashtagFeeds]
@p_UserId			BIGINT,
@p_PageIndex		INT,
@p_PageSize			SMALLINT,
@p_CommentCount		SMALLINT,
@p_LastUpdateTime	DATETIME = NULL OUTPUT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Get User Flashtag feeds
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN

	DECLARE @FlashtagTable AS TABLE
	(
		FlashtagId		INT,
		Question		VARCHAR(MAX),		
		UserId			INT,		
		EndTime			DATETIME,
		CreatedTime		DATETIME,
		ModifiedTime	DATETIME,
		Location		VARCHAR(MAX),
		RowID			INT		
	)	
	
	BEGIN TRY 	
	BEGIN	

	IF @p_LastUpdateTime IS NULL
	BEGIN		
		INSERT INTO @FlashtagTable
		
		SELECT Id AS FlashtagId, Question, UserId, EndTime, CreatedTime, ModifiedTime, Location, ROW_NUMBER() OVER(ORDER BY DT.Id DESC) AS RowID FROM (
			SELECT Id, Question, EndTime, UserId, CreatedTime, ModifiedTime,Location	
				FROM tblFlashTags
				WHERE UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_UserId)
				AND EndTime <= GETUTCDATE()
			UNION
			SELECT Id, Question, EndTime, UserId, CreatedTime, ModifiedTime,Location		
				FROM tblFlashTags
				WHERE UserId = @p_UserId
		) AS DT
	END
	ELSE
	BEGIN
		INSERT INTO @FlashtagTable
	
		SELECT Id, Question, UserId, CreatedTime, EndTime, ModifiedTime, Location,ROW_NUMBER() OVER(ORDER BY DT.Id DESC) AS RowID FROM (
			SELECT Id, Question, EndTime, UserId, CreatedTime, ModifiedTime,Location			
				FROM tblFlashTags
				WHERE UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_UserId)
				AND EndTime <= GETUTCDATE()
			UNION
			SELECT Id, Question, EndTime, UserId, CreatedTime, ModifiedTime,Location		
				FROM tblFlashTags
				WHERE UserId = @p_UserId
		) AS DT
		WHERE Id IN (SELECT DISTINCT Id FROM tblUserFlashTagActivity WHERE UpdatedTime > @p_LastUpdateTime) 
		OR ModifiedTime > @p_LastUpdateTime
	END
				
	SELECT FlashtagId, Question, EndTime, UserId, CreatedTime, ModifiedTime,F.Location, 
			U.Id, U.FirstName, U.LastName, U.ProfilePic, U.Location AS UserLocation,

	(SELECT '<FlashTagImages>' + 
	
	(SELECT FTI.Id, FlashtagImageUrl,'FALSE' AS IsReceipt,
	 ISNULL((SELECT SUM(CASE WHEN UTFI.IsLiked IS NULL OR UTFI.IsLiked = 0 THEN 0 ELSE 1 END)),0) AS LikedCount,  
	 ISNULL((SELECT SUM(CASE WHEN UTFI.IsDisLiked IS NULL OR UTFI.IsDisLiked = 0 THEN 0 ELSE 1 END)),0)AS DisLikedCount,
	 ISNULL(AVG(UTFI.Rating), 0) AS Rating,
	 ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId  = FTI.Id AND UserID = @p_UserID),0) AS IsLikedByUser,
	 ISNULL((SELECT ISNULL(IsDisLiked, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId = FTI.Id AND UserID = @p_UserID),0) AS IsDisLikedByUser,
	 ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId =  FTI.Id AND UserID = @p_UserID),0) AS RatingByUser
	FROM tblFlashTagImageData FTI 
	LEFT OUTER JOIN tblUserFlashTagInteraction UTFI ON UTFI.FlashTagImageId = FTI.Id 
	WHERE FTI.FlashTagId = F.FlashtagId	GROUP BY FTI.Id,FlashtagImageUrl FOR XML PATH('Images')) + '</FlashTagImages>') AS FlashTagImages,	
	(SELECT '<FeedComments>' + (SELECT TOP (@p_CommentCount) U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblFlashTagComments  C 
	JOIN tblUser AS U ON C.UserId = U.Id WHERE C.FlashTagId = F.FlashtagId ORDER BY C.CommentDate DESC FOR XML PATH('Comments')) + '</FeedComments>') AS FeedComments,
	(SELECT COUNT(Id) FROM tblFlashTagComments WHERE FlashTagId = F.FlashtagId) AS CommentCount,
	(SELECT COUNT(Id) FROM tblFlashTagComments WHERE FlashTagId = F.FlashtagId AND UserID = @p_UserID) AS CommentCountByUser,
	STUFF((SELECT ', ' + CAST(SharedUserIds AS VARCHAR(10)) [text()]
         FROM tblFlashTagShares  
         WHERE FlashtagId = F.FlashtagId
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') SharedUserIds
	FROM @FlashtagTable F	
	INNER JOIN tblUser U ON U.Id = F.UserId
	WHERE 
	RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	ORDER BY F.FlashtagId DESC			
					
					
	END
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH
		
END
