GO
DROP PROCEDURE [dbo].CreateFeed 
DROP PROCEDURE [dbo].CreateFlashTags  
DROP PROCEDURE [dbo].UploadImage
DROP PROCEDURE [dbo].GetTags
DROP PROCEDURE [dbo].GetPrivacySettings
DROP FUNCTION [dbo].GetSplittedString_fn


/****** Object:  UserDefinedFunction [dbo].[GetSplittedString_fn]    Script Date: 02/04/2015 00:54:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Get splitted values from comma seperated string
-- =============================================
CREATE FUNCTION [dbo].[GetSplittedString_fn]
(	
	
	@p_String		VARCHAR(8000),
	@p_Delimiter	CHAR(1)
	
)
RETURNS @StringValues TABLE 
(
	Rownum	INT IDENTITY(1,1) PRIMARY KEY,
	Id		VARCHAR(100)
)
AS
BEGIN
	DECLARE @l_StringValue	VARCHAR(100)

	WHILE CHARINDEX(@p_Delimiter,@p_String) > 0
	BEGIN
		SET @l_StringValue = LTRIM(RTRIM(SUBSTRING(@p_String,1,CHARINDEX(@p_Delimiter,@p_String)-1)))
		INSERT @StringValues VALUES(@l_StringValue)
		SET @p_String = RIGHT(LTRIM(@p_String),LEN(LTRIM(@p_String)) - LEN(@l_StringValue)- 1)
	END
	
	IF CHARINDEX(',',@p_String) = 0
		INSERT @StringValues VALUES(LTRIM(RTRIM(@p_String)))

RETURN
END

GO
/****** Object:  StoredProcedure [dbo].[CreateFeed] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateFeed]       
(     
	@p_UserId			INT			,
	@p_Title		NVARCHAR(50),
	@p_Description NVARCHAR(MAX),
	@p_Tags NVARCHAR(MAX),
	@p_Brand NVARCHAR(50),
	@p_Model NVARCHAR(50),
	@p_Serial NVARCHAR(50),
	@p_PurchasedFrom NVARCHAR(50),
	@p_PurchasePrice NVARCHAR(50),
	@p_Warranty NVARCHAR(50),
	@p_ExpirationDate DATETIME,
	@p_Location NVARCHAR(MAX) = NULL,
	@p_Settings INT,
	@p_AddToWishlist BIT,
	@p_PostType INT,
	@p_FeedId		INT OUTPUT	,
	@p_Error			INT OUTPUT
)      
AS   
    
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ErrID	INT
	-- Error Handling declarations End 

	SET @l_ErrID =0
	SET @p_Error =0
	BEGIN TRANSACTION
	BEGIN TRY 		

	DECLARE @l_Tags TABLE
	(
	 TagId		NVARCHAR(MAX)
	 )
	 
	DECLARE @l_CurrentDate DATETIME
	SET @l_CurrentDate = CONVERT(DATETIME,GETUTCDATE())

	IF ISNULL(@p_Tags,'')<>''
			INSERT INTO @l_Tags(TagId )
			SELECT Id FROM [dbo].[GetSplittedString_fn](@p_Tags,',')
	---Step 1
		
		---Handling Feeds
		--BEGIN TRANSACTION
		DECLARE @TagID NVARCHAR(MAX)
		---Step 1
			INSERT INTO  tblFeed (Title,Description, UserId, PostTypeId, WishList, PrivacySettingsId, Location, CreatedTime, ModifiedTime ) VALUES (@p_Title,@p_Description,@p_UserId,@p_PostType, @p_AddToWishlist, @p_Settings, @p_Location, @l_CurrentDate, @l_CurrentDate )  
			SET @p_FeedId = (SELECT MAX(Id) FROM tblFeed)
		
		INSERT INTO tblFeedDetails(FeedId,Brand,SerialNumber,Model,PurchasedFrom,Price,Warranty,ExpiryDate) VALUES(@p_FeedId, @p_Brand, @p_Serial, @p_Model,@p_PurchasedFrom,@p_PurchasePrice,@p_Warranty,@p_ExpirationDate)

		DECLARE @p_TagId INT
		DECLARE sourceDBCoursor cursor FOR SELECT TagId FROM @l_Tags		
		OPEN sourceDBCoursor
		FETCH NEXT FROM sourceDBCoursor INTO @TagID
		WHILE(@@FETCH_STATUS = 0)
		---Step 2
		BEGIN

		SELECT @p_TagId = Id FROM tblTags WHERE Tags LIKE LTRIM(RTRIM(@TagID))
		IF(@p_TagId IS NULL) 
		BEGIN
		INSERT INTO tblTags (Tags) VALUES (@TagID)
		SET @p_TagId =SCOPE_IDENTITY ()
		END
		INSERT INTO tblFeedTags(FeedId, TagId) VALUES(@p_FeedId,@p_TagId)
		FETCH NEXT FROM sourceDBCoursor INTO @TagID
		SET @p_TagId = NULL
		END
		CLOSE sourceDBCoursor    
		DEALLOCATE sourceDBCoursor
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[CreateFlashTags] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateFlashTags]       
(     
	@p_UserId			INT			,
	@p_Question		NVARCHAR(50),
	@p_EndTime DATETIME,
	@p_SharedUserIds NVARCHAR(MAX),
	@p_Location NVARCHAR(MAX),
	@p_FeedId		INT OUTPUT	,
	@p_Error			INT OUTPUT
)      
AS   
    
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ErrID	INT
	-- Error Handling declarations End 

	SET @l_ErrID =0
	SET @p_Error =0
	BEGIN TRY 
	BEGIN TRANSACTION		 
	DECLARE @l_UserIds TABLE
	(
		UserId		INT
	)
	DECLARE @l_CurrentDate DATETIME
	SET @l_CurrentDate = CONVERT(DATETIME,GETUTCDATE()) 
	 IF ISNULL(@p_SharedUserIds,'')<>''
			INSERT INTO @l_UserIds(UserId )
			SELECT Id FROM [dbo].[GetSplittedString_fn](@p_SharedUserIds,',')

		---Step 1
			INSERT INTO  tblFlashTags ( Question, UserId, CreatedTime, ModifiedTime, EndTime, Location) VALUES (@p_Question,@p_UserId,@l_CurrentDate, @l_CurrentDate, @p_EndTime, @p_Location )  
			SET @p_FeedId =SCOPE_IDENTITY ()

		DECLARE @p_SharedId INT
		DECLARE sourceDBCoursor cursor FOR SELECT UserId FROM @l_UserIds		
		OPEN sourceDBCoursor
		FETCH NEXT FROM sourceDBCoursor INTO @p_SharedId
		WHILE(@@FETCH_STATUS = 0)
		---Step 2
		BEGIN
		INSERT INTO tblFlashTagShares(FlashTagId, SharedUserIds) VALUES(@p_FeedId, @p_SharedId)
		FETCH NEXT FROM sourceDBCoursor INTO @p_SharedId	
		END
		CLOSE sourceDBCoursor    
		DEALLOCATE sourceDBCoursor			
	
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[GetTags] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetTags]         
(@p_UserId		INT)
AS   
BEGIN  
	SET NOCOUNT ON       
	SELECT DISTINCT Tags, TAG.Id FROM tblTags AS TAG
	RIGHT OUTER JOIN tblFeedTags AS FEEDTAGS ON TAG.Id = FEEDTAGS.TagId
	RIGHT OUTER JOIN tblFeed AS FEED ON FEED.UserId = @p_UserId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPrivacySettings] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPrivacySettings]   
(@p_UserId		INT)      
AS   
    
BEGIN  
	SET NOCOUNT ON       
	SELECT PS.PrivacySettings, US.PrivacySettingsId FROM tblUserSettings AS US
	LEFT OUTER JOIN tblPrivacySettingsLookup PS ON US.PrivacySettingsId = PS.Id
	WHERE US.UserId = @p_UserId
END

GO
/****** Object:  StoredProcedure [dbo].[UploadImage] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UploadImage] 
(     
	@p_FeedId			INT			,
	@p_FileName		NVARCHAR(50),
	@p_IsReceipt	BIT,
	@p_ImageId		INT OUTPUT	,
	@p_Error			INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 		 
	---Step 1

	BEGIN TRANSACTION
		
		---Step 1
			INSERT INTO  tblImageData( FeedId, ImageUrl, IsReceipt ) VALUES (@p_FeedId, @p_FileName, @p_IsReceipt )  
			SET @p_ImageId =SCOPE_IDENTITY ()	
	
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END

GO
DROP PROCEDURE dbo.UpdateFeed
/****** Object:  StoredProcedure [dbo].[UpdateFeed] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateFeed]       
(     
	@p_FeedId		INT			,
	@p_Title		NVARCHAR(50),
	@p_Description NVARCHAR(MAX),
	@p_Tags NVARCHAR(MAX),
	@p_Brand NVARCHAR(50),
	@p_Model NVARCHAR(50),
	@p_Serial NVARCHAR(50),
	@p_PurchasedFrom NVARCHAR(50),
	@p_PurchasePrice NVARCHAR(50),
	@p_Warranty NVARCHAR(50),
	@p_ExpirationDate DATETIME,
	@p_Location NVARCHAR(MAX) = NULL,
	@p_Settings INT,
	@p_AddToWishlist BIT,
	@p_Error			INT OUTPUT
)      
AS   
    
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ErrID	INT
	-- Error Handling declarations End 

	SET @l_ErrID =0
	SET @p_Error =0

	BEGIN TRY 		
	BEGIN TRANSACTION

DECLARE @l_Tags TABLE
	(
	 TagId		NVARCHAR(MAX)
	 )
	 
	DECLARE @l_CurrentDate DATETIME
	SET @l_CurrentDate = CONVERT(DATETIME,GETUTCDATE())

IF ISNULL(@p_Tags,'')<>''
			INSERT INTO @l_Tags(TagId )
			SELECT Id FROM [dbo].[GetSplittedString_fn](@p_Tags,',')
	---Step 1
		
		---Handling Feeds
		--BEGIN TRANSACTION
		DECLARE @TagID NVARCHAR(MAX)
		---Step 1
			UPDATE  tblFeed SET Title = @p_Title,Description = @p_Description, WishList = @p_AddToWishlist, PrivacySettingsId = @p_Settings, Location=@p_Location, ModifiedTime = @l_CurrentDate WHERE Id = @p_FeedId
			
        	UPDATE tblFeedDetails SET Brand = @p_Brand,SerialNumber = @p_Serial,Model = @p_Model,PurchasedFrom = @p_PurchasedFrom,Price = @p_PurchasePrice,Warranty = @p_Warranty ,ExpiryDate = @p_ExpirationDate WHERE FeedId = @p_FeedId

			DELETE tblFeedTags WHERE FeedId = @p_FeedId
		
        DECLARE @p_TagId INT
		DECLARE sourceDBCoursor cursor FOR SELECT TagId FROM @l_Tags		
		OPEN sourceDBCoursor
		FETCH NEXT FROM sourceDBCoursor INTO @TagID
		WHILE(@@FETCH_STATUS = 0)
		---Step 2
		BEGIN

		SELECT @p_TagId = Id FROM tblTags WHERE Tags LIKE LTRIM(RTRIM(@TagID))
		IF(@p_TagId IS NULL) 
		BEGIN
		INSERT INTO tblTags (Tags) VALUES (@TagID)
		SET @p_TagId =SCOPE_IDENTITY ()
		END
		INSERT INTO tblFeedTags(FeedId, TagId) VALUES(@p_FeedId,@p_TagId)
		FETCH NEXT FROM sourceDBCoursor INTO @TagID
		SET @p_TagId = NULL
		END
		CLOSE sourceDBCoursor    
		DEALLOCATE sourceDBCoursor
		
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END
-----------------------------------------------------
GO
DROP PROCEDURE dbo.DeleteFeed
/****** Object:  StoredProcedure [dbo].[DeleteFeed] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFeed]       
(     
	@p_FeedId		INT,
	@p_UserId		INT,
	@p_Error		INT OUTPUT
)      
AS   
    
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ErrID	INT
	-- Error Handling declarations End 

	SET @l_ErrID =0
	SET @p_Error =0
	
	BEGIN TRY 	
	BEGIN TRANSACTION	
		DELETE tblFeedTags WHERE FeedId = @p_FeedId
		DELETE tblUserActivity WHERE FeedId = @p_FeedId
		DELETE tblUserFeedInteraction WHERE FeedId = @p_FeedId
		DELETE tblComment WHERE FeedId = @p_FeedId
		DELETE tblImageData WHERE FeedId = @p_FeedId
		DELETE tblFeedDetails WHERE FeedId = @p_FeedId
		DELETE tblFeed WHERE Id = @p_FeedId
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END
-------------------------------------------------------

GO
DROP PROCEDURE [dbo].[UpdateFlashTags] 
/****** Object:  StoredProcedure [dbo].[UpdateFlashTags] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateFlashTags]       
(     
	@p_FeedId			INT			,
	@p_Question		NVARCHAR(50),
	@p_EndTime DATETIME,
	@p_SharedUserIds NVARCHAR(MAX),
	@p_Location NVARCHAR(MAX) = NULL,
	@p_Error			INT OUTPUT
)      
AS   
    
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ErrID	INT
	-- Error Handling declarations End 

	SET @l_ErrID =0
	SET @p_Error =0

	BEGIN TRY 
	BEGIN TRANSACTION		 
	DECLARE @l_CurrentDate DATETIME
	SET @l_CurrentDate = CONVERT(DATETIME,GETUTCDATE()) 

	DECLARE @l_UserIds TABLE
	(
		UserId		INT
	)

	IF ISNULL(@p_SharedUserIds,'')<>''
	INSERT INTO @l_UserIds(UserId )
	SELECT Id FROM [dbo].[GetSplittedString_fn](@p_SharedUserIds,',')
		
	---Step 1
	UPDATE tblFlashTags SET Question = @p_Question, ModifiedTime =@l_CurrentDate, EndTime = @p_EndTime, Location = @p_Location WHERE Id = @p_FeedId  
	DELETE FROM tblFlashTagShares WHERE FlashTagId = @p_FeedId

	DECLARE @p_SharedId INT
		DECLARE sourceDBCoursor cursor FOR SELECT UserId FROM @l_UserIds		
		OPEN sourceDBCoursor
		FETCH NEXT FROM sourceDBCoursor INTO @p_SharedId
		WHILE(@@FETCH_STATUS = 0)
		---Step 2
		BEGIN
		INSERT INTO tblFlashTagShares(FlashTagId, SharedUserIds) VALUES(@p_FeedId, @p_SharedId)
		FETCH NEXT FROM sourceDBCoursor INTO @p_SharedId	
		END
		CLOSE sourceDBCoursor    
		DEALLOCATE sourceDBCoursor	
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END

-----------------------------------------------------
GO
DROP PROCEDURE dbo.DeleteFlashTags
/****** Object:  StoredProcedure [dbo].[DeleteFlashTags] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFlashTags]       
(     
	@p_FeedId		INT,
	@p_UserId		INT,
	@p_Error		INT OUTPUT
)      
AS   
    
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ErrID	INT
	-- Error Handling declarations End 

	SET @l_ErrID =0
	SET @p_Error =0
	
	BEGIN TRY 		
	BEGIN TRANSACTION
		DELETE tblUserFlashTagActivity WHERE FlashTagId = @p_FeedId 
		DELETE tblFlashTagComments WHERE FlashTagId = @p_FeedId
		DELETE FTI FROM tblUserFlashTagInteraction FTI JOIN tblFlashTagImageData FIM ON FTI.FlashTagImageId = FIM.Id
		WHERE FIM.FlashTagId = @p_FeedId
		DELETE tblFlashTagImageData WHERE FlashTagId = @p_FeedId
		DELETE tblFlashTags WHERE Id = @p_FeedId AND UserId = @p_UserId
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END
-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[UploadFlashTagImage]
/****** Object:  StoredProcedure [dbo].[UploadFlashTagImage] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UploadFlashTagImage] 
(     
	@p_FeedId			INT			,
	@p_FileName		NVARCHAR(50),
	@p_ImageId		INT OUTPUT	,
	@p_Error			INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 		 
	---Step 1

	BEGIN TRANSACTION
		
		---Step 1
			INSERT INTO  tblFlashTagImageData( FlashTagId, FlashTagImageUrl ) VALUES (@p_FeedId, @p_FileName )  
			SET @p_ImageId =SCOPE_IDENTITY ()	
	
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END

-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[DeleteFeedImage] 
/****** Object:  StoredProcedure [dbo].[DeleteFeedImage] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFeedImage] 
(     
	@p_FeedId		INT,
	@p_UserId		INT,
	@p_ImageIds		NVARCHAR(MAX),
	@p_Error		INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 		 
	---Step 1

	BEGIN TRANSACTION
	DECLARE @l_Images TABLE
	(
		ImageId		INT
	)
	
	 IF ISNULL(@p_ImageIds,'')<>''
			INSERT INTO @l_Images(ImageId )
			SELECT Id FROM [dbo].[GetSplittedString_fn](@p_ImageIds,',')
		
		DECLARE @l_ImageId INT
		DECLARE sourceDBCoursor cursor FOR SELECT ImageId FROM @l_Images		
		OPEN sourceDBCoursor
		FETCH NEXT FROM sourceDBCoursor INTO @l_ImageId

		WHILE(@@FETCH_STATUS = 0)
		---Step 2
		BEGIN
				DELETE IMG FROM tblImageData IMG
		JOIN tblFeed Feed ON IMG.FeedId = Feed.Id
		 WHERE IMG.Id = @l_ImageId AND IMG.FeedId = @p_FeedId AND Feed.UserId = @p_UserId
	
		 FETCH NEXT FROM sourceDBCoursor INTO @l_ImageId
		END
		CLOSE sourceDBCoursor    
		DEALLOCATE sourceDBCoursor		

	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END

-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[DeleteFlashTagImage]
/****** Object:  StoredProcedure [dbo].[DeleteFlashTagImage] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFlashTagImage] 
(     
	@p_FeedId		INT,
	@p_UserId		INT,
	@p_ImageIds		NVARCHAR(MAX),
	@p_Error		INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 		 
	---Step 1

	BEGIN TRANSACTION

		DECLARE @l_Images TABLE
	(
		ImageId		INT
	)
	
	 IF ISNULL(@p_ImageIds,'')<>''
			INSERT INTO @l_Images(ImageId )
			SELECT Id FROM [dbo].[GetSplittedString_fn](@p_ImageIds,',')
		
		DECLARE @l_ImageId INT
		DECLARE sourceDBCoursor cursor FOR SELECT ImageId FROM @l_Images		
		OPEN sourceDBCoursor
		FETCH NEXT FROM sourceDBCoursor INTO @l_ImageId

		WHILE(@@FETCH_STATUS = 0)
		---Step 2
		BEGIN
		DELETE FROM tblUserFlashTagInteraction WHERE FlashTagImageId = @l_ImageId

		DELETE IMG FROM tblFlashTagImageData IMG
		JOIN tblFlashTags Feed ON IMG.FlashTagId = Feed.Id
		 WHERE IMG.Id = @l_ImageId AND IMG.FlashTagId = @p_FeedId AND Feed.UserId = @p_UserId

		 FETCH NEXT FROM sourceDBCoursor INTO @l_ImageId
		END
		CLOSE sourceDBCoursor    
		DEALLOCATE sourceDBCoursor	
			
	COMMIT TRANSACTION			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		ROLLBACK TRANSACTION
		RETURN 50001
	END CATCH
END

-------------------------------------------------------
--GO
--DROP PROCEDURE [dbo].[MakeUserInteraction]
--/****** Object:  StoredProcedure [dbo].[MakeUserInteraction] Date: 31/05/2015 00:54:20 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--CREATE PROCEDURE [dbo].[MakeUserInteraction] 
--(     
--	@p_FeedId		INT,
--	@p_UserId		INT,
--	@p_ImageId		INT,
--	@p_Error		INT OUTPUT
--)           
--AS   
    
--BEGIN  
--	SET NOCOUNT ON       
--	BEGIN TRY 		 
--	---Step 1

--	BEGIN TRANSACTION
		
--		DELETE IMG FROM tblFlashTagImageData IMG
--		JOIN tblFlashTags Feed ON IMG.FlashTagId = Feed.Id
--		 WHERE IMG.Id = @p_ImageId AND IMG.FlashTagId = @p_FeedId AND Feed.UserId = @p_UserId
	
--	COMMIT TRANSACTION			
--    END TRY  
--    BEGIN CATCH
--		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
--		SET @p_Error =50001
--		ROLLBACK TRANSACTION
--		RETURN 50001
--	END CATCH
--END