
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[FollowUnFollowUser]
@p_UserID INT,
@p_FollowerUserID INT,
@p_FollowStatus BIT,
@p_Error INT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
    BEGIN TRY
		IF @p_FollowStatus = 1
		BEGIN
		IF NOT EXISTS (SELECT 1 FROM tblFollower WHERE UserID = @p_UserID AND FollowerId = @p_FollowerUserID)
			BEGIN
				INSERT INTO tblFollower (UserId, FollowerId, CreatedTime) VALUES (@p_UserID, @p_FollowerUserID, GETUTCDATE())							
			END
		END
		ELSE 
		BEGIN
			DELETE FROM tblFollower WHERE UserID = @p_UserID AND FollowerId = @p_FollowerUserID
		END		
    END TRY
	BEGIN CATCH 
		SELECT	 @p_Error 	= 10010
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	END CATCH 
   
END
