GO
/****** Object:  StoredProcedure [dbo].[GetProfileFeeds]    Script Date: 07/03/2015 13:26:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetProfileFeeds]
@p_UserId			BIGINT,
@p_PageIndex		INT,
@p_PageSize			SMALLINT,
@p_LoggedInUserId	BIGINT,
@p_LastUpdateTime	DATETIME = NULL OUTPUT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: GetProfileFeeds 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	
	
	DECLARE @FeedTable AS TABLE
	(
		FeedId			INT,
		Title			VARCHAR(50),
		[Description]	VARCHAR(MAX),
		Location		VARCHAR(MAX),
		UserId			INT,
		PostTypeId		SMALLINT,
		PrivacySettingsId SMALLINT,
		CreatedTime		DATETIME,
		ModifiedTime	DATETIME,		
		ActivityTime	DATETIME,
		RowID			INT		
	)	
		
	BEGIN TRY 	
	BEGIN
	
	IF (@p_LastUpdateTime IS NULL)
	BEGIN
		IF(@p_LoggedInUserId = @p_UserId)
		BEGIN
		INSERT INTO @FeedTable
	
		SELECT Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime, ActivityTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get user feeds */			
			SELECT F.Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime, UA.DateTime	AS ActivityTime		
			FROM tblFeed F
			LEFT OUTER JOIN tblUserActivity UA ON F.Id = UA.FeedId
			WHERE UserId = @p_UserId AND WishList = 0
			)AS  FTABLE
		END
		ELSE IF(@p_UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_LoggedInUserId))
		BEGIN
		INSERT INTO @FeedTable
	

			SELECT Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId,ModifiedTime, CreatedTime, ActivityTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get user feeds */			
			SELECT F.Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId, ModifiedTime, CreatedTime, UA.DateTime	AS ActivityTime		
			FROM tblFeed F
			LEFT OUTER JOIN tblUserActivity UA ON F.Id = UA.FeedId
			WHERE UserId = @p_UserId AND WishList = 0  AND  PrivacySettingsId IN (2, 3)					
		)AS  FTABLE
		END
		ELSE
		BEGIN
		INSERT INTO @FeedTable
	
		SELECT Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId,ModifiedTime, CreatedTime, ActivityTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get feeds of user friends */
			SELECT F.Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime, UA.DateTime	AS ActivityTime		
			FROM tblFeed F
			LEFT OUTER JOIN tblUserActivity UA ON F.Id = UA.FeedId
			WHERE UserId = @p_UserId  AND WishList = 0 AND  PrivacySettingsId = 2
			
		)AS  FTABLE
		END
	END
	ELSE
	BEGIN	
	
		IF(@p_LoggedInUserId = @p_UserId)
		BEGIN
		INSERT INTO @FeedTable
	
		SELECT Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime, ActivityTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get user feeds */			
			SELECT F.Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime, UA.DateTime	AS ActivityTime		
			FROM tblFeed F
			LEFT OUTER JOIN tblUserActivity UA ON F.Id = UA.FeedId
			WHERE UserId = @p_UserId AND WishList = 0 AND [DateTime] > @p_LastUpdateTime OR ModifiedTime > @p_LastUpdateTime
		)AS  FTABLE
		END
		ELSE IF(@p_UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_LoggedInUserId))
		BEGIN
		INSERT INTO @FeedTable
	

			SELECT Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId,ModifiedTime, CreatedTime, ActivityTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get user feeds */			
			SELECT F.Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime, UA.DateTime	AS ActivityTime		
			FROM tblFeed F
			LEFT OUTER JOIN tblUserActivity UA ON F.Id = UA.FeedId
			WHERE UserId = @p_UserId AND WishList = 0  AND  PrivacySettingsId IN (2, 3) AND [DateTime] > @p_LastUpdateTime OR ModifiedTime > @p_LastUpdateTime
	
		)AS  FTABLE
		END
		ELSE
		BEGIN
		INSERT INTO @FeedTable
	
		SELECT Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId, ModifiedTime, CreatedTime, ActivityTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get feeds of user friends */
			SELECT F.Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId, ModifiedTime, CreatedTime, UA.DateTime	AS ActivityTime		
			FROM tblFeed F
			LEFT OUTER JOIN tblUserActivity UA ON F.Id = UA.FeedId
			WHERE UserId = @p_UserId  AND WishList = 0 AND  PrivacySettingsId = 2 AND [DateTime] > @p_LastUpdateTime OR ModifiedTime > @p_LastUpdateTime
		)AS  FTABLE
		END		
	END
			
	--SELECT * FROM 	@FeedTable
	SELECT F.FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic, F.ModifiedTime,F.ActivityTime, U.Location AS UserLocation, F.PostTypeId, F.PrivacySettingsId,
	(SELECT '<FeedImages>' + (SELECT Id, ImageUrl, CASE WHEN (IsReceipt IS NULL OR IsReceipt=0) THEN 'False' ELSE 'True' END AS IsReceipt  FROM tblImageData WHERE FeedId = F.FeedId
				FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
	(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
					INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
					WHERE FT.FeedId = F.FeedId
				FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
	ISNULL((SELECT SUM(CASE WHEN IsLiked IS NULL OR IsLiked = 0 THEN 0 ELSE 1 END) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId),0) AS LikedCount,
	ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId),0) AS Rating,
	ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS IsLikedByUser,
	ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS RatingByUser,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId) AS CommentCount,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId AND UserID = @p_UserID) AS CommentCountByUser
	FROM @FeedTable F
	LEFT JOIN tblFeedDetails FD ON F.FeedId = FD.FeedId
	INNER JOIN tblUser U ON U.Id = F.UserId
	WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	ORDER BY ModifiedTime DESC, ActivityTime DESC, F.FeedID DESC
	
	DECLARE @UserActivityUpdateDate DATETIME
	DECLARE @FeedModifiedDate DATETIME
		
	SELECT TOP 1 @UserActivityUpdateDate = [DateTime] FROM tblUserActivity ORDER BY [DateTime] DESC
	SELECT TOP 1 @FeedModifiedDate = ModifiedTime FROM tblFeed ORDER BY ModifiedTime DESC
	
	IF @UserActivityUpdateDate > @FeedModifiedDate
		SET @p_LastUpdateTime = @UserActivityUpdateDate
	ELSE 
		SET @p_LastUpdateTime = @FeedModifiedDate
	
	END
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
