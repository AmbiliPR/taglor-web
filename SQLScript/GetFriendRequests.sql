
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetFriendRequests]       
(     
	@p_UserId			BIGINT,	
	@p_IsSent			BIT,
	@p_Error			INT	OUTPUT
)      
AS   

/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Get all friend requests
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  
10012				: Failed to get friend requests

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/10/2015
************************************************************************************************/
     
BEGIN  
	SET NOCOUNT ON       
	
	SET @p_Error=0
	BEGIN TRY 		
		BEGIN
		IF @p_IsSent = 0
			SELECT U.Id, FirstName, LastName, EmailAddress, ProfilePic, Location
			FROM tblFriendRequests FR
			INNER JOIN tblUser U ON U.Id = FR.UserId
			WHERE ReceivedUserId = @p_UserId
	  ELSE
		SELECT U.Id, FirstName, LastName, EmailAddress, ProfilePic, Location
			FROM tblFriendRequests FR
			INNER JOIN tblUser U ON U.Id = FR.ReceivedUserId
			WHERE UserId = @p_UserId
		END
	 END TRY  
	 BEGIN CATCH
		SELECT	 @p_Error 	= 10012
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	 END CATCH

END 


