GO
/****** Object:  StoredProcedure [dbo].[FlashTagUserInteraction]    Script Date: 07/03/2015 13:27:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[FlashTagUserInteraction]
@p_UserId			BIGINT,
@p_FlashTagImageId	BIGINT,
@p_FlashTagId		BIGINT,
@p_Like				BIT,
@p_DisLike			BIT,
@p_Rating			INT,
@p_Comment			NVARCHAR(MAX),
@p_FlashInteractionParam SMALLINT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: FlashTagUserInteraction 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	BEGIN TRY 	
	BEGIN TRANSACTION
	DECLARE @l_InteractionId BIGINT
	DECLARE @l_CommentId BIGINT

	-- insert or update userinteraction table
	IF @p_FlashInteractionParam = 1 -- For Like
	BEGIN
	IF NOT EXISTS (SELECT 1 FROM tblUserFlashTagInteraction WHERE FlashTagImageId = @p_FlashTagImageId AND UserId = @p_UserId)
	INSERT INTO tblUserFlashTagInteraction(IsLiked,FlashTagImageId,UserId) VALUES(@p_Like,@p_FlashTagImageId,@p_UserId)
	ELSE
	UPDATE tblUserFlashTagInteraction SET IsLiked = @p_Like WHERE FlashTagImageId = @p_FlashTagImageId AND UserId = @p_UserId

	SET @l_InteractionId = SCOPE_IDENTITY()
	END

	ELSE IF  @p_FlashInteractionParam = 4 -- For Dislike
	BEGIN
	IF NOT EXISTS (SELECT 1 FROM tblUserFlashTagInteraction WHERE FlashTagImageId = @p_FlashTagImageId AND UserId = @p_UserId)
	INSERT INTO tblUserFlashTagInteraction(IsDisLiked,FlashTagImageId,UserId) VALUES(@p_DisLike,@p_FlashTagImageId,@p_UserId)
	ELSE
	UPDATE tblUserFlashTagInteraction SET IsDisLiked = @p_DisLike WHERE FlashTagImageId = @p_FlashTagImageId AND UserId = @p_UserId

	SET @l_InteractionId = SCOPE_IDENTITY()
	END

	ELSE IF  @p_FlashInteractionParam = 2 -- For Rating
	BEGIN
	IF NOT EXISTS (SELECT 1 FROM tblUserFlashTagInteraction WHERE FlashTagImageId = @p_FlashTagImageId AND UserId = @p_UserId)
	INSERT INTO tblUserFlashTagInteraction(Rating,FlashTagImageId,UserId) VALUES(@p_Rating,@p_FlashTagImageId,@p_UserId)
	ELSE
	UPDATE tblUserFlashTagInteraction SET Rating = @p_Rating WHERE FlashTagImageId = @p_FlashTagImageId AND UserId = @p_UserId

	SET @l_InteractionId = SCOPE_IDENTITY()
	END

	ELSE IF  @p_FlashInteractionParam =  3 -- For Comment
	BEGIN
	--IF NOT EXISTS (SELECT 1 FROM tblFlashTagComments WHERE FlashTagId = @p_FlashTagId AND UserId = @p_UserId)
	INSERT INTO tblFlashTagComments(Comment,FlashTagId,UserId,CommentDate) VALUES(@p_Comment,@p_FlashTagId,@p_UserId,GETUTCDATE())
	--ELSE
	--UPDATE tblFlashTagComments SET Comment = @p_Comment WHERE FlashTagId = @p_FlashTagId AND UserId = @p_UserId

	SET @l_CommentId = SCOPE_IDENTITY()
	END
	
	INSERT INTO tblUserFlashTagActivity ( FlashTagInteractionId ,FlashTagCommentId,FlashTagId,UpdatedTime) VALUES(@l_InteractionId,@l_CommentId,@p_FlashTagId,GETUTCDATE())

	--SELECT COUNT(IsLiked)AS LikedCount,  COUNT(IsDisLiked)AS DisLikedCount,ISNULL(AVG(Rating),0) AS Rating FROM tblUserFlashTagInteraction 
	--WHERE FlashTagImageId= @p_FlashTagImageId  GROUP BY FlashTagImageId
	
	--SELECT COUNT(Id) AS CommentCount FROM tblFlashTagComments WHERE FlashTagId = @p_FlashTagId

	--SELECT U.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblFlashTagComments  C 
	--JOIN tblUser AS U ON C.UserId = U.Id
	--WHERE FlashTagId = @p_FlashTagId

	SELECT F.Id AS FlashtagId, Question, EndTime, UserId, CreatedTime, ModifiedTime,F.Location, 
			U.Id, U.FirstName, U.LastName, U.ProfilePic, U.Location AS UserLocation,

	(SELECT '<FlashTagImages>' + 
	
	(SELECT FTI.Id, FlashtagImageUrl,'FALSE' AS IsReceipt,
	 ISNULL((SELECT SUM(CASE WHEN UTFI.IsLiked IS NULL OR UTFI.IsLiked = 0 THEN 0 ELSE 1 END)),0) AS LikedCount,
	 ISNULL((SELECT SUM(CASE WHEN UTFI.IsDisLiked IS NULL OR UTFI.IsDisLiked = 0 THEN 0 ELSE 1 END)),0) AS DisLikedCount,
	 ISNULL(AVG(UTFI.Rating), 0) AS Rating,
	 ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId  = FTI.Id AND UserID = @p_UserID),0) AS IsLikedByUser,
	 ISNULL((SELECT ISNULL(IsDisLiked, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId = FTI.Id AND UserID = @p_UserID),0) AS IsDisLikedByUser,
	 ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFlashTagInteraction WHERE FlashTagImageId =  FTI.Id AND UserID = @p_UserID),0) AS RatingByUser
	FROM tblFlashTagImageData FTI 
	LEFT OUTER JOIN tblUserFlashTagInteraction UTFI ON UTFI.FlashTagImageId = FTI.Id 
	WHERE FTI.FlashTagId = F.Id	GROUP BY FTI.Id,FlashtagImageUrl FOR XML PATH('Images')) + '</FlashTagImages>') AS FlashTagImages,
	
	(SELECT '<FeedComments>' + (SELECT U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblFlashTagComments  C 
	JOIN tblUser AS U ON C.UserId = U.Id WHERE C.FlashTagId = F.Id ORDER BY C.CommentDate DESC FOR XML PATH('Comments')) + '</FeedComments>') AS FeedComments,
		
	(SELECT COUNT(Id) FROM tblFlashTagComments WHERE FlashTagId = F.Id) AS CommentCount,
	(SELECT COUNT(Id) FROM tblFlashTagComments WHERE FlashTagId = F.Id AND UserID = @p_UserID) AS CommentCountByUser,
	STUFF((SELECT ', ' + CAST(SharedUserIds AS VARCHAR(10)) [text()]
         FROM tblFlashTagShares  
         WHERE FlashtagId = F.Id
         FOR XML PATH(''), TYPE)
        .value('.','NVARCHAR(MAX)'),1,2,' ') SharedUserIds
	FROM tblFlashTags F	
	INNER JOIN tblUser U ON U.Id = F.UserId
	WHERE 
	F.Id = @p_FlashTagId


	COMMIT TRANSACTION
	END TRY  
	BEGIN CATCH
	ROLLBACK TRANSACTION
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
