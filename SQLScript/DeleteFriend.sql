
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[DeleteFriend]
@p_UserID		INT,
@p_FriendId		INT,
@p_Error		INT OUTPUT
AS
BEGIN
	
	SET NOCOUNT ON;
    BEGIN TRY
		
		IF EXISTS (SELECT 1 FROM tblFriends WHERE UserID = @p_UserID AND FriendId = @p_FriendId)
			BEGIN
				DELETE FROM tblFriends WHERE UserID = @p_UserID AND FriendId = @p_FriendId
			END
		ELSE
			SET @p_Error = 10011			
    END TRY
	BEGIN CATCH 
		SELECT	 @p_Error 	= 10010
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	END CATCH 
   
END
