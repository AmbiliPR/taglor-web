GO
/****** Object:  StoredProcedure [dbo].[FeedUserInteraction]    Script Date: 07/03/2015 13:32:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[FeedUserInteraction]
@p_UserId			BIGINT,
@p_FeedId			BIGINT,
@p_Like				BIT,
@p_Rating			INT,
@p_Comment			NVARCHAR(MAX),
@p_FeedInteractionParam SMALLINT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: FeedUserInteraction 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	BEGIN TRY 	
	BEGIN TRANSACTION
	DECLARE @l_InteractionId BIGINT
	DECLARE @l_CommentId BIGINT

	-- insert or update userinteraction table
	IF @p_FeedInteractionParam = 1 -- For Like
	BEGIN
	IF NOT EXISTS (SELECT 1 FROM tblUserFeedInteraction WHERE FeedId = @p_FeedId AND UserId = @p_UserId)
	INSERT INTO tblUserFeedInteraction(IsLiked,FeedId,UserId) VALUES(@p_Like,@p_FeedId,@p_UserId)
	ELSE
	UPDATE tblUserFeedInteraction SET IsLiked = @p_Like WHERE FeedId = @p_FeedId AND UserId = @p_UserId

	SET @l_InteractionId = SCOPE_IDENTITY()
	END

	ELSE IF  @p_FeedInteractionParam = 2 -- For Rating
	BEGIN
	IF NOT EXISTS (SELECT 1 FROM tblUserFeedInteraction WHERE FeedId = @p_FeedId AND UserId = @p_UserId)
	INSERT INTO tblUserFeedInteraction(Rating,FeedId,UserId) VALUES(@p_Rating,@p_FeedId,@p_UserId)
	ELSE
	UPDATE tblUserFeedInteraction SET Rating = @p_Rating WHERE FeedId = @p_FeedId AND UserId = @p_UserId

	SET @l_InteractionId = SCOPE_IDENTITY()
	END

	ELSE IF  @p_FeedInteractionParam =  3 -- For Comment
	BEGIN
	--IF NOT EXISTS (SELECT 1 FROM tblComment WHERE FeedId = @p_FeedId AND UserId = @p_UserId)
	INSERT INTO tblComment(Comment,FeedId,UserId,CommentDate) VALUES(@p_Comment,@p_FeedId,@p_UserId,GETUTCDATE())
	--ELSE
	--UPDATE tblComment SET Comment = @p_Comment WHERE FeedId = @p_FeedId AND UserId = @p_UserId

	SET @l_CommentId = SCOPE_IDENTITY()
	END
	
	INSERT INTO tblUserActivity(UserFeedInteractionId,CommentId,FeedId,DateTime) VALUES(@l_InteractionId,@l_CommentId,@p_FeedId,GETUTCDATE())

	--SELECT COUNT(IsLiked)AS LikedCount,ISNULL(AVG(Rating),0) AS Rating FROM tblUserFeedInteraction 
	--WHERE FeedId = @p_FeedId GROUP BY FeedId
	
	--SELECT COUNT(Id) AS CommentCount FROM tblComment WHERE FeedId = @p_FeedId

	--SELECT U.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblComment  C 
	--JOIN tblUser AS U ON C.UserId = U.Id
	--WHERE FeedId = @p_FeedId

	SELECT F.Id AS FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber,F.PostTypeId, F.PrivacySettingsId, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic,U.Location AS UserLocation,
	(SELECT '<FeedImages>' + (SELECT Id, ImageUrl, CASE WHEN (IsReceipt IS NULL OR IsReceipt=0) THEN 'False' ELSE 'True' END AS IsReceipt  FROM tblImageData WHERE FeedId = F.Id
				FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
	(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
					INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
					WHERE FT.FeedId = F.Id
				FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
		(SELECT '<FeedComments>' + (SELECT  U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblComment  C 
	JOIN tblUser AS U ON C.UserId = U.Id WHERE C.FeedId = @p_FeedId ORDER BY C.CommentDate DESC FOR XML PATH('Comments')) + '</FeedComments>') AS FeedComments,

	ISNULL((SELECT SUM(CASE WHEN IsLiked IS NULL OR IsLiked = 0 THEN 0 ELSE 1 END) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS LikedCount,
	ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.Id),0) AS Rating,
	ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID), 0) AS IsLikedByUser,
	ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.Id AND UserID = @p_UserID), 0) AS RatingByUser,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id) AS CommentCount,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.Id AND UserID = @p_UserID) AS CommentCountByUser
	FROM tblFeed F
	LEFT JOIN tblFeedDetails FD ON F.Id = FD.FeedId
	INNER JOIN tblUser U ON U.Id = F.UserId
	WHERE F.Id  = @p_FeedId
	

	COMMIT TRANSACTION
	END TRY  
	BEGIN CATCH
	ROLLBACK TRANSACTION
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
