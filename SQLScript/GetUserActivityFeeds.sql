GO
/****** Object:  StoredProcedure [dbo].[GetUserActivityFeeds]    Script Date: 07/03/2015 13:25:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
ALTER PROCEDURE [dbo].[GetUserActivityFeeds]
@p_UserId			BIGINT,
@p_PageIndex		INT,
@p_PageSize			SMALLINT,
@p_CommentCount		SMALLINT,
@p_LastUpdateTime	DATETIME = NULL OUTPUT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Get User Activity feeds
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	
	
	DECLARE @FeedTable AS TABLE
	(
		FeedId			INT,
		Title			VARCHAR(50),
		[Description]	VARCHAR(MAX),
		Location		VARCHAR(MAX),
		UserId			INT,
		PostTypeId		SMALLINT,
		PrivacySettingsId SMALLINT,
		CreatedTime		DATETIME,
		ModifiedTime	DATETIME,		
		RowID			INT		
	)	
		
	BEGIN TRY 	
	BEGIN
	
	IF @p_LastUpdateTime IS NULL
	BEGIN
		INSERT INTO @FeedTable
	
		SELECT Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get feeds of user friends */
			SELECT Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId, ModifiedTime, CreatedTime			
			FROM tblFeed 
			WHERE UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_UserId) AND WishList = 0
			UNION
			/* Get public feeds of user followers  */
			SELECT Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId, ModifiedTime, CreatedTime			
			FROM tblFeed
			WHERE UserId IN (SELECT FollowerId FROM tblFollower WHERE UserId = @p_UserId) AND PrivacySettingsId = 1 AND WishList = 0
			UNION
			/* Get user feeds */			
			SELECT Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime			
			FROM tblFeed
			WHERE UserId = @p_UserId AND WishList = 0
		)AS  FTABLE
	END
	ELSE
	BEGIN	
	INSERT INTO @FeedTable
	
		SELECT Id, Title, [Description], Location, UserId, PostTypeId, PrivacySettingsId, ModifiedTime, CreatedTime, ROW_NUMBER() OVER(ORDER BY FTABLE.Id DESC) AS RowID FROM (
			/* Get feeds of user friends */
			SELECT Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime			
			FROM tblFeed 
			WHERE UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_UserId)AND WishList = 0
			UNION
			/* Get public feeds of user followers  */
			SELECT Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime			
			FROM tblFeed
			WHERE UserId IN (SELECT FollowerId FROM tblFollower WHERE UserId = @p_UserId) AND PrivacySettingsId = 1 AND WishList = 0
			UNION
			/* Get user feeds */
			SELECT Id, Title, [Description], Location, UserId, PostTypeId,PrivacySettingsId, ModifiedTime, CreatedTime			
			FROM tblFeed 
			WHERE UserId = @p_UserId AND WishList = 0	
		)AS  FTABLE			
		WHERE Id IN (SELECT DISTINCT FeedId FROM tblUserActivity WHERE [DateTime] > @p_LastUpdateTime) 
		OR ModifiedTime > @p_LastUpdateTime
	END
			
	--SELECT * FROM 	@FeedTable
	SELECT F.FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber,F.PostTypeId, F.PrivacySettingsId, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic,U.Location AS UserLocation,
	(SELECT '<FeedImages>' + (SELECT Id, ImageUrl, CASE WHEN (IsReceipt IS NULL OR IsReceipt=0) THEN 'False' ELSE 'True' END AS IsReceipt  FROM tblImageData WHERE FeedId = F.FeedId
				FOR XML PATH('Images')) + '</FeedImages>') AS FeedImages,
	(SELECT '<FeedTags>' + (SELECT T.Id, Tags FROM tblTags T 
					INNER JOIN tblFeedTags FT ON T.Id = FT.TagId
					WHERE FT.FeedId = F.FeedId
				FOR XML PATH('Tag')) + '</FeedTags>') AS FeedTags,
	(SELECT '<FeedComments>' + (SELECT TOP (@p_CommentCount) U.Id AS UserId,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,C.Id, Comment, CommentDate FROM tblComment  C 
	JOIN tblUser AS U ON C.UserId = U.Id WHERE C.FeedId = F.FeedId ORDER BY C.CommentDate DESC FOR XML PATH('Comments')) + '</FeedComments>') AS FeedComments,
	ISNULL((SELECT SUM(CASE WHEN IsLiked IS NULL OR IsLiked = 0 THEN 0 ELSE 1 END) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId),0) AS LikedCount,
	ISNULL((SELECT AVG(Rating) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId),0) AS Rating,
	ISNULL((SELECT ISNULL(IsLiked, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS IsLikedByUser,
	ISNULL((SELECT ISNULL(Rating, 0) FROM tblUserFeedInteraction WHERE FeedId = F.FeedId AND UserID = @p_UserID), 0) AS RatingByUser,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId) AS CommentCount,
	(SELECT COUNT(Id) FROM tblComment WHERE FeedId = F.FeedId AND UserID = @p_UserID) AS CommentCountByUser,
	F.ModifiedTime
	FROM @FeedTable F
	LEFT JOIN tblFeedDetails FD ON F.FeedId = FD.FeedId
	INNER JOIN tblUser U ON U.Id = F.UserId
	LEFT OUTER JOIN tblUserActivity UA ON UA.FeedId = F.FeedId
	WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
	GROUP BY F.FeedId, Title, [Description], F.Location, Brand, Model, SerialNumber,F.PostTypeId, F.PrivacySettingsId, PurchasedFrom, Price, Warranty, ExpiryDate, CreatedTime,
	U.Id, U.FirstName, U.LastName, U.ProfilePic,U.Location,F.ModifiedTime
	ORDER BY F.ModifiedTime DESC
	
	DECLARE @UserActivityUpdateDate DATETIME
	DECLARE @FeedModifiedDate DATETIME
		
	SELECT TOP 1 @UserActivityUpdateDate = [DateTime] FROM tblUserActivity ORDER BY [DateTime] DESC
	SELECT TOP 1 @FeedModifiedDate = ModifiedTime FROM tblFeed ORDER BY ModifiedTime DESC
	
	IF @UserActivityUpdateDate > @FeedModifiedDate
		SET @p_LastUpdateTime = @UserActivityUpdateDate
	ELSE 
		SET @p_LastUpdateTime = @FeedModifiedDate
	
	END
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH				   
END
