DROP PROCEDURE [dbo].[GetUpdateCount]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUpdateCount]
@p_UserId			BIGINT,
@p_LastUpdatedTime  DATETIME, 
@p_UpdateCount		INT OUTPUT,
@p_Error			INT	OUTPUT
AS
/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: FeedUserInteraction 
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
06/07/2015
************************************************************************************************/
BEGIN
	
	SET NOCOUNT ON;
	SET @p_Error=0
	BEGIN TRY 	


	IF @p_LastUpdatedTime IS NULL
	SELECT @p_UpdateCount = COUNT(DISTINCT FeedTable.Id) FROM(
			/* Get feeds of user friends */
			SELECT Id, CreatedTime		
			FROM tblFeed 
			WHERE UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_UserId) AND WishList = 0
			UNION
			/* Get public feeds of user followers  */
			SELECT Id,CreatedTime			
			FROM tblFeed
			WHERE UserId IN (SELECT FollowerId FROM tblFollower WHERE UserId = @p_UserId) AND PrivacySettingsId = 1 AND WishList = 0
			UNION
			/* Get user feeds */			
			SELECT Id,CreatedTime			
			FROM tblFeed
			WHERE UserId = @p_UserId AND WishList = 0) AS FeedTable
			LEFT OUTER JOIN tblUserActivity UA ON FeedTable.Id = UA.FeedId
	ELSE

		SELECT @p_UpdateCount = COUNT(DISTINCT FeedTable.Id) FROM(
			/* Get feeds of user friends */
			SELECT Id, CreatedTime		
			FROM tblFeed 
			WHERE UserId IN (SELECT FriendId FROM tblFriends WHERE UserId = @p_UserId) AND WishList = 0
			UNION
			/* Get public feeds of user followers  */
			SELECT Id,CreatedTime			
			FROM tblFeed
			WHERE UserId IN (SELECT FollowerId FROM tblFollower WHERE UserId = @p_UserId) AND PrivacySettingsId = 1 AND WishList = 0
			UNION
			/* Get user feeds */			
			SELECT Id,CreatedTime			
			FROM tblFeed
			WHERE UserId = @p_UserId AND WishList = 0) AS FeedTable
			LEFT OUTER JOIN tblUserActivity UA ON FeedTable.Id = UA.FeedId
			WHERE FeedTable.CreatedTime > @p_LastUpdatedTime
	END TRY  
	BEGIN CATCH
	SELECT	@p_Error = ERROR_NUMBER()
	SELECT  ERROR_MESSAGE()
	END CATCH						   
END
