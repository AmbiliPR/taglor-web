-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[DeleteFriendRequest]
/****** Object:  StoredProcedure [dbo].[DeleteFriendRequest] Date: 29/06/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteFriendRequest] 
(     
	@p_UserId			BIGINT,	
	@p_FriendRequestId	BIGINT,
	@p_Error			INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 		 
	---Step 1
	
		IF EXISTS (SELECT 1 FROM tblFriendRequests WHERE ReceivedUserId = @p_FriendRequestId AND UserId = @p_UserID)
			BEGIN
				DELETE FROM tblFriendRequests WHERE ReceivedUserId = @p_FriendRequestId AND UserId = @p_UserID
			END
		ELSE
			SET @p_Error = 10011
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		RETURN 50001
	END CATCH
END

