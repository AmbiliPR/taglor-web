/****** Object:  StoredProcedure [dbo].[ValidateUser]    Script Date: 02/04/2015 00:54:20 ******/
DROP PROCEDURE [dbo].[ValidateUser] 
DROP PROCEDURE [dbo].ValidateFacebookUser  
DROP PROCEDURE [dbo].SignUpUser
DROP PROCEDURE [dbo].CheckUser

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ValidateUser]       
(     
	@p_EmailAddress 		NVARCHAR(50),	 
	@p_Password			NVARCHAR(250), 
	@p_Error			INT			 OUTPUT
)      
AS   

/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: To validate user credentials
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  
10000				: Invalid User Credentials

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
25/05/2015
************************************************************************************************/
     
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ERRID	INT
	-- Error Handling declarations End 
	SET @p_Error=0
	BEGIN TRY 		
		BEGIN
		
			SELECT 
				Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location
			FROM 
				tblUser  
			WHERE	
				(EmailAddress = @p_EmailAddress  AND Password = @p_Password	)

			IF @@ROWCOUNT =0
				SELECT	 @p_Error 	= 10001
		END
	 END TRY  
	 BEGIN CATCH
		SELECT	 @p_Error 	= 10001
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	 END CATCH

END /* [ValidateUser] */

/****** Object:  StoredProcedure [dbo].[ValidateFacebookUser]    Script Date: 02/04/2015 00:54:20 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].ValidateFacebookUser       
(     
	@p_FacebookUserId 		NVARCHAR(50),	 
	@p_EmailAddress 		NVARCHAR(250), 
	@p_FirstName 			NVARCHAR(50),
	@p_LastName				NVARCHAR(50),
	@p_ProfilePicUrl		NVARCHAR(50),
	@p_UserName				NVARCHAR(50),
	@p_Location				NVARCHAR(50),
	@p_Error			INT			 OUTPUT
)      
AS   

/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: To validate user credentials when logging in with facebook id.
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  
10002				: Invalid User Credentials

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
25/05/2015
************************************************************************************************/
     
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ERRID	INT
	-- Error Handling declarations End 
	SET @p_Error=0
	BEGIN TRANSACTION
	BEGIN TRY 		
		BEGIN
		DECLARE @l_NewId INT
		SET @l_NewId = 0
			SELECT 
				@l_NewId = Id
			FROM 
				tblUser  
			WHERE	
				(EmailAddress = @p_EmailAddress  AND TokenId  = @p_FacebookUserId	)

			IF @@ROWCOUNT =0
			BEGIN
				INSERT INTO tblUser ( EmailAddress, TokenId, FirstName, LastName, UserName, ProfilePic,Location) VALUES (@p_EmailAddress, @p_FacebookUserId, @p_FirstName, @p_LastName, @p_UserName, @p_ProfilePicUrl,@p_Location ) 
			SET @l_NewId = SCOPE_IDENTITY()
			END
			ELSE
			BEGIN
			IF ISNULL(@p_ProfilePicUrl,'')<>''
			UPDATE tblUser SET ProfilePic = @p_ProfilePicUrl WHERE Id = @l_NewId 
			
			IF ISNULL(@p_Location,'')<>''
			UPDATE tblUser SET Location = @p_Location WHERE Id = @l_NewId 
			
			IF ISNULL(@p_UserName,'')<>''
			UPDATE tblUser SET UserName = @p_UserName WHERE Id = @l_NewId 
			
			UPDATE tblUser SET EmailAddress = @p_EmailAddress, FirstName = @p_FirstName, LastName = @p_LastName WHERE Id = @l_NewId 
			END
			SELECT 
				Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,Password
			FROM 
				tblUser  
			WHERE Id = @l_NewId
				
		END
	 COMMIT TRANSACTION
	 END TRY  

	 BEGIN CATCH
		SELECT	 @p_Error 	= 10001
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		ROLLBACK TRANSACTION
	 END CATCH

END /* [ValidateFacebookUser] */
--------------------------------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[SignUpUser]    Script Date: 02/04/2015 00:54:20 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].SignUpUser       
(     
	@p_EmailAddress 		NVARCHAR(50),	 
	@p_Password				NVARCHAR(250), 
	@p_FirstName 			NVARCHAR(50),
	@p_LastName				NVARCHAR(50),
	@p_UserName				NVARCHAR(50),
	@p_Location				NVARCHAR(50),
	@p_ProfilePicUrl		NVARCHAR(MAX),
	@p_BannerImageUrl		NVARCHAR(MAX),
	@p_Error			INT			 OUTPUT
)      
AS   

/**********************************************************************************************
Project/Module	    : Taglor
Purpose				: Signup user into taglor system
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  
10002				: User already exists

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
25/05/2015
************************************************************************************************/
     
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ERRID	INT
	-- Error Handling declarations End 
	SET @p_Error=0

	BEGIN TRY 		
		BEGIN TRANSACTION
		BEGIN
		DECLARE @l_NewId INT
		SET @l_NewId = 0
		INSERT INTO tblUser ( EmailAddress, FirstName, LastName, UserName, Password, ProfilePic, BannerImage, Location) VALUES (@p_EmailAddress, @p_FirstName, @p_LastName, @p_UserName, @p_Password, @p_ProfilePicUrl, @p_BannerImageUrl, @p_Location) 		
		SET @l_NewId = SCOPE_IDENTITY()
		SELECT 
				Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location
			FROM 
				tblUser  
			WHERE Id = @l_NewId
		END
	 COMMIT TRANSACTION	
	 END TRY  
	 BEGIN CATCH
		SELECT	 @p_Error 	= 10004
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	 END CATCH

END /* [SignUpUser] */
GO
--------------------------------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[CheckUser]    Script Date: 02/04/2015 00:54:20 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].CheckUser       
(     
	@p_EmailAddress 		NVARCHAR(50),	 	
	@p_FaceBookTokenId		NVARCHAR(50),
	@p_IsFaceBook			BIT,
	@p_UserID			INT			 OUTPUT,
	@p_Error			INT			 OUTPUT
)      
AS   

/**********************************************************************************************
Project/Module	    : Taglor
Purpose				:Checks for user already exists in Taglor system
Inputs taken        :

Outputs returned    :

ErrorId				: Error message       
-------------------------------------------------------------------------------------------  
10002				: User already exists

Return values       : 0-Success
Notes               :                        

************************************************************************************************
Change History
************************************************************************************************
Date			Author				Revision    Description
------------------------------------------------------------------------------------------------
25/05/2015
************************************************************************************************/
     
BEGIN  
	SET NOCOUNT ON       
	-- Error Handling declarations Start  
	DECLARE @l_ErrMsg	VARCHAR(1000)
	DECLARE @l_ErrProc	VARCHAR(50)
	DECLARE @l_ERRID	INT
	-- Error Handling declarations End 
	SET @p_Error=0
	BEGIN TRY 		
		BEGIN
		  IF @p_IsFaceBook = 0
		   BEGIN
			SELECT 
				@p_UserID  = Id
			FROM 
				tblUser  
			WHERE	
				(EmailAddress = @p_EmailAddress)
			IF @@ROWCOUNT=0
			SELECT @p_UserID = 0
			END
			ELSE
			BEGIN
			SELECT 
				@p_UserID  = Id
			FROM 
				tblUser  
			WHERE	
				(TokenId = @p_FaceBookTokenId)	
			IF @@ROWCOUNT=0
			SELECT @p_UserID = 0

			END
		END
	 END TRY  
	 BEGIN CATCH
		SELECT	 @p_Error 	= 10004
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
	 END CATCH

END /* [SignUpUser] */
-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[GetUserProfile]
/****** Object:  StoredProcedure [dbo].[[GetUserProfile]] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetUserProfile] 
(     
	@p_UserId		INT,
	@p_LoggedUserId	INT,
	@p_IsFriend BIT OUTPUT,
	@p_IsFollowing BIT OUTPUT,
	@p_Error		INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 	
		SELECT FRIENDCOUNT = COUNT(FriendId) FROM tblFriends WHERE UserId = @p_UserId
	SELECT FOLLOWINGCOUNT = COUNT( FollowerId) FROM tblFollower WHERE UserId = @p_UserId
	SELECT FOLLOWERCOUNT = COUNT(UserId) FROM tblFollower WHERE FollowerId = @p_UserId	 
	SELECT Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location
	FROM tblUser WHERE Id = @p_UserId
	IF(@p_UserId != @p_LoggedUserId)
	BEGIN
			IF(@p_UserId IN( SELECT FriendId FROM tblFriends WHERE UserId = @p_LoggedUserId))
			SET @p_IsFriend = 1
			ELSE
			SET @p_IsFriend = 0 
			
			IF(@p_UserId IN( SELECT FollowerId FROM tblFollower WHERE UserId = @p_LoggedUserId))
			SET @p_IsFollowing = 1
			ELSE
			SET @p_IsFollowing = 0

	END
			
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		RETURN 50001
	END CATCH
END

-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[ChangePassword]
/****** Object:  StoredProcedure [dbo].[[ChangePassword]] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ChangePassword] 
(     
	@p_UserId		INT,
	@p_ExistingPassword	NVARCHAR(MAX),
	@p_NewPassword	NVARCHAR(MAX),
	@p_Error		INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 	
	BEGIN TRANSACTION
	DECLARE @l_ExistingPassword NVARCHAR(MAX)
	SELECT @l_ExistingPassword = Password FROM tblUser WHERE Id = @p_UserId
	IF(@l_ExistingPassword = @p_ExistingPassword)
	UPDATE tblUser SET Password = @p_NewPassword WHERE Id = @p_UserId
	ELSE
	SET @p_Error =20001

	COMMIT TRANSACTION
    END TRY  
    BEGIN CATCH
	ROLLBACK TRANSACTION
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		RETURN 50001
	END CATCH
END
-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[ResetPassword]
/****** Object:  StoredProcedure [dbo].[ResetPassword] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ResetPassword] 
(     
	@p_EmailAddress		NVARCHAR(50),
	@p_NewPassword	NVARCHAR(MAX),
	@p_Error		INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 	
	BEGIN TRANSACTION
	UPDATE tblUser SET Password = @p_NewPassword WHERE EmailAddress = @p_EmailAddress
	COMMIT TRANSACTION
    END TRY  
    BEGIN CATCH
	ROLLBACK TRANSACTION
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		RETURN 50001
	END CATCH
END

-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[UpdateUserProfile]
/****** Object:  StoredProcedure [dbo].[UpdateUserProfile] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateUserProfile] 
(  
    @p_UserId				INT,
	@p_EmailAddress 		NVARCHAR(50),	 
	@p_Password				NVARCHAR(250), 
	@p_FirstName 			NVARCHAR(50),
	@p_LastName				NVARCHAR(50),
	@p_UserName				NVARCHAR(50),
	@p_Location				NVARCHAR(50),
	@p_ProfilePicUrl		NVARCHAR(MAX),
	@p_BannerImageUrl		NVARCHAR(MAX),
	@p_IsRemovedProfilePic BIT,
	@p_IsRemovedBannerPic   BIT,
	@p_Error		INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 	
	BEGIN TRANSACTION
	UPDATE tblUser SET EmailAddress = @p_EmailAddress, FirstName = @p_FirstName, LastName = @p_LastName, Password = @p_Password, UserName = @p_UserName,Location=@p_Location WHERE Id = @p_UserId
	
	IF( @p_IsRemovedBannerPic = 1 OR @p_IsRemovedProfilePic = 1)
	BEGIN
		IF( @p_IsRemovedBannerPic = 1)
		UPDATE tblUser SET BannerImage=NULL WHERE Id = @p_UserId

		IF( @p_IsRemovedProfilePic = 1)
		UPDATE tblUser SET ProfilePic=NULL WHERE Id = @p_UserId
	END
	ELSE 
	BEGIN
		IF( ISNULL(@p_ProfilePicUrl,'')<>'')
		UPDATE tblUser SET ProfilePic=@p_ProfilePicUrl WHERE Id = @p_UserId

		IF( ISNULL(@p_BannerImageUrl,'')<>'')
		UPDATE tblUser SET BannerImage=@p_BannerImageUrl WHERE Id = @p_UserId
	END
	SELECT 	Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location FROM tblUser WHERE Id = @p_UserId

	COMMIT TRANSACTION
    END TRY  
    BEGIN CATCH
	ROLLBACK TRANSACTION
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		RETURN 50001
	END CATCH
END
-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[UpdateUserSettings]
/****** Object:  StoredProcedure [dbo].[UpdateUserSettings] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateUserSettings] 
(     
	@p_UserId		INT,
	@p_PrivacySettings	INT,
	@p_Error		INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 	
	BEGIN TRANSACTION	
	UPDATE tblUserSettings SET PrivacySettingsId = @p_PrivacySettings WHERE UserId  = @p_UserId
	IF(@@ROWCOUNT = 0)
	INSERT INTO tblUserSettings(UserId,PrivacySettingsId) VALUES(@p_UserId,@p_PrivacySettings)
	COMMIT TRANSACTION
    END TRY  
    BEGIN CATCH
	ROLLBACK TRANSACTION
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		RETURN 50001
	END CATCH
END

-------------------------------------------------------
GO
DROP PROCEDURE [dbo].[GetFriendsOrFollowersList]
/****** Object:  StoredProcedure [dbo].[GetFriendsOrFollowersList] Date: 31/05/2015 00:54:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFriendsOrFollowersList] 
(     
	@p_UserId			INT			,
	@p_UserRelation		INT,
	@p_PageIndex		INT,
	@p_PageSize			SMALLINT,
	@p_LoggedInUserId	INT,
	@p_Error			INT OUTPUT
)           
AS   
    
BEGIN  
	SET NOCOUNT ON       
	BEGIN TRY 		 
	---Step 1
	DECLARE @UserTable AS TABLE
	(
	Id BIGINT,
	FirstName VARCHAR(50),
	LastName VARCHAR(50),
	UserName VARCHAR(50),
	ProfilePic VARCHAR(50),
	BannerImage VARCHAR(50),
	EmailAddress VARCHAR(50),
	Location NVARCHAR(MAX),
	RowID			INT
	)
		IF(@p_UserRelation = 1)
		INSERT INTO @UserTable
		SELECT 	TU.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location, ROW_NUMBER() OVER(ORDER BY TU.Id DESC) AS RowID FROM tblUser TU 
		WHERE TU.Id IN (SELECT TF.FriendId FROM tblFriends TF WHERE TF.UserId = @p_UserId)
		ELSE IF(@p_UserRelation = 3)
		INSERT INTO @UserTable
		SELECT 	TU.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,ROW_NUMBER() OVER(ORDER BY TU.Id DESC) AS RowID FROM tblUser TU 
		WHERE TU.Id IN (SELECT TF.FollowerId FROM tblFollower TF WHERE TF.UserId = @p_UserId)
		ELSE IF(@p_UserRelation = 2)
		INSERT INTO @UserTable
		SELECT 	TU.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location,ROW_NUMBER() OVER(ORDER BY TU.Id DESC) AS RowID FROM tblUser TU 
		WHERE TU.Id IN (SELECT TF.UserId FROM tblFollower TF WHERE TF.FollowerId = @p_UserId)

		SELECT UT.Id,FirstName,LastName,UserName,ProfilePic,BannerImage,EmailAddress,Location, CASE WHEN (F.FriendId IS NULL) THEN 'False'ELSE 'True' END AS IsFriend,CASE WHEN (FO.FollowerId IS NULL) THEN 'False'ELSE 'True' END AS IsFollower, RowID FROM @UserTable AS UT
		LEFT OUTER JOIN tblFriends F ON ( F.FriendId = UT.Id AND F.UserId = @p_LoggedInUserId)
		LEFT OUTER JOIN tblFollower FO ON (FO.FollowerId = UT.Id AND FO.UserId = @p_LoggedInUserId)
		WHERE RowID BETWEEN ((@p_PageIndex - 1) * @p_PageSize + 1) AND (@p_PageIndex * @p_PageSize)
    END TRY  
    BEGIN CATCH
		SELECT  ERROR_NUMBER(),ERROR_MESSAGE()
		SET @p_Error =50001
		RETURN 50001
	END CATCH
END

