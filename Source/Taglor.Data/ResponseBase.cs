﻿using Taglor.Data.Utils;

namespace Taglor.Data
{
    public class ResponseBase
    {
        public string ErrorCode { get; set; }
        public ResponseStatus ErrorStatus { get; set; }
        public string ErrorDescription { get; set; }
    }
}