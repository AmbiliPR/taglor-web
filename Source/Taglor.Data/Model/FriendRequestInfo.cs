﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class FriendRequestInfo
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string ProfilePic { get; set; }
        public string Location { get; set; }
    }

    public class FriendRequestsList : ResponseBase
    {
        public List<FriendRequestInfo> FriendRequests { get; set; }
    }
}
