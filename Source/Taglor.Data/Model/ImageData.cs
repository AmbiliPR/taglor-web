﻿namespace Taglor.Data.Model
{
    /// <summary>
    ///     Image data that needs to be uploaded into Taglor corresponding to a feed.
    /// </summary>
    public class UploadImageParams
    {
        /// <summary>
        ///     Id of the feed corresponding to an image.
        /// </summary>
        public int FeedId { get; set; }

        /// <summary>
        ///     Image data.
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is flash tag.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is flash tag; otherwise, <c>false</c>.
        /// </value>
        public bool IsFlashTag { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is receipt.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is receipt; otherwise, <c>false</c>.
        /// </value>
        public bool IsReceipt { get; set; }
    }

    public class FeedImage
    {
        public int ImageId { get; set; }
        public string ImageUrl { get; set; }
        public bool IsReceipt { get; set; }

    }

    public class FlashTagImage : FeedImage
    {
        public int LikedCount { get; set; }
        public int DislikedCount { get; set; }
        public int Rating { get; set; }
        public bool IsLikedByLoggedUser { get; set; }
        public bool IsDislikedByLoggedUser { get; set; }
        public int RatingByLoggedUser { get; set; }
    }
}