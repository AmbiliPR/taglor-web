﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taglor.Data.Utils;

namespace Taglor.Data.Model
{
    public class FeedInteractionParams
    {
        public int UserId { get; set; }
        public int FeedId { get; set; }
        public bool IsLike { get; set; }
        public int Rating { get; set; }
        public string Comment { get; set; }
        public UserInteractionType UserInteraction { get; set; }
    }
}
