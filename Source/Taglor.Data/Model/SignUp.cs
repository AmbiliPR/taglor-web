﻿namespace Taglor.Data.Model
{
    public class SignUp
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public string ProfilePic { get; set; }
        public string BannerImage { get; set; }
        public string Location { get; set; }

        /// <summary>
        ///     This will set by Service layer
        /// </summary>
        public string ProfilePicUrl { get; set; }

        /// <summary>
        ///     This will set by service layer
        /// </summary>
        public string BannerImageUrl { get; set; }
    }
}