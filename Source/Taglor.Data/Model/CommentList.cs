﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class CommentList : ResponseBase
    {
        public IList<FeedComment> Comments { get; set; }
    }
}
