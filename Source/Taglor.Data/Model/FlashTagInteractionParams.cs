﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class FlashTagInteractionParams : FeedInteractionParams
    {
        public int ImageId { get; set; }
        public bool Dislike { get; set; }
    }
}
