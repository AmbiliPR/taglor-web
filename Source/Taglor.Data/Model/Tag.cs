﻿namespace Taglor.Data.Model
{
    public class Tag
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}