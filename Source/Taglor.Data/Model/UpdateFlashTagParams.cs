﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class UpdateFlashTagParams
    {
        public int FeedId { get; set; }
        public string Question { get; set; }
        public DateTime EndTime { get; set; }
        public string SharedUserIds { get; set; }
        public string Location { get; set; }
    }
}
