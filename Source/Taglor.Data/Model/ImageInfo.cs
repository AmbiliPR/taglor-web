﻿namespace Taglor.Data.Model
{
    public class ImageInfo : ResponseBase
    {
        public int ImageId { get; set; }
    }
}