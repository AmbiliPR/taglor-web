﻿using System;

namespace Taglor.Data.Model
{
    public class FeedDetails
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Serial { get; set; }
        public string PurchasedFrom { get; set; }
        public string PurchasePrice { get; set; }
        public string Warranty { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}