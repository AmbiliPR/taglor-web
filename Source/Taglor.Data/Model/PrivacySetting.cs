﻿using Taglor.Data.Utils;

namespace Taglor.Data.Model
{
    public class PrivacySetting
    {
        public PrivacySettings Privacy { get; set; }
        public int Id { get; set; }
    }
}