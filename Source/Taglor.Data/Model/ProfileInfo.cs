﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taglor.Data.Utils;

namespace Taglor.Data.Model
{
    public class ProfileInfo : ResponseBase
    {
        public int FriendCount { get; set; }
        public int FollowerCount { get; set; }
        public int FollowingCount { get; set; }
        public bool IsFriend { get; set; }
        public bool IsFollowing { get; set; }
        public LoginInfo UserInfo { get; set; }
    }
}
