﻿namespace Taglor.Data.Model
{
    /// <summary>
    ///     Holds the data that needs to be returned to application.
    ///     UserId is the unique key identifier for a logged in user and its value shall be greated than 0.
    /// </summary>
    public class LoginInfo : ResponseBase
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string ProfilePicUrl { get; set; }
        public string BannerImageUrl { get; set; }
        public string Location { get; set; }
    }
}