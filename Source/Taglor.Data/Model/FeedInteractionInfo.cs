﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class FeedInteractionInfo : ResponseBase
    {
        public int FeedId { get; set; }
        public int LikeCount { get; set; }
        public int Rating { get; set; }
        public int CommentCount { get; set; }
        public IList<FeedComment> FeedComments { get; set; }
    }
}
