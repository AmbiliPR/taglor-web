﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class TagList : ResponseBase
    {
        public IList<Tag> Tags { get; set; }
    }
}
