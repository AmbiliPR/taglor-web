﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class UserList : ResponseBase
    {
        public IEnumerable<ProfileInfo> UserInfos { get; set; }
    }
}
