﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class UpdateUserProfileParams
    {
        public int UserId { get; set; }
        public bool IsProfileImageRemoved { get; set; }
        public bool IsBannerImageRemoved { get; set; }
        public SignUp UserProfile { get; set; }
    }
}
