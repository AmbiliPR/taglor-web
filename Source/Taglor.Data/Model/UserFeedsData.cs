﻿using System;
using System.Collections.Generic;
using Taglor.Data.Utils;

namespace Taglor.Data.Model
{
    public class UserFeedsData
    {
        public int FeedId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Serial { get; set; }
        public string PurchasedFrom { get; set; }
        public string PurchasePrice { get; set; }
        public string Warranty { get; set; }
        public DateTime ExpirationDate { get; set; }
        public List<FeedImage> FeedImages { get; set; }
        public List<Tag> FeedTags { get; set; }
        public DateTime CreatedTime { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePic { get; set; }
        public int LikedCount { get; set; }
        public int CommentCount { get; set; }
        public int Rating { get; set; }
        public bool IsLikedByLoggedUser { get; set; }
        public int CommentCountByLoggedUser { get; set; }
        public int RatingByLoggedUser { get; set; }
        public PostType Post { get; set; }
        public PrivacySettings PrivacySetting { get; set; }
        public IList<FeedComment> Comments { get; set; }
    }

    public class UserFeedDataList : ResponseBase
    {
        public List<UserFeedsData> UserFeedsList { get; set; }
        public string LastUpdateDate { get; set; }
    }
}