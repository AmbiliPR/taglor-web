﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class UserInventoryData
    {
        public Tag TagData { get; set; }

        public int TotalFeedsCount { get; set; }
        public List<UserFeedsData> UserFeedsList { get; set; }
    }

    public class UserInventoryList : ResponseBase
    {
        public List<UserInventoryData> UserInventoryFeedList { get; set; }
    }
}
