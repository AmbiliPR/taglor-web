﻿namespace Taglor.Data.Model
{
    public class LoginWithFacebookParams
    {
        public string FacebookUserId { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePic { get; set; }
        public string UserName { get; set; }
    }
}