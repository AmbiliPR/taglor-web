﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class InteractedUsers : ResponseBase
    {
        public IEnumerable<ProfileInfo> UsersList { get; set; }
    }
}
