﻿using System;
using System.Collections.Generic;

namespace Taglor.Data.Model
{
    public class UserFlashtagFeedData
    {
        public int FlashtagdId { get; set; }
        public string Question { get; set; }
        public DateTime EndTime { get; set; }
        public List<FlashTagImage> FeedImages { get; set; }
        public DateTime CreatedTime { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePic { get; set; }      
        public int CommentCount { get; set; }       
        public int CommentCountByLoggedUser { get; set; }
        public string SharedUserIds { get; set; }
        public string Location { get; set; }
        public IList<FeedComment> Comments { get; set; }
    }

    public class UserFlashtagFeedDataList : ResponseBase
    {
        public List<UserFlashtagFeedData> UserFlashtagFeedList { get; set; }
        public string LastUpdateDate { get; set; }
    }
}