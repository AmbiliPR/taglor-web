﻿using System;
using Taglor.Data.Utils;

namespace Taglor.Data.Model
{
    public class NewFeedParams
    {
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TagIds { get; set; }
        public FeedDetails FeedDetails { get; set; }
        public PrivacySettings Settings { get; set; }
        public bool AddToWishlist { get; set; }
        public PostType PostType { get; set; }
        public DateTime EndTime { get; set; }
        public string Question { get; set; }
        public string SharedUserIds { get; set; }
        public string Location { get; set; }
    }
}