﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class FeedComment
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public LoginInfo CommentedUser { get; set; }
        public DateTime CommentedDate { get; set; }
    }
}
