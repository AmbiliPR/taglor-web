﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class SearchResult : ResponseBase
    {
        public IEnumerable<ProfileInfo> Users { get; set; }
        public IEnumerable<UserFeedsData> Inventory { get; set; }
        public IEnumerable<UserFeedsData> WishList { get; set; }
    }
}
