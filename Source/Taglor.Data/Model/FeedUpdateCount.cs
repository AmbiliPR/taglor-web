﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class FeedUpdateCount:ResponseBase
    {
        public int Count { get; set; }
    }
}
