﻿using Taglor.Data.Utils;

namespace Taglor.Data.Model
{
    public class UpdateFeedParams
    {
        public int FeedId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string TagIds { get; set; }
        public FeedDetails FeedDetails { get; set; }
        public PrivacySettings FeedSettings { get; set; }
        public bool AddToWishList { get; set; }
        public PostType PostType { get; set; }
        public string Location { get; set; }
    }
}