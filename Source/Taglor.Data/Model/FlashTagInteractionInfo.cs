﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taglor.Data.Model
{
    public class FlashTagInteractionInfo:FeedInteractionInfo
    {
        public int ImageId { get; set; }
        public int DislikeCount { get; set; }
    }
}
