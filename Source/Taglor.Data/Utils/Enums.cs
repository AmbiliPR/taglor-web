﻿namespace Taglor.Data.Utils
{
    public enum ResponseStatus
    {
        Success = 0,
        BadRequest = 1,
        ServerError = 2
    }

    public enum PrivacySettings
    {
        Private = 1,
        Public = 2,
        Friends = 3
    }

    public enum PostType
    {
        Item = 1,
        Receipt = 2,
        FlashTag = 3
    }

    public enum FeedType
    {
        Activity = 1,
        FlashTag = 2,
        Wishlist = 3,
        Inventory = 4,
        Untagged = 5,
        Profile = 6
    }

    public enum UserRelation
    {
        Friend = 1,
        Follower = 2,
        Following = 3,
        None = 4
    }

    public enum SearchType
    {
        All = 1,
        Users=2,
        Inventory=3,
        Wishlist=4
    }

    public enum UserInteractionType
    {
        Like = 1,
        Rating = 2,
        Comment = 3,
        Dislike = 4
    }
}