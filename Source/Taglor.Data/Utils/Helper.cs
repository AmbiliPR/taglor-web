﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Taglor.Data.Model;

namespace Taglor.Data.Utils
{
    public static class Helper
    {


        // This constant string is used as a "salt" value for the PasswordDeriveBytes function calls.
        // This size of the IV (in bytes) must = (keysize / 8).  Default keysize is 256, so the IV must be
        // 32 bytes long.  Using a 16 character string here gives us 32 bytes when converted to a byte array.
        private static readonly byte[] InitVectorBytes = Encoding.ASCII.GetBytes("T@glOrW1nfy92014#");
        private static readonly string PassPhrase = "T@gl0r";
        // This constant is used to determine the keysize of the encryption algorithm.
        private const int Keysize = 256;

        /// <summary>
        ///     method is used for get error message from resource file.
        /// </summary>
        public static string GetErrorMessage(string code)
        {
            const string messagePrefix = "E";
            var retValue = Resource.ResourceManager.GetString(string.Format("{0}{1}", messagePrefix, code));
            return retValue;
        }

        /// <summary>
        ///     Returns true if the object is null or 0, otherwise false.
        /// </summary>
        public static bool IsNullOrZero(object param)
        {
            if (param == DBNull.Value || param.ToString().Equals("0"))
                return true;
            return false;
        }

        /// <summary>
        ///     Returns the xml element for the the given object.
        /// </summary>
        public static XElement GetXMLFromObject(object obj)
        {
            var xmlSerializer = new XmlSerializer(obj.GetType());

            var sw = new StringWriter();
            var tw = new XmlTextWriter(sw);

            xmlSerializer.Serialize(tw, obj);

            return XElement.Parse(sw.ToString());
        }

        /// <summary>
        ///     Extenstion method for int, to return null if the value is int.Min or int.Max
        /// </summary>
        public static int? GetValueOrNull(this int param)
        {
            if (param == int.MinValue || param == int.MaxValue)
            {
                return null;
            }
            return param;
        }

        /// <summary>
        ///     Extension method for float, to return null if the value is float.Min or float.Max or float.NaN
        /// </summary>
        public static float? GetValueOrNull(this float param)
        {
            if (param == float.MinValue || param == float.MaxValue || param == float.NaN)
            {
                return null;
            }
            return param;
        }

        /// <summary>
        ///     Extension method for DateTime value.
        /// </summary>
        /// <param name="param">The parameter.</param>
        /// <returns></returns>
        public static DateTime? GetValueOrNull(this DateTime param)
        {
            if (param == DateTime.MinValue || param == DateTime.MaxValue)
            {
                return null;
            }
            return param.ToUniversalTime();
        }

        public static string GetValueOrNull(this object param)
        {
            if (param != null && !string.IsNullOrEmpty(param.ToString()))
            {
                return param.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        ///     Saves the image.
        /// </summary>
        /// <param name="base64String">The base64 string.</param>
        /// <param name="fullOutputPath">The full output path.</param>
        public static void SaveImage(this string base64String, string fullOutputPath)
        {
            var bytes = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(bytes))
            {
                // image = Image.FromStream(ms);
                using (var bm2 = new Bitmap(ms))
                {
                    bm2.Save(fullOutputPath, ImageFormat.Jpeg);
                }
            }

            // image.Save(fullOutputPath, System.Drawing.Imaging.ImageFormat.Png);
        }

        /// <summary>
        /// Encrypts the specified plain text.
        /// </summary>
        /// <param name="plainText">The plain text.</param>
        /// <returns></returns>
        public static string Encrypt(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (var password = new PasswordDeriveBytes(PassPhrase, null))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, InitVectorBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor,
                                    CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                var cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts the specified cipher text.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <returns></returns>
        public static string Decrypt(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText)) return string.Empty;
            var cipherTextBytes = Convert.FromBase64String(cipherText);
            using (var password = new PasswordDeriveBytes(PassPhrase, null))
            {
                var keyBytes = password.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, InitVectorBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (
                                var cryptoStream = new CryptoStream(memoryStream, decryptor,
                                    CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Deletes the file.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="profilePicPath">The profile pic path.</param>
        public static int DeleteFile(string emailAddress, string profilePicPath)
        {
            var dirInfo = new DirectoryInfo(profilePicPath);
            int fileindex = 0;
            if (dirInfo.Exists)
            {
                var files = dirInfo.GetFiles();

                if (files.Any())
                {
                    for (var index = 0; index < files.Count(); index++)
                    {
                        var fileToDelete = files[index];
                        if (fileToDelete.Name.Contains(emailAddress))
                        {
                            // Get the fileIndex
                            fileindex = Convert.ToInt32(fileToDelete.Name.Split('_')[0]);

                            fileToDelete.Delete();
                        }
                    }
                }
            }
            return fileindex;
        }

        /// <summary>
        /// Gets the u sers.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns></returns>
        public static IEnumerable<ProfileInfo> GetUSers(DataTable dataTable)
        {
            IList<ProfileInfo> list = new List<ProfileInfo>();
            foreach (DataRow row in dataTable.Rows)
                list.Add(new ProfileInfo
                {
                    UserInfo = new LoginInfo
                    {

                        UserId = Convert.ToInt32(row["Id"]),
                        FirstName = row["FirstName"].GetValueOrNull(),
                        LastName = row["LastName"].GetValueOrNull(),
                        UserName = row["UserName"].GetValueOrNull(),
                        EmailAddress = row["EmailAddress"].GetValueOrNull(),
                        ProfilePicUrl = !string.IsNullOrEmpty(row["ProfilePic"].ToString()) ? Path.Combine(Constants.Server_ProfilePicPath, row["ProfilePic"].ToString()) : string.Empty,
                        BannerImageUrl = !string.IsNullOrEmpty(row["BannerImage"].ToString()) ? Path.Combine(Constants.Server_BannerPicPath, row["BannerImage"].ToString()) : string.Empty,
                        Location = row["Location"].GetValueOrNull()
                    },
                    IsFollowing = Convert.ToBoolean(row["IsFollower"]),
                    IsFriend = Convert.ToBoolean(row["IsFriend"])
                });
            return list;
        }
    }
}