﻿namespace Taglor.Data.Utils
{
    public static class Constants
    {
        public static readonly string Server_ProfilePicPath = "/ProfilePic";
        public static readonly string Server_BannerPicPath = "/BannerPic";
        public static readonly string Server_FeedPicPath = "/FeedPics";
        public static readonly string Server_FlashTagPicPath = "/FlashTagPics";
        public static readonly int UnExpectedErrorCode = 10000;
        public static readonly string ProfilePicExtn = "_Profile.jpeg";
        public static readonly string BannerPicExtn = "_BannerImage.jpeg";
        public static readonly string FeedPicExtn = ".jpeg";

    }
}