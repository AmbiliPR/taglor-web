﻿using System;
using System.Text;
using log4net;

namespace Taglor.Data.Utils
{
    public static class Logger
    {
        private static ILog logger = LogManager.GetLogger("Taglor");

        /// <summary>
        ///     Create Logger
        /// </summary>
        /// <param name="processName"></param>
        public static void CreateLogger(string processName)
        {
            if (logger == null)
            {
                logger = LogManager.GetLogger(processName);
            }
        }

        /// <summary>
        ///     Logs information
        /// </summary>
        public static void Info(string message)
        {
            if (logger == null)
            {
                return;
            }

            logger.Info(message);
        }

        /// <summary>
        ///     Logs debug info
        /// </summary>
        public static void Debug(string message)
        {
            if (logger == null)
            {
                return;
            }

            if (logger.IsDebugEnabled)
                logger.Debug(message);
        }

        /// <summary>
        ///     Logs warning message
        /// </summary>
        public static void Warning(string message)
        {
            if (logger == null)
            {
                return;
            }
            logger.Warn(message);
        }

        /// <summary>
        ///     Logs exception as warning
        /// </summary>
        public static void Warning(Exception ex)
        {
            if (logger == null)
            {
                return;
            }
            logger.Warn(CreateExceptionMessage(ex));
        }

        /// <summary>
        ///     Logs error message
        /// </summary>
        public static void Error(string message)
        {
            if (logger == null)
            {
                return;
            }
            logger.Error(message);
        }

        /// <summary>
        ///     Logs error exception
        /// </summary>
        public static void Error(Exception ex)
        {
            if (logger == null)
            {
                return;
            }
            logger.Error(CreateExceptionMessage(ex));
        }

        /// <summary>
        ///     Formats exception message
        /// </summary>
        private static string CreateExceptionMessage(Exception ex)
        {
            var buffer = new StringBuilder();
            buffer.Append(ex.Message).Append(Environment.NewLine);
            buffer.Append("----------").Append(Environment.NewLine);
            buffer.Append(ex).Append(Environment.NewLine);
            buffer.Append("----------").Append(Environment.NewLine);
            buffer.Append(ex.StackTrace).Append(Environment.NewLine);
            buffer.Append("----------").Append(Environment.NewLine);
            return buffer.ToString();
        }
    }
}