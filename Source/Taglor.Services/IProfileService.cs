﻿using System.Collections.Generic;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;

namespace Taglor.Services
{
    /// <summary>
    ///     Interface for signup
    /// </summary>
    public interface IProfileService
    {
        /// <summary>
        ///     Sign up user for Taglor
        /// </summary>
        LoginInfo SignUp(SignUp signUpData, string profilnnerPicPath, string bannerPicPath);

        /// <summary>
        ///     Changes the password.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        ResponseBase ChangePassword(int userId, string oldPassword, string newPassword);

        /// <summary>
        ///     Resets the password.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="generatedPassword"></param>
        /// <returns></returns>
        ResponseBase ResetPassword(string emailAddress, string generatedPassword);

        /// <summary>
        /// Save user friend request.
        /// </summary>
        /// <param name="userId">LoggedIn Userid</param>
        /// <param name="receivedUserId">Friend UserId</param>
        /// <returns></returns>
        ResponseBase SaveFriendRequest(int userId, string receivedUserIds);

        /// <summary>
        /// Accept or Reject Friend request
        /// </summary>
        /// <param name="loggedInUserId"></param>
        /// <param name="requestedUserId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        ResponseBase AcceptOrRejectFriendRequest(int loggedInUserId, string requestedUserIds, bool status);

        /// <summary>
        /// Get All friend requests for user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        FriendRequestsList GetFriendRequests(int userId, bool isSent);

        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <returns></returns>
        ProfileInfo GetUserProfile(int userId, int loggedInUserId);

        /// <summary>
        /// Updates the user profile.
        /// </summary>
        /// <param name="userProfileParams">The user profile parameters.</param>
        /// <param name="profilePicPath">The profile pic path.</param>
        /// <param name="bannerPicPath">The banner pic path.</param>
        /// <returns></returns>
        LoginInfo UpdateUserProfile(UpdateUserProfileParams userProfileParams, string profilePicPath, string bannerPicPath);

        /// <summary>
        /// Updates the user settings.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        ResponseBase UpdateUserSettings(int userId, PrivacySettings setting);

        /// <summary>
        /// Gets the friends list.
        /// </summary>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <param name="inUserId"></param>
        /// <param name="userRelation"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isFriendList"></param>
        /// <returns></returns>
        UserList GetFriendsOrFollowersList(int loggedInUserId, int userId, UserRelation userRelation, int pageIndex, int pageSize);

        /// <summary>
        /// Follow or Unfollow a user
        /// </summary>
        /// <param name="userId">LoggedIn userid</param>
        /// <param name="followingUserId">UserId of following profile</param>
        /// <param name="followStatus">Follow status - True -> Follow, False - Unfollow</param>
        /// <returns></returns>
        ResponseBase FollowUnFollowUser(int userId, int followerUserID, bool followStatus);

        /// <summary>
        /// Delete a friend
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        ResponseBase DeleteFriend(int userId, int friendId);

        ResponseBase DeleteFriendRequest(int userId, int friendRequestId);
    }
}