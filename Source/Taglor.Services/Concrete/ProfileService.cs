﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;
using Taglor.DataAccess;

namespace Taglor.Services.Concrete
{
    public class ProfileService : IProfileService
    {
        private readonly IProfileDA myProfileDA;

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProfileService" /> class.
        /// </summary>
        /// <param name="signupDA">The signup da.</param>
        public ProfileService(IProfileDA signupDA)
        {
            myProfileDA = signupDA;
        }

        /// <summary>
        ///     Signs up.
        /// </summary>
        /// <param name="signUpData">The sign up data.</param>
        /// <param name="profilePicPath">The profile pic path.</param>
        /// <param name="bannerPicPath">The banner pic path.</param>
        /// <returns></returns>
        public LoginInfo SignUp(SignUp signUpData, string profilePicPath, string bannerPicPath)
        {
            LoginInfo info = null;
            try
            {
                if (0 == myProfileDA.IsUserExists(false, signUpData.EmailAddress))
                {
                    var profilePic = signUpData.ProfilePic;
                    var bannerImage = signUpData.BannerImage;
                    var profileFileName = string.Empty;
                    var bannerFileName = string.Empty;

                    // If signup is successfull then save the image data to the server
                    profileFileName = SaveUserImage(signUpData, profilePicPath, bannerPicPath, profilePic, profileFileName, bannerImage, ref bannerFileName);
                    signUpData.Password = Helper.Encrypt(signUpData.Password);
                    info = myProfileDA.SignUp(signUpData, profileFileName, bannerFileName);
                }
                else
                {
                    info = new LoginInfo
                    {
                        UserId = 0,
                        ErrorDescription = "User already exists",
                        ErrorStatus = ResponseStatus.BadRequest
                    };
                }
            }
            catch (Exception ex)
            {
                if (info == null) info = new LoginInfo();
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                info.ErrorCode = "0";
            }

            return info;
        }

        /// <summary>
        /// Saves the user image.
        /// </summary>
        /// <param name="signUpData">The sign up data.</param>
        /// <param name="profilePicPath">The profile pic path.</param>
        /// <param name="bannerPicPath">The banner pic path.</param>
        /// <param name="profilePic">The profile pic.</param>
        /// <param name="profileFileName">Name of the profile file.</param>
        /// <param name="bannerImage">The banner image.</param>
        /// <param name="bannerFileName">Name of the banner file.</param>
        /// <returns></returns>
        private static string SaveUserImage(SignUp signUpData, string profilePicPath, string bannerPicPath, string profilePic,
            string profileFileName, string bannerImage, ref string bannerFileName)
        {           
            if (!string.IsNullOrEmpty(profilePic))
            {
                if (!Directory.Exists(profilePicPath)) Directory.CreateDirectory(profilePicPath);
                int index = Helper.DeleteFile(signUpData.EmailAddress, profilePicPath);
                profileFileName = ++index +"_"+ signUpData.EmailAddress + Constants.ProfilePicExtn;
                var profileImagePath = Path.Combine(profilePicPath, profileFileName);
                profilePic.SaveImage(profileImagePath);
            }

            if (!string.IsNullOrEmpty(bannerImage))
            {
                if (!Directory.Exists(bannerPicPath)) Directory.CreateDirectory(bannerPicPath);
                int index = Helper.DeleteFile(signUpData.EmailAddress, bannerPicPath);
                bannerFileName = ++index + "_" + signUpData.EmailAddress + Constants.BannerPicExtn;
                var bannerImagePath = Path.Combine(bannerPicPath, bannerFileName);
                bannerImage.SaveImage(bannerImagePath);
                //signUpData.BannerImageUrl = fileName;
            }
            return profileFileName;
        }

        /// <summary>
        ///     Updates the profile.
        /// </summary>
        /// <param name="userProfileParams"></param>
        /// <param name="profilePicPath"></param>
        /// <param name="bannerPicPath"></param>
        /// <param name="signUpData">The sign up data.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public LoginInfo UpdateUserProfile(UpdateUserProfileParams userProfileParams, string profilePicPath, string bannerPicPath)
        {
            if (userProfileParams == null) throw new ArgumentNullException("userProfileParams");
            var profilePic = userProfileParams.UserProfile.ProfilePic;
            var bannerImage = userProfileParams.UserProfile.BannerImage;
            var profileFileName = string.Empty;
            var bannerFileName = string.Empty;

            // If signup is successfull then save the image data to the server
            profileFileName = SaveUserImage(userProfileParams.UserProfile, profilePicPath, bannerPicPath, profilePic, profileFileName, bannerImage, ref bannerFileName);
            userProfileParams.UserProfile.Password = Helper.Encrypt(userProfileParams.UserProfile.Password);
            return myProfileDA.UpdateUserProfile(userProfileParams, profileFileName, bannerFileName);
        }

        /// <summary>
        /// Updates the user settings.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        public ResponseBase UpdateUserSettings(int userId, PrivacySettings setting)
        {
            return myProfileDA.UpdateUserSettings(userId, setting);
        }

        /// <summary>
        /// Gets the friends list.
        /// </summary>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <param name="inUserId"></param>
        /// <param name="userRelation"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isFriendList"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public UserList GetFriendsOrFollowersList(int loggedInUserId, int userId, UserRelation userRelation, int pageIndex, int pageSize)
        {
            return myProfileDA.GetFriendsOrFollowersList(userId, loggedInUserId, userRelation, pageIndex,pageSize);
        }


        /// <summary>
        ///     Changes the password.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public ResponseBase ChangePassword(int userId, string oldPassword, string newPassword)
        {
            return myProfileDA.ChangePassword(userId, Helper.Encrypt(oldPassword), Helper.Encrypt(newPassword));
        }

        /// <summary>
        ///     Resets the password.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="generatedPassword"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public ResponseBase ResetPassword(string emailAddress, string generatedPassword)
        {
            return myProfileDA.ResetPassword(emailAddress, Helper.Encrypt(generatedPassword));
        }

        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <returns></returns>
        public ProfileInfo GetUserProfile(int userId, int loggedInUserId)
        {
            return myProfileDA.GetUserProfile(userId, loggedInUserId);
        }

        /// <summary>
        /// Save user friend request.
        /// </summary>
        /// <param name="userId">LoggedIn Userid</param>
        /// <param name="receivedUserId">Friend UserId</param>
        /// <returns></returns>
        public ResponseBase SaveFriendRequest(int userId, string receivedUserIds)
        {
            return myProfileDA.SaveFriendRequest(userId, receivedUserIds);
        }

        /// <summary>
        /// Accept or Reject Friend request
        /// </summary>
        /// <param name="loggedInUserId"></param>
        /// <param name="requestedUserId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public ResponseBase AcceptOrRejectFriendRequest(int loggedInUserId, string requestedUserIds, bool status)
        {
            return myProfileDA.AcceptOrRejectFriendRequest(loggedInUserId, requestedUserIds, status);
        }

        /// <summary>
        /// Get All friend requests for user
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public FriendRequestsList GetFriendRequests(int userId, bool isSent)
        {
            return myProfileDA.GetFriendRequests(userId, isSent);
        }

        /// <summary>
        /// Follow or unfollow a user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="followingUserId"></param>
        /// <param name="followStatus"></param>
        /// <returns></returns>
        public ResponseBase FollowUnFollowUser(int userId, int followerUserID, bool followStatus)
        {
            return myProfileDA.FollowUnFollowUser(userId, followerUserID, followStatus);
        }

        /// <summary>
        /// Delete a friend
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        public ResponseBase DeleteFriend(int userId, int friendId)
        {
            return myProfileDA.DeleteFriend(userId, friendId);
        }

        public ResponseBase DeleteFriendRequest(int userId, int friendRequestId)
        {
            return myProfileDA.DeleteFriendRequest(userId, friendRequestId);
        }
    }
}