﻿using System;
using System.IO;
using Taglor.Data.Model;
using Taglor.Data.Utils;
using Taglor.DataAccess;

namespace Taglor.Services.Concrete
{
    public class LoginService : ILoginService
    {
        private readonly ILoginDA myLoginDA;

        /// <summary>
        ///     Initializes a new instance of the <see cref="LoginService" /> class.
        /// </summary>
        /// <param name="loginDA">The login da.</param>
        public LoginService(ILoginDA loginDA)
        {
            myLoginDA = loginDA;
        }

        /// <summary>
        ///     Validates the user for logging into Taglor system.
        ///     If Validation fails the userid will be 0.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public LoginInfo Login(string emailAddress, string password)
        {
            LoginInfo info = null;
            try
            {
                info = myLoginDA.Login(emailAddress, Helper.Encrypt(password));
            }
            catch (Exception ex)
            {
                if (info == null) info = new LoginInfo();
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                info.ErrorCode = "0";
            }
            return info;
        }

        /// <summary>
        ///     Validates the user which uses Faceboook to login into Taglor.
        ///     Firsttime users will be added to database and corresponding user id shall be returned.
        /// </summary>
        public FacebookLoginInfo LoginWithFacebook(string facebookUserId, string emailAddress, string firstName, string lastName,
            string profilePic, string userName, string serverPath)
        {
            FacebookLoginInfo info = null;
            try
            {
                string fileName = string.Empty;
                if (!string.IsNullOrEmpty(profilePic))
                {
                    if (!Directory.Exists(serverPath)) Directory.CreateDirectory(serverPath);
                    int index = Helper.DeleteFile(emailAddress, serverPath);
                    fileName = ++index + "_" + emailAddress + Constants.ProfilePicExtn;
                    var profileImagePath = Path.Combine(serverPath, fileName);
                    profilePic.SaveImage(profileImagePath);
                }                
                info = myLoginDA.LoginWithFacebook(facebookUserId, emailAddress, firstName, lastName, fileName, userName);
            }
            catch (Exception ex)
            {
                if (info == null) info = new FacebookLoginInfo();
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                info.ErrorCode = "0";
            }
            return info;
        }
    }
}