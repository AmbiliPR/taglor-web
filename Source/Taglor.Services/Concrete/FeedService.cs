﻿using System;
using System.Collections.Generic;
using System.IO;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;
using Taglor.DataAccess;

namespace Taglor.Services.Concrete
{
    public class FeedService : IFeedService
    {
        private readonly IFeedDA myFeedDA;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FeedService" /> class.
        /// </summary>
        /// <param name="feedDA">The feed da.</param>
        /// <param name="isLoggedInUser1"></param>
        public FeedService(IFeedDA feedDA)
        {
            myFeedDA = feedDA;
        }

        /// <summary>
        ///     Gets the tags.
        /// </summary>
        /// <returns></returns>
        public TagList GetTags(int userId)
        {
            return myFeedDA.GetTags(userId);
        }

        /// <summary>
        ///     Gets the privacy settings.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public PrivacySetting GetPrivacySettings(int userId)
        {
            return myFeedDA.GetPrivacySettings(userId);
        }

        /// <summary>
        ///     Deletes the image from feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public ResponseBase DeleteImageFromFeed(int userId, int feedId, int imageId, bool isFlashTag)
        {
            return myFeedDA.DeleteImageFromFeed(userId, feedId, imageId, isFlashTag);
        }

        /// <summary>
        ///     Deletes the feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public ResponseBase DeleteFeed(int feedId, int userId, bool isFlashTag)
        {
            return myFeedDA.DeleteFeed(feedId, userId, isFlashTag);
        }

        /// <summary>
        ///     Updates the flash tags.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="question">The question.</param>
        /// <param name="endTime">The end time.</param>
        /// <returns></returns>
        public ResponseBase UpdateFlashTags(int feedId, string question, DateTime endTime, string sharedUserIds, string location)
        {
            return myFeedDA.UpdateFlashTags(feedId, question, endTime, sharedUserIds, location);
        }

        /// <summary>
        ///     Updates the feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="tagIds">The tag ids.</param>
        /// <param name="feedDetails">The feed details.</param>
        /// <param name="feedSettings">The feed settings.</param>
        /// <param name="addToWishList">if set to <c>true</c> [add to wish list].</param>
        /// <param name="postType">Type of the post.</param>
        /// <returns></returns>
        public ResponseBase UpdateFeed(int feedId, string title, string description, string tagIds,
            FeedDetails feedDetails,
            PrivacySettings feedSettings, bool addToWishList, PostType postType, string location)
        {
            return myFeedDA.UpdateFeed(feedId, title, description, tagIds, feedDetails, feedSettings, addToWishList,
                postType, location);
        }

        /// <summary>
        ///     Creates the new feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="tagIds">The tag ids.</param>
        /// <param name="feedDetails">The feed details.</param>
        /// <param name="settings">The settings.</param>
        /// <param name="addToWishlist">if set to <c>true</c> [add to wishlist].</param>
        /// <param name="postType">Type of the post.</param>
        /// <param name="endTime">The end time.</param>
        /// <param name="question">The question.</param>
        /// <param name="sharedUserIds"></param>
        /// <returns></returns>
        public FeedInfo CreateNewFeed(int userId, string title, string description, string tagIds,
            FeedDetails feedDetails, PrivacySettings settings, bool addToWishlist, PostType postType, DateTime endTime,
            string question, string sharedUserIds,string location)
        {
            return postType != PostType.FlashTag
                ? myFeedDA.CreateNewFeed(userId, title, description, tagIds, feedDetails, settings, addToWishlist,
                    postType, location)
                : myFeedDA.CreateFlashTags(userId, question, endTime, sharedUserIds, location);
        }

        /// <summary>
        ///     Uploads the image.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageDataPath">The image data path.</param>
        /// <param name="imageData">The image data.</param>
        /// <returns></returns>
        public ImageInfo UploadImage(int feedId, string imageDataPath, string imageData, bool isFlashTag, bool isReceipt)
        {
            ImageInfo imageInfo = null;
            // If signup is successfull then save the image data to the server
            if (!string.IsNullOrEmpty(imageData))
            {
                var fileName = feedId + "_" + Path.GetRandomFileName() + Constants.FeedPicExtn;
                var feedImagePath = Path.Combine(imageDataPath, fileName);
                if (!Directory.Exists(imageDataPath)) Directory.CreateDirectory(imageDataPath);
                imageData.SaveImage(feedImagePath);
                imageInfo = myFeedDA.UploadImage(feedId, fileName, isFlashTag, isReceipt);
            }
            return imageInfo;
        }

        /// <summary>
        /// Get All User Activity Feeds
        /// User Feeds, User Followers public feeds, user friends feeds.
        /// </summary>
        /// <param name="userId">Logged In UserId</param>
        /// <param name="lastUpdateTime">Last updated time of feed</param>
        /// <param name="pageIndex">PageIndex</param>
        /// <param name="pageSize">Page sise</param>
        /// <param name="feedType"></param>
        /// <param name="isLoggedInUser"></param>
        /// <returns></returns>
        public UserFeedDataList GetUserFeeds(int userId, string lastUpdateTime, int pageIndex, short pageSize,
            short feedType, int loggedInUser, int commentCount)
        {
            return myFeedDA.GetUserFeeds(userId, lastUpdateTime, pageIndex, pageSize, feedType, loggedInUser,commentCount);
        }

        public UserFlashtagFeedDataList GetUserFlashtagFeeds(int userId, string lastUpdateTime, int pageIndex,
            short pageSize, int commentCount)
        {
            return myFeedDA.GetUserFlashtagFeeds(userId, lastUpdateTime, pageIndex, pageSize, commentCount);
        }

        /// <summary>
        /// Get user inventory feeds group by tags
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserInventoryList GetUserInventoryFeeds(int userId)
        {
            return myFeedDA.GetUserInventoryFeeds(userId);
        }

        /// <summary>
        /// Get user inventory feeds by tag id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public UserFeedDataList GetUserInventoryByTag(int userId, int tagId)
        {
            return myFeedDA.GetUserInventoryByTag(userId, tagId);
        }

        /// <summary>
        /// Searches the specified search text.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="searchType">Type of the search.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        public SearchResult Search(string searchText, int userId, SearchType searchType, int pageIndex, int pageSize)
        {
            return myFeedDA.Search(searchText, userId, searchType, pageIndex, pageSize);
        }


        /// <summary>
        /// Gets the update count.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="lastUpdateDateTime">The last update date time.</param>
        /// <returns></returns>
        public FeedUpdateCount GetUpdateCount(int userId, string lastUpdateDateTime)
        {
            return myFeedDA.GetUpdateCount(userId, lastUpdateDateTime);
        }

        /// <summary>
        /// Makes the user feed interaction.
        /// </summary>
        /// <param name="feedInteractionParams">The feed interaction parameters.</param>
        /// <returns></returns>
        public UserFeedDataList MakeUserFeedInteraction(FeedInteractionParams feedInteractionParams)
        {
            return myFeedDA.MakeUserFeedInteraction(feedInteractionParams);
        }

        /// <summary>
        /// Makes the flash tag interaction.
        /// </summary>
        /// <param name="flashTagInteractionParams">The flash tag interaction parameters.</param>
        /// <returns></returns>
        public UserFlashtagFeedDataList MakeFlashTagInteraction(FlashTagInteractionParams flashTagInteractionParams)
        {
            return myFeedDA.MakeFlashTagInteraction(flashTagInteractionParams);
        }

        /// <summary>
        /// Deletes the comment for feed.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public UserFeedDataList DeleteCommentForFeed(int commentId, int userId, int feedId)
        {
          return  myFeedDA.DeleteCommentForFeed(commentId, userId, feedId);
        }

        /// <summary>
        /// Deletes the comment for flash tag.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="flashTagId">The flash tag identifier.</param>
        /// <returns></returns>
        public UserFlashtagFeedDataList DeleteCommentForFlashTag(int commentId, int userId, int flashTagId)
        {
            return myFeedDA.DeleteCommentForFlashTag(commentId, userId, flashTagId);
        }

        /// <summary>
        /// Gets the comments for feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public CommentList GetCommentsForFeed(int feedId, int userId, bool isFlashTag, int pageIndex, int pageSize, string lastUpdatedTime)
        {
           return myFeedDA.GetCommentsForFeed(feedId, userId, isFlashTag,pageIndex, pageSize, lastUpdatedTime);
        }

        /// <summary>
        /// Gets the interacted users from feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="isLike">if set to <c>true</c> [is like].</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public InteractedUsers GetInteractedUsersFromFeed(int userId, int feedId, int imageId, bool isLike,
            bool isFlashTag, int pageIndex, int pageSize)
        {
            return myFeedDA.GetInteractedUsersFromFeed(userId, feedId, imageId, isLike, isFlashTag,pageIndex,pageSize);
        }
    }
}