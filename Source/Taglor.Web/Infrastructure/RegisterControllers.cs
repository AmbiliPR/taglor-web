﻿using System;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using System.Web.Hosting;
using System.Web.Http;

namespace Taglor.Infrastructure
{
    /// <summary>
    /// Configure the container and set the application controller factory and dependency resolver with the MEF framework.
    /// </summary>
    public static class RegisterControllers
    {
        public static void Register(AggregateCatalog catalog)
        {
            var compositionContainer = ConfigureContainer(catalog);
            //ControllerBuilder.Current.SetControllerFactory(new MEFControllerFactory(compositionContainer));
            GlobalConfiguration.Configuration.DependencyResolver = new MEFDependencyResolver(compositionContainer);
        }

        /// <summary>
        /// Configure the container from the given catalog.
        /// If the catalog is null, compose the container from the assembly path.
        /// </summary>
        private static CompositionContainer ConfigureContainer(AggregateCatalog catalog)
        {
            var path = HostingEnvironment.MapPath("~/bin");
            if (path == null) throw new Exception("Unable to find the path");

            var aggregateCatalog = new AggregateCatalog(new DirectoryCatalog(path));
            CompositionContainer container;
            if (catalog != null)
            {
                aggregateCatalog.Catalogs.Add(catalog);
                container = new CompositionContainer(aggregateCatalog);
            }
            else
            {
                var assemblyCatalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
                container = new CompositionContainer(assemblyCatalog);
            }

            return container;
        }
    }
}
