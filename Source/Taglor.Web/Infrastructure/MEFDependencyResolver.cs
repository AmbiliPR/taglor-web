﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Web.Http.Dependencies;

namespace Taglor.Infrastructure
{
    /// <summary>
    /// Resolves web api
    /// </summary>
    public class MEFDependencyResolver : IDependencyResolver
    {
        private readonly CompositionContainer myCompositionContainer;

        public MEFDependencyResolver(CompositionContainer container)
        {
            myCompositionContainer = container;
        }

        /// <see cref="IDependencyResolver.BeginScope"/>
        public IDependencyScope BeginScope()
        {
            return this;
        }

        /// <see cref="IDependencyScope.GetService"/>
        public object GetService(Type type)
        {
            var export = myCompositionContainer
                .GetExports(type, null, null).SingleOrDefault();

            return null != export ? export.Value : null;
        }

        /// <see cref="IDependencyScope.GetServices"/>
        public IEnumerable<object> GetServices(Type type)
        {
            var exports = myCompositionContainer.GetExports(type, null, null);
            var exportList = new List<object>();
            if (exports.Any()) exportList.AddRange(exports.Select(export => export.Value));
            return exportList;
        }

        /// <see cref="IDispoable.Dispose"/>
        public void Dispose()
        {
        }
    }
}
