﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Registration;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Taglor.DataAccess;
using Taglor.Services;

namespace Taglor.Infrastructure
{
    // <summary>
    /// Configuration class for MEF.
    /// Export all controllers and api by-default with Non shared creation policy. 
    /// Register all the services and dataacess interfaces to the catalog.
    /// </summary>
    public static class MEFConfig
    {
        public static void RegisterMEF()
        {
            var registrationBuilder = new RegistrationBuilder();
            registrationBuilder.ForTypesDerivedFrom<Controller>().Export().SetCreationPolicy(CreationPolicy.NonShared);
            registrationBuilder.ForTypesDerivedFrom<ApiController>().Export().SetCreationPolicy(CreationPolicy.NonShared);
            // registrationBuilder.ForTypesDerivedFrom<ICacheHandler>().Export<ICacheHandler>().SetCreationPolicy(CreationPolicy.Shared);

            // Register Services interfaces
            registrationBuilder.ForTypesDerivedFrom<ILoginService>().Export<ILoginService>();
            registrationBuilder.ForTypesDerivedFrom<IProfileService>().Export<IProfileService>();
            registrationBuilder.ForTypesDerivedFrom<IFeedService>().Export<IFeedService>();


            // Register Data Access interfaces
            registrationBuilder.ForTypesDerivedFrom<ILoginDA>().Export<ILoginDA>();
            registrationBuilder.ForTypesDerivedFrom<IProfileDA>().Export<IProfileDA>();
            registrationBuilder.ForTypesDerivedFrom<IFeedDA>().Export<IFeedDA>();

            var aggregateCatalog = new AggregateCatalog();
            aggregateCatalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly(), registrationBuilder));

            aggregateCatalog.Catalogs.Add(new AssemblyCatalog(Assembly.Load("Taglor.Services"), registrationBuilder));
            aggregateCatalog.Catalogs.Add(new AssemblyCatalog(Assembly.Load("Taglor.DataAccess"), registrationBuilder));


            RegisterControllers.Register(aggregateCatalog);
        }
    }
}
