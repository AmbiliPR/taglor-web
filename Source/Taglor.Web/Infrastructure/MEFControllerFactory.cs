﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace Taglor.Infrastructure
{
   /// <summary>
    /// Holds the container which contains details of all assembly that needs to be exported.
    /// </summary>
    public class MEFControllerFactory : DefaultControllerFactory
    {
        private readonly CompositionContainer myCompositionContainer;

        public MEFControllerFactory(CompositionContainer compositionContainer)
        {
            myCompositionContainer = compositionContainer;
        }

        /// <see cref="DefaultControllerFactory.GetControllerInstance"/>
        protected override IController GetControllerInstance( RequestContext requestContext, Type controllerType)
        {
            var export = myCompositionContainer.GetExports(controllerType, null, null).SingleOrDefault();

            IController result;
            if (null != export)
                result = export.Value as IController;
            else
            {
                result = base.GetControllerInstance(requestContext, controllerType);
                CompositionBatch batch = new CompositionBatch();
                batch.AddPart(AttributedModelServices.CreatePart(result));

                // TODO: Composing again is failing needs to check this.
                myCompositionContainer.Compose(batch);
            }

            return result;
        }
    }
}
