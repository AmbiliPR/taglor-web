﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.UI.WebControls;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;
using Taglor.Services;

namespace Taglor.Controllers.API
{
    public class UserController : ApiController
    {
        private readonly ILoginService myLoginService;
        private readonly IProfileService myProfileService;

        public UserController(ILoginService loginService, IProfileService profileService)
        {
            myLoginService = loginService;
            myProfileService = profileService;
        }

        /// <summary>
        ///     Helps user to login to Taglor system.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public LoginInfo Login(string emailAddress, string password)
        {
            if (!ValidateEmailAddress(emailAddress))
            {
                var info = new LoginInfo
                {
                    ErrorStatus = ResponseStatus.BadRequest,
                    ErrorDescription = "Invalid email address."
                };
                return info;
            }
            var loginInfo = myLoginService.Login(emailAddress, password);
            SetImagePath(loginInfo);
            return loginInfo;
        }

        /// <summary>
        /// Sets the image path.
        /// </summary>
        /// <param name="loginInfo">The login information.</param>
        private static void SetImagePath(LoginInfo loginInfo)
        {
            if (null != loginInfo)
            {
                if (!string.IsNullOrEmpty(loginInfo.ProfilePicUrl))
                    loginInfo.ProfilePicUrl =
                        Path.Combine(Constants.Server_ProfilePicPath, loginInfo.ProfilePicUrl);

                if (!string.IsNullOrEmpty(loginInfo.BannerImageUrl))
                    loginInfo.BannerImageUrl =
                        Path.Combine(Constants.Server_BannerPicPath, loginInfo.BannerImageUrl);
            }
        }

        /// <summary>
        ///     Helps to login with facebook account.
        /// </summary>
        /// <param name="loginWithFacebookParams">The login with facebook parameters.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public FacebookLoginInfo LoginWithFacebook(LoginWithFacebookParams loginWithFacebookParams)
        {
            var loginInfo = myLoginService.LoginWithFacebook(loginWithFacebookParams.FacebookUserId,
                loginWithFacebookParams.EmailAddress, loginWithFacebookParams.FirstName,
                loginWithFacebookParams.LastName, loginWithFacebookParams.ProfilePic, loginWithFacebookParams.UserName,
                HttpContext.Current.Server.MapPath(Constants.Server_ProfilePicPath));
            SetImagePath(loginInfo);
            return loginInfo;
        }

        /// <summary>
        ///     Sign up into Taglor system.
        /// </summary>
        /// <param name="signUpData">The sign up data.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public LoginInfo SignUp(SignUp signUpData)
        {
            var signUpInfo = myProfileService.SignUp(signUpData,
                HttpContext.Current.Server.MapPath(Constants.Server_ProfilePicPath),
                HttpContext.Current.Server.MapPath(Constants.Server_BannerPicPath));

            SetImagePath(signUpInfo);
            return signUpInfo;
        }

        /// <summary>
        /// Save Friend Request
        /// </summary>
        /// <param name="userId">LoggedIn UserId</param>
        /// <param name="receivedUserId">Friend UserId</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase SaveFriendRequest(int userId, string receivedUserIds)
        {
            return myProfileService.SaveFriendRequest(userId, receivedUserIds);
        }

        /// <summary>
        /// Accept or Reject friend request.
        /// </summary>
        /// <param name="loggedInUserId"></param>
        /// <param name="requestedUserId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase AcceptOrRejectFriendRequest(int loggedInUserId, string requestedUserIds, bool status)
        {
            return myProfileService.AcceptOrRejectFriendRequest(loggedInUserId, requestedUserIds, status);
        }

        /// <summary>
        /// Get user pending friend requests.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public FriendRequestsList GetFriendRequests(int userId, bool isSent)
        {
            return myProfileService.GetFriendRequests(userId,isSent);
        }

        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public ProfileInfo GetUserProfile(int userId, int loggedInUserId)
        {
            var info = myProfileService.GetUserProfile(userId, loggedInUserId);
            SetImagePath(info.UserInfo);
            return info;
        }


        /// <summary>
        ///     Changes the password.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase ChangePassword(int userId, string oldPassword, string newPassword)
        {
            return myProfileService.ChangePassword(userId, oldPassword, newPassword);
        }

        /// <summary>
        ///     Resets the password.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase ResetPassword(string emailAddress)
        {
            ResponseBase responseBase;
            try
            {
                if (!ValidateEmailAddress(emailAddress))
                {
                    responseBase = new ResponseBase
                                {
                                    ErrorStatus = ResponseStatus.BadRequest,
                                    ErrorDescription = "Invalid email address."
                                };
                }
                else
                {
                    string password = Membership.GeneratePassword(8, 2);
                    responseBase = myProfileService.ResetPassword(emailAddress, password);
                    if (responseBase.ErrorStatus == ResponseStatus.Success)
                    {
                        // TODO: Send email part has to be done.
                        SendMail(emailAddress);
                    }
                }

            }
            catch (Exception ex)
            {
                responseBase = new ResponseBase
                {
                    ErrorStatus = ResponseStatus.ServerError,
                    ErrorDescription = ex.Message
                };
            }
            return responseBase;
        }

        /// <summary>
        /// Updates the user profile.
        /// </summary>
        /// <param name="userProfileParams">The user profile parameters.</param>
        /// <returns></returns>
        public LoginInfo UpdateUserProfile(UpdateUserProfileParams userProfileParams)
        {
            var info = myProfileService.UpdateUserProfile(userProfileParams,
                HttpContext.Current.Server.MapPath(Constants.Server_ProfilePicPath),
                HttpContext.Current.Server.MapPath(Constants.Server_BannerPicPath));
            SetImagePath(info);
            return info;
        }

        /// <summary>
        /// Updates the user settings.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        public ResponseBase UpdateUserSettings(int userId, PrivacySettings setting)
        {
            return myProfileService.UpdateUserSettings(userId, setting);
        }

        /// <summary>
        /// Gets the friends list.
        /// </summary>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <param name="userRelation">if set to <c>true</c> [is friend list].</param>
        /// <returns></returns>
        public UserList GetFriendsOrFollowersList(int userId, int loggedInUserId, UserRelation userRelation, int pageIndex, int pageSize)
        {
            return myProfileService.GetFriendsOrFollowersList(userId, loggedInUserId, userRelation,pageIndex,pageSize);
        }

        /// <summary>
        /// Sends the mail.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        private void SendMail(string emailAddress)
        {

        }

        /// <summary>
        /// Validates the email address.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <returns></returns>
        private static bool ValidateEmailAddress(string emailAddress)
        {
            var emailAttribute = new EmailAddressAttribute();
            return emailAttribute.IsValid(emailAddress);
        }

        /// <summary>
        /// Follow or Unfollow a user
        /// </summary>
        /// <param name="userId">LoggedIn userid</param>
        /// <param name="followingUserId">UserId of following profile</param>
        /// <param name="followStatus">Follow status - True -> Follow, False - Unfollow</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase FollowUnFollowUser(int userId, int followerUserID, bool followStatus)
        {
            return myProfileService.FollowUnFollowUser(userId, followerUserID, followStatus);
        }

        /// <summary>
        /// Delete a friend
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="friendId"></param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase UnFriendUser(int userId, int friendId)
        {
            return myProfileService.DeleteFriend(userId, friendId);
        }

        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase DeleteFriendRequest(int userId, int friendRequestId)
        {
            return myProfileService.DeleteFriendRequest(userId, friendRequestId);
        }
    }
}