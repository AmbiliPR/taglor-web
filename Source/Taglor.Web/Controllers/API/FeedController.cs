﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;
using Taglor.Services;

namespace Taglor.Controllers.API
{
    public class FeedController : ApiController
    {
        private readonly IFeedService myFeedService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FeedController" /> class.
        /// </summary>
        /// <param name="feedService">The feed service.</param>
        public FeedController(IFeedService feedService)
        {
            myFeedService = feedService;
        }

        /// <summary>
        ///     Creates the new feed.
        /// </summary>
        /// <param name="newFeedParams">The new feed parameters.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public FeedInfo CreateNewFeed([FromBody] NewFeedParams newFeedParams)
        {
            return myFeedService.CreateNewFeed(newFeedParams.UserId, newFeedParams.Title, newFeedParams.Description,
                newFeedParams.TagIds, newFeedParams.FeedDetails, newFeedParams.Settings, newFeedParams.AddToWishlist,
                newFeedParams.PostType, newFeedParams.EndTime, newFeedParams.Question, newFeedParams.SharedUserIds, newFeedParams.Location);
        }

        /// <summary>
        ///     Uploads the image.
        /// </summary>
        /// <param name="imageData">The image data.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ImageInfo UploadImage(UploadImageParams uploadImageParams)
        {
            return myFeedService.UploadImage(uploadImageParams.FeedId,
                uploadImageParams.IsFlashTag
                    ? HttpContext.Current.Server.MapPath(Constants.Server_FlashTagPicPath)
                    : HttpContext.Current.Server.MapPath(Constants.Server_FeedPicPath), uploadImageParams.Data, uploadImageParams.IsFlashTag, uploadImageParams.IsReceipt);
        }

        /// </summary>
        /// Get All User Feeds 
        /// Activity => User Feeds, User Followers public feeds, user friends feeds.
        /// Wishlist => User feeds which is added to wishlist.
        /// Untagged => User feeds which is not tagged.
        /// <param name="userId">Logged In UserId</param>
        /// <param name="lastUpdateTime">Last updated time of feed</param>
        /// <param name="pageIndex">PageIndex</param>
        /// <param name="pageSize">Page sise</param>
        /// <param name="feedType">FeedType => 1 - Activity, 2 - Wishlist, 3 - Untagged</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserFeedDataList GetUserFeeds(int userId, string lastUpdateTime, int pageIndex, short pageSize,
            short feedType, int loggedInUser,int commentCount)
        {
            return myFeedService.GetUserFeeds(userId, lastUpdateTime, pageIndex, pageSize, feedType, loggedInUser,commentCount);
        }

        /// <summary>
        ///     Get user flashtag feeds
        /// </summary>
        /// <param name="userId">Logged In userId</param>
        /// <param name="lastUpdateTime">Last updated time of flashtag</param>
        /// <param name="pageIndex">Page Index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserFlashtagFeedDataList GetUserFlashtagFeeds(int userId, string lastUpdateTime, int pageIndex,
            short pageSize, int commentCount)
        {
            return myFeedService.GetUserFlashtagFeeds(userId, lastUpdateTime, pageIndex, pageSize,commentCount);
        }

        /// <summary>
        /// Gets the tags.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public TagList GetTags(int userId)
        {
            return myFeedService.GetTags(userId);
        }

        /// <summary>
        /// Gets the update count.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="lastUpdateDateTime">The last update date time.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public FeedUpdateCount GetUpdateCount(int userId, string lastUpdateDateTime)
        {
            return myFeedService.GetUpdateCount(userId, lastUpdateDateTime);
        }

        /// <summary>
        ///     Gets the privacy settings.
        /// </summary>
        /// <returns></returns>
        public PrivacySetting GetPrivacySettings(int userId)
        {
            return myFeedService.GetPrivacySettings(userId);
        }

        /// <summary>
        ///     Deletes the image from feed.
        /// </summary>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase DeleteImageFromFeed(int userId, int feedId, int imageId, bool isFlashTag)
        {
            return myFeedService.DeleteImageFromFeed(userId, feedId, imageId, isFlashTag);
        }

        /// <summary>
        ///     Deletes the feed.
        /// </summary>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase DeleteFeed(int feedId, int userId, bool isFlashTag)
        {
            return myFeedService.DeleteFeed(feedId, userId, isFlashTag);
        }

        /// <summary>
        ///     Updates the flash tags.
        /// </summary>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase UpdateFlashTags([FromBody]UpdateFlashTagParams flashTagParams)
        {
            return myFeedService.UpdateFlashTags(flashTagParams.FeedId, flashTagParams.Question, flashTagParams.EndTime, flashTagParams.SharedUserIds, flashTagParams.Location);
        }

        /// <summary>
        ///     Updates the feed.
        /// </summary>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public ResponseBase UpdateFeed([FromBody] UpdateFeedParams updateFeedParams)
        {
            return myFeedService.UpdateFeed(updateFeedParams.FeedId, updateFeedParams.Title,
                updateFeedParams.Description, updateFeedParams.TagIds, updateFeedParams.FeedDetails,
                updateFeedParams.FeedSettings, updateFeedParams.AddToWishList,
                updateFeedParams.PostType, updateFeedParams.Location);
        }

        //[AcceptVerbs("GET", "POST")]
        //[HttpPost]
        //public ResponseBase MakeUserInteraction()
        //{
            
        //}

        /// <summary>
        /// Get User Inventory group by tags
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserInventoryList GetUserInventoryFeeds(int userId)
        {
            return myFeedService.GetUserInventoryFeeds(userId);
        }

        /// <summary>
        /// Get user inventory feeds by tag id
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserFeedDataList GetUserInventoryByTag(int userId, int tagId)
        {
            return myFeedService.GetUserInventoryByTag(userId, tagId);
        }

        /// <summary>
        /// Searches the specified search text.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="searchType">Type of the search.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public SearchResult Search(string searchText, int userId, SearchType searchType, int pageIndex, int pageSize)
        {
            return myFeedService.Search(searchText, userId, searchType, pageIndex, pageSize);
        }

        /// <summary>
        /// Makes the user feed interaction.
        /// </summary>
        /// <param name="feedInteractionParams">The feed interaction parameters.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserFeedDataList MakeUserFeedInteraction(FeedInteractionParams feedInteractionParams)
        {
            return myFeedService.MakeUserFeedInteraction(feedInteractionParams);
        }

        /// <summary>
        /// Makes the flash tag interaction.
        /// </summary>
        /// <param name="flashTagInteractionParams">The flash tag interaction parameters.</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserFlashtagFeedDataList MakeFlashTagInteraction(FlashTagInteractionParams flashTagInteractionParams)
        {
            return myFeedService.MakeFlashTagInteraction(flashTagInteractionParams);
        }

        /// <summary>
        /// Deletes the comment for feed.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserFeedDataList DeleteCommentForFeed(int commentId, int userId, int feedId)
        {
            return myFeedService.DeleteCommentForFeed(commentId, userId,feedId);
        }

        /// <summary>
        /// Deletes the comment for feed.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpPost]
        public UserFlashtagFeedDataList DeleteCommentForFlashTag(int commentId=0, int userId=0, int flashTagId=0)
        {
            return myFeedService.DeleteCommentForFlashTag(commentId, userId, flashTagId);
        }

        /// <summary>
        /// Gets the comments for feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public CommentList GetCommentsForFeed(int feedId=0, int userId=0, bool isFlashTag=false, int pageIndex=1, int pageSize=5, string lastUpdatedTime="")
        {
            return myFeedService.GetCommentsForFeed(feedId, userId, isFlashTag,pageIndex,pageSize, lastUpdatedTime);
        }

        /// <summary>
        /// Gets the interacted users from feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="isLike">if set to <c>true</c> [is like].</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        [AcceptVerbs("GET", "POST")]
        [HttpGet]
        public InteractedUsers GetInteractedUsersFromFeed(int userId=0, int feedId=0, int? imageId=0, bool isLike=false, bool isFlashTag=false, int pageIndex=1, int pageSize=5)
        {
            return myFeedService.GetInteractedUsersFromFeed(userId, feedId, imageId ?? 0, isLike, isFlashTag, pageIndex, pageSize);
        }
    }
}