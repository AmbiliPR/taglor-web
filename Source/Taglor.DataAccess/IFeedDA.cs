﻿using System;
using System.Collections.Generic;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;

namespace Taglor.DataAccess
{
    public interface IFeedDA
    {
        /// <summary>
        ///     Gets the tags.
        /// </summary>
        /// <returns></returns>
        TagList GetTags(int userId);

        /// <summary>
        ///     Gets the privacy settings.
        /// </summary>
        /// <returns></returns>
        PrivacySetting GetPrivacySettings(int userId);

        /// <summary>
        ///     Creates the new feed.
        /// </summary>
        FeedInfo CreateNewFeed(int userId, string title, string description, string tagIds, FeedDetails feedDetails, PrivacySettings settings, bool addToWishlist, PostType postType, string location);

        /// <summary>
        ///     Creates the flash tags.
        /// </summary>
        FeedInfo CreateFlashTags(int userId, string question, DateTime endTime, string sharedUserIds, string location);

        /// <summary>
        ///     Uploads the image.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="isFlashTag"></param>
        /// <param name="isReceipt"></param>
        /// <returns></returns>
        ImageInfo UploadImage(int feedId, string fileName, bool isFlashTag, bool isReceipt);

        /// <summary>
        ///     Deletes the image from feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        ResponseBase DeleteImageFromFeed(int userId, int feedId, int imageId, bool isFlashTag);

        /// <summary>
        ///     Deletes the feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        ResponseBase DeleteFeed(int feedId, int userId, bool isFlashTag);

        /// <summary>
        ///     Updates the flash tags.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="question">The question.</param>
        /// <param name="endTime">The end time.</param>
        /// <param name="sharedUserIds"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        ResponseBase UpdateFlashTags(int feedId, string question, DateTime endTime, string sharedUserIds, string location);

        /// <summary>
        ///     Updates the feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="title">The title.</param>
        /// <param name="description">The description.</param>
        /// <param name="tagIds">The tag ids.</param>
        /// <param name="feedDetails">The feed details.</param>
        /// <param name="feedSettings">The feed settings.</param>
        /// <param name="addToWishList">if set to <c>true</c> [add to wish list].</param>
        /// <param name="postType">Type of the post.</param>
        /// <param name="location"></param>
        /// <returns></returns>
        ResponseBase UpdateFeed(int feedId, string title, string description, string tagIds, FeedDetails feedDetails, PrivacySettings feedSettings, bool addToWishList, PostType postType, string location);

        /// <summary>
        ///     Gets the user feeds.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="lastUpdateTime">The last update time.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="feedType">Type of the feed.</param>
        /// <param name="loggedInUser"></param>
        /// <returns></returns>
        UserFeedDataList GetUserFeeds(int userId, string lastUpdateTime, int pageIndex, short pageSize, short feedType, int loggedInUser,int commentCount);

        /// <summary>
        ///     Gets the user flashtag feeds.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="lastUpdateTime">The last update time.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        UserFlashtagFeedDataList GetUserFlashtagFeeds(int userId, string lastUpdateTime, int pageIndex, short pageSize, int commentCount);

        UserInventoryList GetUserInventoryFeeds(int userId);

        UserFeedDataList GetUserInventoryByTag(int userId, int tagId);

        /// <summary>
        /// Searches the specified search text.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="searchType">Type of the search.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        SearchResult Search(string searchText, int userId, SearchType searchType, int pageIndex, int pageSize);

        /// <summary>
        /// Gets the update count.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="lastUpdateDateTime">The last update date time.</param>
        /// <returns></returns>
        FeedUpdateCount GetUpdateCount(int userId, string lastUpdateDateTime);

        /// <summary>
        /// Makes the user feed interaction.
        /// </summary>
        /// <param name="feedInteractionParams">The feed interaction parameters.</param>
        /// <returns></returns>
        UserFeedDataList MakeUserFeedInteraction(FeedInteractionParams feedInteractionParams);

        /// <summary>
        /// Makes the flash tag interaction.
        /// </summary>
        /// <param name="flashTagInteractionParams">The flash tag interaction parameters.</param>
        /// <returns></returns>
        UserFlashtagFeedDataList MakeFlashTagInteraction(FlashTagInteractionParams flashTagInteractionParams);

        /// <summary>
        /// Deletes the comment for feed.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <returns></returns>
        UserFeedDataList DeleteCommentForFeed(int commentId, int userId, int feedId);

        /// <summary>
        /// Gets the comments for feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        CommentList GetCommentsForFeed(int feedId, int userId, bool isFlashTag, int pageIndex, int pageSize, string lastUpdatedTime);

        /// <summary>
        /// Gets the interacted users from feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="isLike">if set to <c>true</c> [is like].</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        InteractedUsers GetInteractedUsersFromFeed(int userId, int feedId, int imageId, bool isLike,
            bool isFlashTag, int pageIndex, int pageSize);

        /// <summary>
        /// Deletes the comment for flash tag.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="flashTagId">The flash tag identifier.</param>
        /// <returns></returns>
        UserFlashtagFeedDataList DeleteCommentForFlashTag(int commentId, int userId, int flashTagId);
    }
}