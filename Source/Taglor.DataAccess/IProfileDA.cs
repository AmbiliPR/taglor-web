﻿using System.Collections.Generic;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;

namespace Taglor.DataAccess
{
    public interface IProfileDA
    {
        LoginInfo SignUp(SignUp signUpData, string profilePicName, string bannerPicName);
        int IsUserExists(bool isFaceBookValidation, string emailAddress = "", string facebookTokenId = "");

        ResponseBase SaveFriendRequest(int userId, string receivedUserIds);
        ResponseBase AcceptOrRejectFriendRequest(int loggedInUserId, string requestedUserIds, bool status);
        FriendRequestsList GetFriendRequests(int userId, bool isSent);

        ProfileInfo GetUserProfile(int userId, int loggedInUserId);
        /// <summary>
        ///     Changes the password.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        ResponseBase ChangePassword(int userId, string oldPassword, string newPassword);

        /// <summary>
        ///     Resets the password.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="generatedPassword"></param>
        /// <returns></returns>
        ResponseBase ResetPassword(string emailAddress, string generatedPassword);

        /// <summary>
        /// Updates the user profile.
        /// </summary>
        /// <param name="userProfileParams">The user profile parameters.</param>
        /// <param name="profilePicPath">The profile pic path.</param>
        /// <param name="bannerPicPath">The banner pic path.</param>
        /// <returns></returns>
        LoginInfo UpdateUserProfile(UpdateUserProfileParams userProfileParams,string profilePicPath, string bannerPicPath);

        /// <summary>
        /// Updates the user profile.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        ResponseBase UpdateUserSettings(int userId, PrivacySettings setting);

        /// <summary>
        /// Gets the friends or followers list.
        /// </summary>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <param name="inUserId"></param>
        /// <param name="userRelation"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isFriendList">if set to <c>true</c> [is friend list].</param>
        /// <returns></returns>
        UserList GetFriendsOrFollowersList(int loggedInUserId, int userId, UserRelation userRelation, int pageIndex, int pageSize);

        /// <summary>
        /// Follow or Unfollow a user
        /// </summary>
        /// <param name="userId">LoggedIn userid</param>
        /// <param name="followingUserId">UserId of following profile</param>
        /// <param name="followStatus">Follow status - True -> Follow, False - Unfollow</param>
        /// <returns></returns>
        ResponseBase FollowUnFollowUser(int userId, int followerUserID, bool followStatus);

        ResponseBase DeleteFriend(int userId, int friendId);
        ResponseBase DeleteFriendRequest(int userId, int friendRequestId);
    }
}