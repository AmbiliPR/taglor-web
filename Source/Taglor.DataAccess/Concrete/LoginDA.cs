﻿using System;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;

namespace Taglor.DataAccess.Concrete
{
    public class LoginDA : ILoginDA
    {
        private readonly Database myDatabase = new DatabaseProviderFactory().CreateDefault();

        /// <see cref="" />
        public LoginInfo Login(string emailAddress, string password)
        {
            var loginInfo = new LoginInfo();
            try
            {
                var procName = "ValidateUser";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_EmailAddress", DbType.String, emailAddress);
                    myDatabase.AddInParameter(dbCommand, "@p_Password", DbType.String, password);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof (int));

                    var dataSet = myDatabase.ExecuteDataSet(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        loginInfo.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        loginInfo.ErrorDescription = Helper.GetErrorMessage(loginInfo.ErrorCode);
                        loginInfo.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        var row = dataSet.Tables[0].Rows[0];
                        loginInfo.UserId = Convert.ToInt32(row["Id"]);
                        loginInfo.FirstName = row["FirstName"].GetValueOrNull();
                        loginInfo.LastName = row["LastName"].GetValueOrNull();
                        loginInfo.UserName = row["UserName"].GetValueOrNull();
                        loginInfo.EmailAddress = row["EmailAddress"].GetValueOrNull();
                        loginInfo.ProfilePicUrl = row["ProfilePic"].GetValueOrNull();
                        loginInfo.BannerImageUrl = row["BannerImage"].GetValueOrNull();
                        loginInfo.Location = row["Location"].GetValueOrNull();
                        loginInfo.ErrorStatus = ResponseStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                loginInfo.ErrorStatus = ResponseStatus.ServerError;
                loginInfo.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return loginInfo;
        }

        /// <see cref="" />
        public FacebookLoginInfo LoginWithFacebook(string facebookUserId, string emailAddress, string firstName, string lastName,
            string profilePic, string userName)
        {
            var loginInfo = new FacebookLoginInfo();
            try
            {
                var procName = "ValidateFacebookUser";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FacebookUserId", DbType.String, facebookUserId);
                    myDatabase.AddInParameter(dbCommand, "@p_FirstName", DbType.String, firstName);
                    myDatabase.AddInParameter(dbCommand, "@p_EmailAddress", DbType.String, emailAddress);
                    myDatabase.AddInParameter(dbCommand, "@p_LastName", DbType.String, lastName);
                    myDatabase.AddInParameter(dbCommand, "@p_ProfilePicUrl", DbType.String, profilePic);
                    myDatabase.AddInParameter(dbCommand, "@p_UserName", DbType.String, userName);
                    myDatabase.AddInParameter(dbCommand, "@p_Location", DbType.String, string.Empty); // Will be updated later using update profile

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof (int));

                    var dataSet = myDatabase.ExecuteDataSet(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        loginInfo.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        loginInfo.ErrorDescription = Helper.GetErrorMessage(loginInfo.ErrorCode);
                        loginInfo.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        var row = dataSet.Tables[0].Rows[0];
                        loginInfo.UserId = Convert.ToInt32(row["Id"]);
                        loginInfo.FirstName = row["FirstName"].GetValueOrNull();
                        loginInfo.LastName = row["LastName"].GetValueOrNull();
                        loginInfo.UserName = row["UserName"].GetValueOrNull();
                        loginInfo.EmailAddress = row["EmailAddress"].GetValueOrNull();
                        loginInfo.ProfilePicUrl = row["ProfilePic"].GetValueOrNull();
                        loginInfo.BannerImageUrl = row["BannerImage"].GetValueOrNull();
                        loginInfo.Location = row["Location"].GetValueOrNull();
                        loginInfo.Password = Helper.Decrypt(row["Password"].GetValueOrNull());
                        loginInfo.ErrorStatus = ResponseStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                loginInfo.ErrorStatus = ResponseStatus.ServerError;
                loginInfo.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return loginInfo;
        }
    }
}