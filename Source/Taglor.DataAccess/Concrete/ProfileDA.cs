﻿using System;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;
using Taglor.Data;
using System.Collections.Generic;

namespace Taglor.DataAccess.Concrete
{
    public class ProfileDA : IProfileDA
    {
        private readonly Database myDatabase = new DatabaseProviderFactory().CreateDefault();

        public int IsUserExists(bool isFaceBookValidation, string emailAddress = "", string facebookTokenId = "")
        {
            var userId = 0;
            try
            {
                var procName = "CheckUser";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_EmailAddress", DbType.String, emailAddress);
                    myDatabase.AddInParameter(dbCommand, "@p_FaceBookTokenId", DbType.String, facebookTokenId);
                    myDatabase.AddInParameter(dbCommand, "@p_IsFaceBook", DbType.Boolean, isFaceBookValidation);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));
                    myDatabase.AddOutParameter(dbCommand, "@p_UserID", DbType.Int32, sizeof(int));


                    var dataSet = myDatabase.ExecuteDataSet(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_UserID")))
                    {
                        userId = Convert.ToInt32(myDatabase.GetParameterValue(dbCommand, "@p_UserID"));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            return userId;
        }

        public LoginInfo SignUp(SignUp signUpData, string profilePicName, string bannerPicName)
        {
            var loginInfo = new LoginInfo();
            try
            {
                var procName = "SignUpUser";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_EmailAddress", DbType.String, signUpData.EmailAddress);
                    myDatabase.AddInParameter(dbCommand, "@p_Password", DbType.String, signUpData.Password);
                    myDatabase.AddInParameter(dbCommand, "@p_FirstName", DbType.String, signUpData.FirstName);
                    myDatabase.AddInParameter(dbCommand, "@p_LastName", DbType.String, signUpData.LastName);
                    myDatabase.AddInParameter(dbCommand, "@p_UserName", DbType.String, signUpData.UserName);
                    myDatabase.AddInParameter(dbCommand, "@p_Location", DbType.String, signUpData.Location);
                    myDatabase.AddInParameter(dbCommand, "@p_ProfilePicUrl", DbType.String, profilePicName);
                    myDatabase.AddInParameter(dbCommand, "@p_BannerImageUrl", DbType.String, bannerPicName);


                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    var dataSet = myDatabase.ExecuteDataSet(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        loginInfo.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        loginInfo.ErrorDescription = Helper.GetErrorMessage(loginInfo.ErrorCode);
                        loginInfo.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        var row = dataSet.Tables[0].Rows[0];
                        loginInfo.UserId = Convert.ToInt32(row["Id"]);
                        loginInfo.FirstName = row["FirstName"].GetValueOrNull();
                        loginInfo.LastName = row["LastName"].GetValueOrNull();
                        loginInfo.UserName = row["UserName"].GetValueOrNull();
                        loginInfo.EmailAddress = row["EmailAddress"].GetValueOrNull();
                        loginInfo.ProfilePicUrl = row["ProfilePic"].GetValueOrNull();
                        loginInfo.BannerImageUrl = row["BannerImage"].GetValueOrNull();
                        loginInfo.BannerImageUrl = row["Location"].GetValueOrNull();
                        loginInfo.ErrorStatus = ResponseStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                loginInfo.ErrorStatus = ResponseStatus.ServerError;
                loginInfo.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return loginInfo;
        }

        public ResponseBase SaveFriendRequest(int userId, string receivedUserIds)
        {
            ResponseBase response = new ResponseBase();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("SaveFriendRequest"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_RequestedUserIds", DbType.String, receivedUserIds);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        response.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        response.ErrorDescription = Helper.GetErrorMessage(response.ErrorCode);
                        response.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorStatus = ResponseStatus.ServerError;
                response.ErrorDescription = ex.Message;
                Logger.Error("SaveFriendRequest: " + ex);
            }
            return response;
        }

        public ResponseBase AcceptOrRejectFriendRequest(int loggedInUserId, string requestedUserIds, bool status)
        {
            ResponseBase response = new ResponseBase();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("AcceptOrRejectFriendRequest"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, loggedInUserId);
                    myDatabase.AddInParameter(dbCommand, "@p_RequestedUserIds", DbType.String, requestedUserIds);
                    myDatabase.AddInParameter(dbCommand, "@p_Status", DbType.Boolean, status);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        response.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        response.ErrorDescription = Helper.GetErrorMessage(response.ErrorCode);
                        response.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorDescription = Helper.GetErrorMessage(response.ErrorCode);
                response.ErrorStatus = ResponseStatus.ServerError;
                Logger.Error("AcceptOrRejectFriendRequest: " + ex);
            }
            return response;
        }

        public FriendRequestsList GetFriendRequests(int userId, bool isSent)
        {
            FriendRequestsList requests = new FriendRequestsList();
            try
            {
                var dbCommand = myDatabase.GetStoredProcCommand("GetFriendRequests");

                myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                myDatabase.AddInParameter(dbCommand, "@p_IsSent", DbType.Boolean, isSent);
                myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                using (var reader = myDatabase.ExecuteReader(dbCommand))
                {
                    requests.FriendRequests = new List<FriendRequestInfo>();
                    while (reader.Read())
                    {
                        FriendRequestInfo friendRequest = new FriendRequestInfo();
                        friendRequest.UserId = Convert.ToInt32(reader["Id"]);
                        friendRequest.FirstName = reader["FirstName"].ToString();
                        friendRequest.LastName = reader["LastName"].ToString();
                        friendRequest.EmailAddress = reader["EmailAddress"].ToString();
                        friendRequest.ProfilePic = !string.IsNullOrEmpty(reader["ProfilePic"].ToString()) ? System.IO.Path.Combine(Constants.Server_ProfilePicPath, reader["ProfilePic"].ToString()) : string.Empty;
                        friendRequest.Location = reader["Location"].ToString();
                        requests.FriendRequests.Add(friendRequest);
                    }
                }
                if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                {
                    requests.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                    requests.ErrorDescription = Helper.GetErrorMessage(requests.ErrorCode);
                    requests.ErrorStatus = ResponseStatus.ServerError;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetFriendRequests: " + ex);
                requests.FriendRequests = null;
                requests.ErrorCode = Constants.UnExpectedErrorCode.ToString();
                requests.ErrorDescription = Helper.GetErrorMessage(Constants.UnExpectedErrorCode.ToString());
                requests.ErrorStatus = ResponseStatus.ServerError;
            }

            return requests;
        }


        /// <summary>
        /// Gets the user profile.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <returns></returns>
        public ProfileInfo GetUserProfile(int userId, int loggedInUserId)
        {
            var profileInfo = new ProfileInfo();
            try
            {
                var procName = "GetUserProfile";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_LoggedUserId", DbType.Int32, loggedInUserId);

                    myDatabase.AddOutParameter(dbCommand, "@p_IsFriend", DbType.Boolean, sizeof(bool));
                    myDatabase.AddOutParameter(dbCommand, "@p_IsFollowing", DbType.Boolean, sizeof(bool));
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    var dataSet = myDatabase.ExecuteDataSet(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        profileInfo.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        profileInfo.ErrorDescription = Helper.GetErrorMessage(profileInfo.ErrorCode);
                        profileInfo.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        var row = dataSet.Tables[0].Rows[0];
                        profileInfo.FriendCount = Convert.ToInt32(row["FRIENDCOUNT"]).GetValueOrNull() ?? 0;

                        row = dataSet.Tables[1].Rows[0];
                        profileInfo.FollowingCount = Convert.ToInt32(row["FOLLOWINGCOUNT"]).GetValueOrNull() ?? 0;

                        row = dataSet.Tables[2].Rows[0];
                        profileInfo.FollowerCount = Convert.ToInt32(row["FOLLOWERCOUNT"]).GetValueOrNull() ?? 0;

                        //if (userId != loggedInUserId)
                        {
                            row = dataSet.Tables[3].Rows[0];
                            profileInfo.UserInfo = new LoginInfo();
                            profileInfo.UserInfo.UserId = Convert.ToInt32(row["Id"]);
                            profileInfo.UserInfo.FirstName = row["FirstName"].GetValueOrNull();
                            profileInfo.UserInfo.LastName = row["LastName"].GetValueOrNull();
                            profileInfo.UserInfo.UserName = row["UserName"].GetValueOrNull();
                            profileInfo.UserInfo.EmailAddress = row["EmailAddress"].GetValueOrNull();
                            profileInfo.UserInfo.ProfilePicUrl = row["ProfilePic"].GetValueOrNull();
                            profileInfo.UserInfo.BannerImageUrl = row["BannerImage"].GetValueOrNull();
                            profileInfo.UserInfo.Location = row["Location"].GetValueOrNull();
                        }

                        profileInfo.IsFriend =
                            !Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_IsFriend")) && Convert.ToBoolean(myDatabase.GetParameterValue(dbCommand, "@p_IsFriend"));
                        profileInfo.IsFollowing = !Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_IsFollowing")) && Convert.ToBoolean(myDatabase.GetParameterValue(dbCommand, "@p_IsFollowing"));
                        profileInfo.ErrorStatus = ResponseStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                profileInfo.ErrorStatus = ResponseStatus.ServerError;
                profileInfo.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return profileInfo;
        }

        /// <summary>
        ///     Changes the password.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        public ResponseBase ChangePassword(int userId, string oldPassword, string newPassword)
        {
            var responseBase = new ResponseBase();
            try
            {
                var procName = "ChangePassword";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_ExistingPassword", DbType.String, oldPassword);
                    myDatabase.AddInParameter(dbCommand, "@p_NewPassword", DbType.String, newPassword);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteReader(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        responseBase.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        responseBase.ErrorDescription = Helper.GetErrorMessage(responseBase.ErrorCode);
                        responseBase.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        responseBase.ErrorStatus = ResponseStatus.Success;
                        responseBase.ErrorDescription = "Password changed successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                responseBase.ErrorStatus = ResponseStatus.ServerError;
                responseBase.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return responseBase;
        }

        /// <summary>
        ///     Resets the password.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="generatedPassword"></param>
        /// <returns></returns>
        public ResponseBase ResetPassword(string emailAddress, string generatedPassword)
        {
            var responseBase = new ResponseBase();
            try
            {
                var procName = "ResetPassword";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_EmailAddress", DbType.String, emailAddress);
                    myDatabase.AddInParameter(dbCommand, "@p_NewPassword", DbType.String, generatedPassword);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteReader(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        responseBase.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        responseBase.ErrorDescription = Helper.GetErrorMessage(responseBase.ErrorCode);
                        responseBase.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        responseBase.ErrorStatus = ResponseStatus.Success;
                        responseBase.ErrorDescription = "Reset password successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                responseBase.ErrorStatus = ResponseStatus.ServerError;
                responseBase.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return responseBase;
        }

        public LoginInfo UpdateUserProfile(UpdateUserProfileParams userProfileParams, string profilePicName, string bannerPicName)
        {
            var loginInfo = new LoginInfo();
            try
            {
                var procName = "UpdateUserProfile";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userProfileParams.UserId);
                    myDatabase.AddInParameter(dbCommand, "@p_EmailAddress", DbType.String, userProfileParams.UserProfile.EmailAddress);
                    myDatabase.AddInParameter(dbCommand, "@p_Password", DbType.String, userProfileParams.UserProfile.Password);
                    myDatabase.AddInParameter(dbCommand, "@p_FirstName", DbType.String, userProfileParams.UserProfile.FirstName);
                    myDatabase.AddInParameter(dbCommand, "@p_LastName", DbType.String, userProfileParams.UserProfile.LastName);
                    myDatabase.AddInParameter(dbCommand, "@p_UserName", DbType.String, userProfileParams.UserProfile.UserName);
                    myDatabase.AddInParameter(dbCommand, "@p_ProfilePicUrl", DbType.String, profilePicName);
                    myDatabase.AddInParameter(dbCommand, "@p_BannerImageUrl", DbType.String, bannerPicName);
                    myDatabase.AddInParameter(dbCommand, "@p_Location", DbType.String, userProfileParams.UserProfile.Location);
                    myDatabase.AddInParameter(dbCommand, "@p_IsRemovedProfilePic", DbType.Boolean, userProfileParams.IsProfileImageRemoved);
                    myDatabase.AddInParameter(dbCommand, "@p_IsRemovedBannerPic", DbType.Boolean, userProfileParams.IsBannerImageRemoved);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    var dataSet = myDatabase.ExecuteDataSet(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        loginInfo.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        loginInfo.ErrorDescription = Helper.GetErrorMessage(loginInfo.ErrorCode);
                        loginInfo.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        var row = dataSet.Tables[0].Rows[0];
                        loginInfo.UserId = Convert.ToInt32(row["Id"]);
                        loginInfo.FirstName = row["FirstName"].GetValueOrNull();
                        loginInfo.LastName = row["LastName"].GetValueOrNull();
                        loginInfo.UserName = row["UserName"].GetValueOrNull();
                        loginInfo.EmailAddress = row["EmailAddress"].GetValueOrNull();
                        loginInfo.ProfilePicUrl = row["ProfilePic"].GetValueOrNull();
                        loginInfo.BannerImageUrl = row["BannerImage"].GetValueOrNull();
                        loginInfo.Location = row["Location"].GetValueOrNull();
                        loginInfo.ErrorStatus = ResponseStatus.Success;
                        loginInfo.ErrorDescription = "Updated user profile.";
                    }
                }
            }
            catch (Exception ex)
            {
                loginInfo.ErrorStatus = ResponseStatus.ServerError;
                loginInfo.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return loginInfo;
        }

        /// <summary>
        /// Updates the user profile.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="setting">The setting.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public ResponseBase UpdateUserSettings(int userId, PrivacySettings setting)
        {
            var responseBase = new ResponseBase();
            try
            {
                var procName = "UpdateUserSettings";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_PrivacySettings", DbType.Int32, setting);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteReader(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        responseBase.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        responseBase.ErrorDescription = Helper.GetErrorMessage(responseBase.ErrorCode);
                        responseBase.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        responseBase.ErrorStatus = ResponseStatus.Success;
                        responseBase.ErrorDescription = "Privacy settings updated successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                responseBase.ErrorStatus = ResponseStatus.ServerError;
                responseBase.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return responseBase;
        }

        /// <summary>
        /// Gets the friends or followers list.
        /// </summary>
        /// <param name="loggedInUserId">The logged in user identifier.</param>
        /// <param name="inUserId"></param>
        /// <param name="userRelation"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="isFriendList">if set to <c>true</c> [is friend list].</param>
        /// <exception cref="ArgumentNullException"></exception>
        /// <returns></returns>
        public UserList GetFriendsOrFollowersList(int loggedInUserId, int userId, UserRelation userRelation, int pageIndex, int pageSize)
        {
            UserList userList = new UserList();
            try
            {
                var procName = "GetFriendsOrFollowersList";
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.String, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_UserRelation", DbType.Int32, userRelation);
                    myDatabase.AddInParameter(dbCommand, "@p_PageIndex", DbType.Int32, pageIndex);
                    myDatabase.AddInParameter(dbCommand, "@p_PageSize", DbType.Int32, pageSize);
                    myDatabase.AddInParameter(dbCommand, "@p_LoggedInUserId", DbType.Int32, loggedInUserId);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    var dataSet = myDatabase.ExecuteDataSet(dbCommand);


                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        userList.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        userList.ErrorDescription = Helper.GetErrorMessage(userList.ErrorCode);
                        userList.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        if (dataSet.Tables[0] != null && dataSet.Tables[0].Rows.Count > 0)
                        {
                            userList.UserInfos = Helper.GetUSers(dataSet.Tables[0]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                userList.ErrorStatus = ResponseStatus.ServerError;
                userList.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return userList;
        }

        public ResponseBase FollowUnFollowUser(int userId, int followerUserID, bool followStatus)
        {
            ResponseBase response = new ResponseBase();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("FollowUnFollowUser"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_FollowerUserID", DbType.Int32, followerUserID);
                    myDatabase.AddInParameter(dbCommand, "@p_FollowStatus", DbType.Boolean, followStatus);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        response.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        response.ErrorDescription = Helper.GetErrorMessage(response.ErrorCode);
                        response.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorStatus = ResponseStatus.ServerError;
                response.ErrorDescription = ex.Message;
                Logger.Error("FollowUnFollowUser: " + ex);
            }
            return response;
        }

        public ResponseBase DeleteFriend(int userId, int friendId)
        {
            ResponseBase response = new ResponseBase();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("DeleteFriend"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_FriendId", DbType.Int32, friendId);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        response.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        response.ErrorDescription = Helper.GetErrorMessage(response.ErrorCode);
                        response.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorStatus = ResponseStatus.ServerError;
                response.ErrorDescription = ex.Message;
                Logger.Error("DeleteFriend: " + ex);
            }
            return response;
        }

        /// <summary>
        /// Deletes the friend request.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="friendRequestId">The friend request identifier.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public ResponseBase DeleteFriendRequest(int userId, int friendRequestId)
        {
            ResponseBase response = new ResponseBase();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("DeleteFriendRequest"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_FriendRequestId", DbType.Int32, friendRequestId);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        response.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        response.ErrorDescription = Helper.GetErrorMessage(response.ErrorCode);
                        response.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorStatus = ResponseStatus.ServerError;
                response.ErrorDescription = ex.Message;
                Logger.Error("DeleteFriendRequest: " + ex);
            }
            return response;
            throw new NotImplementedException();
        }
    }
}