﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Taglor.Data;
using Taglor.Data.Model;
using Taglor.Data.Utils;

namespace Taglor.DataAccess.Concrete
{
    public class FeedDA : IFeedDA
    {
        private readonly Database myDatabase = new DatabaseProviderFactory().CreateDefault();

        /// <summary>
        ///     Gets the tags.
        /// </summary>
        /// <returns></returns>
        public TagList GetTags(int userId)
        {
            var tagList = new TagList();
            try
            {
                var procName = "GetTags";
                DataSet dataSet;
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, Convert.ToInt32(userId));
                    dataSet = myDatabase.ExecuteDataSet(dbCommand);
                }
                if (null != dataSet)
                {
                    tagList.Tags = new List<Tag>();
                    foreach (DataRow dataRow in dataSet.Tables[0].Rows)
                    {
                        if (!Helper.IsNullOrZero(dataRow["Id"]))
                        {
                            var tag = new Tag
                            {
                                Id = Convert.ToInt32(dataRow["Id"]),
                                Name = dataRow["Tags"].ToString()
                            };
                            tagList.Tags.Add(tag);
                        }
                    }


                    tagList.ErrorStatus = ResponseStatus.Success;
                    tagList.ErrorDescription = "";
                    tagList.ErrorCode = "10001";

                }
                else
                {
                    tagList.ErrorStatus = ResponseStatus.Success;
                    tagList.ErrorDescription = "Empty data.";
                }
            }
            catch (Exception ex)
            {
                tagList.ErrorStatus = ResponseStatus.ServerError;
                tagList.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }

            return tagList;
        }

        /// <summary>
        ///     Gets the privacy settings.
        /// </summary>
        /// <returns></returns>
        public PrivacySetting GetPrivacySettings(int userId)
        {
            try
            {
                var procName = "GetPrivacySettings";
                DataSet dataSet = null;
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, Convert.ToInt32(userId));
                    dataSet = myDatabase.ExecuteDataSet(dbCommand);
                }
                if (null != dataSet)
                {
                    var row = dataSet.Tables[0].Rows[0];
                    if (row == null) throw new ArgumentNullException("row");
                    return new PrivacySetting
                            {
                                Id = Convert.ToInt32(row["PrivacySettingsId"]),
                                Privacy =
                                    (PrivacySettings)
                                        Enum.Parse(typeof(PrivacySettings), row["PrivacySettings"].ToString())
                            };
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }

            return null;
        }

        /// <summary>
        ///     Creates the flash tags.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="question"></param>
        /// <param name="endTime"></param>
        /// <param name="sharedUserIds"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public FeedInfo CreateFlashTags(int userId, string question, DateTime endTime, string sharedUserIds, string location)
        {
            var info = new FeedInfo();
            try
            {
                var procName = "CreateFlashTags";

                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, Convert.ToInt32(userId));
                    myDatabase.AddInParameter(dbCommand, "@p_Question", DbType.String, question);
                    // Removed converting to UTC after discussion
                    myDatabase.AddInParameter(dbCommand, "@p_EndTime", DbType.DateTime, endTime);
                    myDatabase.AddInParameter(dbCommand, "@p_SharedUserIds", DbType.String, sharedUserIds);
                    myDatabase.AddInParameter(dbCommand, "@p_Location", DbType.String, location);


                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));
                    myDatabase.AddOutParameter(dbCommand, "@p_FeedId", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_FeedId")))
                        {
                            info.FeedId = Convert.ToInt32(myDatabase.GetParameterValue(dbCommand, "@p_FeedId"));
                            info.ErrorStatus = ResponseStatus.Success;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        /// <summary>
        ///     Creates the new feed.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="tagIds"></param>
        /// <param name="feedDetails"></param>
        /// <param name="settings"></param>
        /// <param name="addToWishlist"></param>
        /// <param name="postType"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public FeedInfo CreateNewFeed(int userId, string title, string description, string tagIds,
            FeedDetails feedDetails, PrivacySettings settings, bool addToWishlist, PostType postType, string location)
        {
            var info = new FeedInfo();
            try
            {
                var procName = "CreateFeed";
                ;

                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_Title", DbType.String, title);
                    myDatabase.AddInParameter(dbCommand, "@p_Description", DbType.String, description);
                    myDatabase.AddInParameter(dbCommand, "@p_Tags", DbType.String, tagIds);

                    // Feed details
                    myDatabase.AddInParameter(dbCommand, "@p_Brand", DbType.String, feedDetails.Brand);
                    myDatabase.AddInParameter(dbCommand, "@p_Model", DbType.String, feedDetails.Model);
                    myDatabase.AddInParameter(dbCommand, "@p_Serial", DbType.String, feedDetails.Serial);
                    myDatabase.AddInParameter(dbCommand, "@p_PurchasedFrom", DbType.String, feedDetails.PurchasedFrom);
                    myDatabase.AddInParameter(dbCommand, "@p_PurchasePrice", DbType.String, feedDetails.PurchasePrice);
                    myDatabase.AddInParameter(dbCommand, "@p_Warranty", DbType.String, feedDetails.Warranty);
                    myDatabase.AddInParameter(dbCommand, "@p_ExpirationDate", DbType.DateTime,
                        feedDetails.ExpirationDate.GetValueOrNull());
                    myDatabase.AddInParameter(dbCommand, "@p_Location", DbType.String, location);

                    myDatabase.AddInParameter(dbCommand, "@p_Settings", DbType.Int32, Convert.ToInt32(settings));
                    myDatabase.AddInParameter(dbCommand, "@p_AddToWishlist", DbType.Boolean, addToWishlist);
                    myDatabase.AddInParameter(dbCommand, "@p_PostType", DbType.Int32, Convert.ToInt32(postType));


                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));
                    myDatabase.AddOutParameter(dbCommand, "@p_FeedId", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_FeedId")))
                        {
                            info.FeedId = Convert.ToInt32(myDatabase.GetParameterValue(dbCommand, "@p_FeedId"));
                            info.ErrorStatus = ResponseStatus.Success;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        /// <summary>
        ///     Uploads the image.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public ImageInfo UploadImage(int feedId, string fileName, bool isFlashTag, bool isReceipt)
        {
            var info = new ImageInfo();
            try
            {
                var procName = isFlashTag ? "UploadFlashTagImage" : "UploadImage";

                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedId);
                    myDatabase.AddInParameter(dbCommand, "@p_FileName", DbType.String, fileName);
                    if (!isFlashTag)
                        myDatabase.AddInParameter(dbCommand, "@p_IsReceipt", DbType.Boolean, isReceipt);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));
                    myDatabase.AddOutParameter(dbCommand, "@p_ImageId", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_ImageId")))
                        {
                            info.ImageId = Convert.ToInt32(myDatabase.GetParameterValue(dbCommand, "@p_ImageId"));
                            info.ErrorStatus = ResponseStatus.Success;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        /// <summary>
        ///     Updates the feed details.
        /// </summary>
        public ResponseBase UpdateFeed(int feedId, string title, string description, string tagIds, FeedDetails feedDetails, PrivacySettings feedSettings, bool addToWishList, PostType postType, string location)
        {
            var info = new ResponseBase();
            try
            {
                var procName = "UpdateFeed";
                ;

                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.String, feedId);
                    myDatabase.AddInParameter(dbCommand, "@p_Title", DbType.String, title);
                    myDatabase.AddInParameter(dbCommand, "@p_Description", DbType.String, description);
                    myDatabase.AddInParameter(dbCommand, "@p_Tags", DbType.String, tagIds);

                    // Feed details
                    myDatabase.AddInParameter(dbCommand, "@p_Brand", DbType.String, feedDetails.Brand);
                    myDatabase.AddInParameter(dbCommand, "@p_Model", DbType.String, feedDetails.Model);
                    myDatabase.AddInParameter(dbCommand, "@p_Serial", DbType.String, feedDetails.Serial);
                    myDatabase.AddInParameter(dbCommand, "@p_PurchasedFrom", DbType.String, feedDetails.PurchasedFrom);
                    myDatabase.AddInParameter(dbCommand, "@p_PurchasePrice", DbType.String, feedDetails.PurchasePrice);
                    myDatabase.AddInParameter(dbCommand, "@p_Warranty", DbType.String, feedDetails.Warranty);
                    myDatabase.AddInParameter(dbCommand, "@p_ExpirationDate", DbType.DateTime,
                        feedDetails.ExpirationDate.GetValueOrNull());

                    myDatabase.AddInParameter(dbCommand, "@p_Settings", DbType.Int32, Convert.ToInt32(feedSettings));
                    myDatabase.AddInParameter(dbCommand, "@p_AddToWishlist", DbType.Boolean, addToWishList);
                    myDatabase.AddInParameter(dbCommand, "@p_Location", DbType.String, location);


                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        public ResponseBase UpdateFlashTags(int feedId, string question, DateTime endTime, string sharedUserIds, string location)
        {
            var info = new ResponseBase();
            try
            {
                var procName = "UpdateFlashTags";

                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedId);
                    myDatabase.AddInParameter(dbCommand, "@p_Question", DbType.String, question);
                    myDatabase.AddInParameter(dbCommand, "@p_EndTime", DbType.DateTime, endTime);
                    myDatabase.AddInParameter(dbCommand, "@p_Location", DbType.String, location);
                    myDatabase.AddInParameter(dbCommand, "@p_SharedUserIds", DbType.String, sharedUserIds);

                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        /// <summary>
        ///     Deletes the feed.
        /// </summary>
        public ResponseBase DeleteFeed(int feedId, int userId, bool isFlashTag)
        {
            var procName = "DeleteFeed";
            var info = new ResponseBase();
            if (isFlashTag)
            {
                procName = "DeleteFlashTags";
            }
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedId);
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        /// <summary>
        ///     Deletes the image from feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public ResponseBase DeleteImageFromFeed(int userId, int feedId, int imageId, bool isFlashTag)
        {
            var procName = "DeleteFeedImage";
            var info = new ResponseBase();
            if (isFlashTag)
            {
                procName = "DeleteFlashTagImage";
            }
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedId);
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_ImageIds", DbType.Int32, imageId);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        public UserFeedDataList GetUserFeeds(int userId, string lastUpdateTime, int pageIndex, short pageSize,
            short feedType, int loggedInUser, int commentCount)
        {
            var userFeedData = new UserFeedDataList();
            try
            {
                var procName = string.Empty;
                switch (feedType)
                {
                    case (short)FeedType.Activity:
                        procName = "GetUserActivityFeeds";
                        break;
                    case (short)FeedType.Wishlist:
                        procName = "GetUserWishlist";
                        break;
                    case (short)FeedType.Untagged:
                        procName = "GetUserUntaggedFeeds";
                        break;
                    case (short)FeedType.Profile:
                        procName = "GetProfileFeeds";
                        break;
                    default:
                    {
                        userFeedData.ErrorCode = "100001";
                        userFeedData.ErrorStatus = ResponseStatus.BadRequest;
                        userFeedData.ErrorDescription = "Feed type not supported";
                        break;
                    }
                }
                if (string.IsNullOrEmpty(procName)) return userFeedData;
                var dbCommand = myDatabase.GetStoredProcCommand(procName);

                myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                myDatabase.AddInParameter(dbCommand, "@p_PageIndex", DbType.Int32, pageIndex);
                myDatabase.AddInParameter(dbCommand, "@p_PageSize", DbType.Int16, pageSize);
                if (feedType == (short)FeedType.Profile)
                    myDatabase.AddInParameter(dbCommand, "@p_LoggedInUserId", DbType.Int32, loggedInUser);

                if(feedType == (short)FeedType.Activity)
                    myDatabase.AddInParameter(dbCommand, "@p_CommentCount", DbType.Int32, commentCount);

                myDatabase.AddParameter(dbCommand, "@p_LastUpdateTime", DbType.DateTime, ParameterDirection.InputOutput,
                    null, DataRowVersion.Default, lastUpdateTime);

                myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));


                using (var reader = myDatabase.ExecuteReader(dbCommand))
                {
                    userFeedData.UserFeedsList = new List<UserFeedsData>();
                    while (reader.Read())
                    {
                        GetFeedData(loggedInUser, reader, userFeedData, (FeedType)feedType);
                    }
                }

                if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                {
                    userFeedData.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                    userFeedData.ErrorDescription = Helper.GetErrorMessage(userFeedData.ErrorCode);
                    userFeedData.ErrorStatus = ResponseStatus.ServerError;
                }
                else
                {
                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_LastUpdateTime")))
                    {
                        userFeedData.LastUpdateDate =
                            Convert.ToDateTime(myDatabase.GetParameterValue(dbCommand, "@p_LastUpdateTime"))
                                .ToString("MM/dd/yyyy hh:mm:ss:fff");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetUserFeeds: " + ex);
                userFeedData.UserFeedsList = null;
                userFeedData.ErrorCode = Constants.UnExpectedErrorCode.ToString();
                userFeedData.ErrorDescription = Helper.GetErrorMessage(Constants.UnExpectedErrorCode.ToString());
                userFeedData.ErrorStatus = ResponseStatus.ServerError;
            }

            return userFeedData;
        }

        private void GetFeedData(int loggedInUser, IDataReader reader, UserFeedDataList userFeedData, FeedType feedType)
        {
            var userFeeds = new UserFeedsData();
            userFeeds.FeedId = Convert.ToInt32(reader["FeedId"]);
            userFeeds.Title = reader["Title"].ToString();
            userFeeds.Description = reader["Description"].ToString();
            userFeeds.Location = reader["Location"].ToString();
                //reader["UserLocation"].ToString(); //reader["Location"].ToString();
            userFeeds.Brand = reader["Brand"].ToString();
            userFeeds.Model = reader["Model"].ToString();
            userFeeds.PurchasedFrom = reader["PurchasedFrom"].ToString();
            userFeeds.UserId = Convert.ToInt32(reader["Id"]);
            userFeeds.Serial = userFeeds.UserId == loggedInUser ? reader["SerialNumber"].ToString() : string.Empty;
            userFeeds.PurchasePrice = userFeeds.UserId == loggedInUser ? reader["Price"].ToString() : string.Empty;
            userFeeds.Warranty = userFeeds.UserId == loggedInUser ? reader["Warranty"].ToString() : string.Empty;
            ;
            userFeeds.ExpirationDate = userFeeds.UserId == loggedInUser && !Helper.IsNullOrZero(reader["ExpiryDate"])
                ? Convert.ToDateTime(reader["ExpiryDate"])
                : DateTime.MinValue;
            userFeeds.CreatedTime = Convert.ToDateTime(reader["CreatedTime"]);
            userFeeds.UserId = Convert.ToInt32(reader["Id"]);
            userFeeds.FirstName = reader["FirstName"].ToString();
            userFeeds.LastName = reader["LastName"].ToString();
            userFeeds.ProfilePic = !string.IsNullOrEmpty(reader["ProfilePic"].ToString())
                ? Path.Combine(Constants.Server_ProfilePicPath, reader["ProfilePic"].ToString())
                : string.Empty;
            userFeeds.Post = (PostType) Convert.ToInt32(reader["PostTypeId"]);
            userFeeds.PrivacySetting = (PrivacySettings) Convert.ToInt32(reader["PrivacySettingsId"]);

            if (reader["FeedImages"] != null && reader["FeedImages"].ToString().Length > 0)
            {
                var doc = XDocument.Parse(reader["FeedImages"].ToString());
                var xElement = doc.Element("FeedImages");
                if (xElement != null)
                    userFeeds.FeedImages = (from images in xElement.Elements("Images")
                        let receipt = images.Element("IsReceipt")
                        let url = images.Element("ImageUrl")
                        where url != null
                        select new FeedImage
                        {
                            ImageId = Convert.ToInt32(images.Element("Id").Value),
                            ImageUrl = Path.Combine(Constants.Server_FeedPicPath, url.Value),
                            IsReceipt = receipt != null && (receipt.Value == "True")
                        }).ToList();
            }
            else
                userFeeds.FeedImages = null;

            if (reader["FeedTags"] != null && reader["FeedTags"].ToString().Length > 0)
            {
                var doc = XDocument.Parse(reader["FeedTags"].ToString());
                var feedTagEle = doc.Element("FeedTags");
                if (feedTagEle != null)
                    userFeeds.FeedTags = (from tags in feedTagEle.Elements("Tag")
                        let tagElement = tags.Element("Tags")
                        where tagElement != null
                        let idElement = tags.Element("Id")
                        where idElement != null
                        select new Tag
                        {
                            Id = Convert.ToInt32(idElement.Value),
                            Name = tagElement.Value
                        }).ToList();
            }
            else
                userFeeds.FeedTags = null;
            
            userFeeds.Comments = GetComments(reader, feedType );

            userFeeds.LikedCount = Convert.ToInt32(reader["LikedCount"]);
            userFeeds.CommentCount = Convert.ToInt32(reader["CommentCount"]);
            userFeeds.Rating = Convert.ToInt32(reader["Rating"]);
            userFeeds.CommentCountByLoggedUser = Convert.ToInt32(reader["CommentCountByUser"]);
            userFeeds.IsLikedByLoggedUser = Convert.ToBoolean(reader["IsLikedByUser"]);
            userFeeds.RatingByLoggedUser = Convert.ToInt32(reader["RatingByUser"]);
            userFeedData.UserFeedsList.Add(userFeeds);
        }

        public UserFlashtagFeedDataList GetUserFlashtagFeeds(int userId, string lastUpdateTime, int pageIndex,
            short pageSize, int commentCount)
        {
            var userFeedData = new UserFlashtagFeedDataList();
            try
            {
                var dbCommand = myDatabase.GetStoredProcCommand("GetUserFlashtagFeeds");

                myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                myDatabase.AddInParameter(dbCommand, "@p_PageIndex", DbType.Int32, pageIndex);
                myDatabase.AddInParameter(dbCommand, "@p_PageSize", DbType.Int16, pageSize);

                myDatabase.AddParameter(dbCommand, "@p_LastUpdateTime", DbType.DateTime, ParameterDirection.InputOutput,
                    null, DataRowVersion.Default, lastUpdateTime);
                myDatabase.AddInParameter(dbCommand, "@p_CommentCount", DbType.Int32, commentCount);
                myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));


                using (var reader = myDatabase.ExecuteReader(dbCommand))
                {
                    userFeedData.UserFlashtagFeedList = new List<UserFlashtagFeedData>();
                    while (reader.Read())
                    {
                        GetFlashTagFeedData(reader, userFeedData,FeedType.Activity);
                    }
                }
                if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                {
                    userFeedData.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                    userFeedData.ErrorDescription = Helper.GetErrorMessage(userFeedData.ErrorCode);
                    userFeedData.ErrorStatus = ResponseStatus.ServerError;
                }
                else
                {
                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_LastUpdateTime")))
                    {
                        userFeedData.LastUpdateDate =
                            Convert.ToDateTime(myDatabase.GetParameterValue(dbCommand, "@p_FeedId"))
                                .ToString("MM/dd/yyyy hh:mm:ss:fff");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetUserFlashtagFeeds: " + ex);
                userFeedData.UserFlashtagFeedList = null;
                userFeedData.ErrorCode = Constants.UnExpectedErrorCode.ToString();
                userFeedData.ErrorDescription = Helper.GetErrorMessage(Constants.UnExpectedErrorCode.ToString());
                userFeedData.ErrorStatus = ResponseStatus.ServerError;
            }

            return userFeedData;
        }

        private void GetFlashTagFeedData(IDataReader reader, UserFlashtagFeedDataList userFeedData,FeedType feedType)
        {
            var userFeeds = new UserFlashtagFeedData();
            userFeeds.FlashtagdId = Convert.ToInt32(reader["FlashtagId"]);
            userFeeds.Question = reader["Question"].ToString();
            userFeeds.EndTime = Convert.ToDateTime(reader["EndTime"]);
            userFeeds.CreatedTime = Convert.ToDateTime(reader["CreatedTime"]);
            userFeeds.UserId = Convert.ToInt32(reader["UserId"]);
            userFeeds.FirstName = reader["FirstName"].ToString();
            userFeeds.LastName = reader["LastName"].ToString();
            userFeeds.Location = reader["Location"].ToString(); //reader["UserLocation"].ToString();
            userFeeds.ProfilePic = !string.IsNullOrEmpty(reader["ProfilePic"].ToString())
                ? Path.Combine(Constants.Server_ProfilePicPath, reader["ProfilePic"].ToString())
                : string.Empty;

            if (reader["FlashTagImages"] != null && reader["FlashTagImages"].ToString().Length > 0)
            {
                var doc = XDocument.Parse(reader["FlashTagImages"].ToString());
                var flashTagImages = doc.Element("FlashTagImages");
                if (flashTagImages != null)
                    userFeeds.FeedImages = (from images in flashTagImages.Elements("Images")
                        select new FlashTagImage
                        {
                            ImageId = Convert.ToInt32(images.Element("Id").Value),
                            ImageUrl = Path.Combine(Constants.Server_FlashTagPicPath, images.Element("FlashtagImageUrl").Value),
                            LikedCount = Convert.ToInt32(images.Element("LikedCount").Value),
                            DislikedCount = Convert.ToInt32(images.Element("DisLikedCount").Value),
                            Rating = Convert.ToInt32(images.Element("Rating").Value),
                            IsLikedByLoggedUser = images.Element("IsLikedByUser").Value == "1",
                            IsDislikedByLoggedUser = images.Element("IsDisLikedByUser").Value == "1",
                            RatingByLoggedUser = Convert.ToInt32(images.Element("RatingByUser").Value)
                        }).ToList();
            }
            else
                userFeeds.FeedImages = null;

           
            userFeeds.Comments =  GetComments(reader,feedType);

            userFeeds.CommentCount = Convert.ToInt32(reader["CommentCount"]);
            userFeeds.CommentCountByLoggedUser = Convert.ToInt32(reader["CommentCountByUser"]);
            userFeeds.SharedUserIds = reader["SharedUserIds"].GetValueOrNull();
            userFeedData.UserFlashtagFeedList.Add(userFeeds);
        }

        private IList<FeedComment> GetComments(IDataReader reader, FeedType feedType)
        {
            if (feedType != FeedType.Activity) return null;
            IList<FeedComment> comments = null;
            if (reader["FeedComments"] != null &&
                 reader["FeedComments"].ToString().Length > 0)
            {
                comments = new List<FeedComment>();
                var doc = XDocument.Parse(reader["FeedComments"].ToString());
                var feedCommentEle = doc.Element("FeedComments");
                foreach (var feedComment in feedCommentEle.Elements("Comments"))
                {
                    var comment = new FeedComment();
                    comment.Comment = feedComment.Element("Comment").Value;
                    comment.CommentedDate = Convert.ToDateTime(feedComment.Element("CommentDate").Value);
                    comment.Id = Convert.ToInt32(feedComment.Element("Id").Value);
                    var profilePic = feedComment.Element("ProfilePic");
                    var bannerPic = feedComment.Element("BannerImage");

                    comment.CommentedUser = new LoginInfo()
                    {
                        UserId = Convert.ToInt32(feedComment.Element("UserId").Value),
                        UserName = feedComment.Element("UserName").Value,
                        FirstName = feedComment.Element("FirstName").Value,
                        LastName = feedComment.Element("LastName").Value,
                        Location = feedComment.Element("Location").Value,
                        ProfilePicUrl =
                            profilePic != null ? Path.Combine(Constants.Server_ProfilePicPath, profilePic.Value) : string.Empty,
                        BannerImageUrl =
                            bannerPic != null ? Path.Combine(Constants.Server_BannerPicPath, bannerPic.Value) : string.Empty,
                        EmailAddress = feedComment.Element("EmailAddress").Value
                    };
                    comments.Add(comment);
                }
            }
            return comments;
        }

        public UserInventoryList GetUserInventoryFeeds(int userId)
        {
            var userFeedData = new UserInventoryList();
            try
            {
                DataSet dataSet = null;
                using (var dbCommand = myDatabase.GetStoredProcCommand("GetUserInventory"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));
                    dataSet = myDatabase.ExecuteDataSet(dbCommand);
                }
                if (null != dataSet && dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        userFeedData.UserInventoryFeedList = new List<UserInventoryData>();
                        foreach (DataRow tagRow in dataSet.Tables[0].Rows)
                        {
                            UserInventoryData inventory = new UserInventoryData();
                            inventory.TagData = new Tag();
                            inventory.TagData.Id = Convert.ToInt32(tagRow["TagId"]);
                            inventory.TagData.Name = tagRow["Tags"].ToString();
                            inventory.TotalFeedsCount = Convert.ToInt32(tagRow["FeedsCount"]);
                            DataRow[] feedRows = dataSet.Tables[1].Select("TagId = " + inventory.TagData.Id);
                            if (feedRows.Any())
                            {
                                inventory.UserFeedsList = new List<UserFeedsData>();
                                foreach (DataRow feedRow in feedRows)
                                {
                                    var userFeeds = ParseUserFeeds(feedRow);
                                    inventory.UserFeedsList.Add(userFeeds);
                                }
                                userFeedData.UserInventoryFeedList.Add(inventory);
                            }
                            else
                                inventory.UserFeedsList = null;
                        }
                    }
                    else
                        userFeedData.UserInventoryFeedList = null;
                }
                else
                    userFeedData.UserInventoryFeedList = null;
            }
            catch (Exception ex)
            {
                Logger.Error("GetUserInventoryFeeds: " + ex);
                userFeedData.UserInventoryFeedList = null;
                userFeedData.ErrorCode = Constants.UnExpectedErrorCode.ToString();
                userFeedData.ErrorDescription = Helper.GetErrorMessage(Constants.UnExpectedErrorCode.ToString());
                userFeedData.ErrorStatus = ResponseStatus.ServerError;
            }

            return userFeedData;
        }

        /// <summary>
        /// Searches the specified search text.
        /// </summary>
        /// <param name="searchText">The search text.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="searchType">Type of the search.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException">searchType;null</exception>
        public SearchResult Search(string searchText, int userId, SearchType searchType, int pageIndex, int pageSize)
        {
            SearchResult searchResult = null;
            string procName = "Search";
            try
            {
                DataSet dataSet = null;
                DbCommand dbCommand;
                using (dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_PageIndex", DbType.Int32, pageIndex);
                    myDatabase.AddInParameter(dbCommand, "@p_PageSize", DbType.Int32, pageSize);
                    myDatabase.AddInParameter(dbCommand, "@pSearchParams", DbType.Int32, searchType);
                    myDatabase.AddInParameter(dbCommand, "@p_SearchText", DbType.String, searchText);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));
                    dataSet = myDatabase.ExecuteDataSet(dbCommand);
                }
                if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                {
                    searchResult = new SearchResult();
                    searchResult.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                    searchResult.ErrorDescription = Helper.GetErrorMessage(searchResult.ErrorCode);
                    searchResult.ErrorStatus = ResponseStatus.ServerError;
                }
                else if (null != dataSet && dataSet.Tables.Count > 0)
                {
                    switch (searchType)
                    {
                        case SearchType.All:
                            searchResult = FillAllSearchResults(dataSet);
                            break;
                        case SearchType.Users:
                            searchResult = new SearchResult();
                            searchResult.Users = Helper.GetUSers(dataSet.Tables[0]);
                            break;
                        case SearchType.Inventory:
                            searchResult = new SearchResult();
                            searchResult.Inventory = GetFeedData(dataSet.Tables[1]);
                            break;
                        case SearchType.Wishlist:
                            searchResult = new SearchResult();
                            searchResult.WishList = GetFeedData(dataSet.Tables[2]);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException("searchType", searchType, null);
                    }
                }
            }
            catch (Exception ex)
            {
                searchResult = new SearchResult();
                Logger.Error("Search: " + ex);
                searchResult.ErrorCode = Constants.UnExpectedErrorCode.ToString();
                searchResult.ErrorDescription = Helper.GetErrorMessage(Constants.UnExpectedErrorCode.ToString());
                searchResult.ErrorStatus = ResponseStatus.ServerError;
            }
            return searchResult;
        }

        /// <summary>
        /// Gets the update count.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="lastUpdateDateTime">The last update date time.</param>
        /// <returns></returns>
        public FeedUpdateCount GetUpdateCount(int userId, string lastUpdateDateTime)
        {
            var info = new FeedUpdateCount();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("GetUpdateCount"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_LastUpdatedTime", DbType.DateTime, lastUpdateDateTime);
                    myDatabase.AddOutParameter(dbCommand, "@p_UpdateCount", DbType.Int32, sizeof(int));
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    myDatabase.ExecuteNonQuery(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        info.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        info.ErrorDescription = Helper.GetErrorMessage(info.ErrorCode);
                        info.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_UpdateCount")))
                    {
                        info.Count = Convert.ToInt32(myDatabase.GetParameterValue(dbCommand, "@p_UpdateCount"));
                    }
                }
            }
            catch (Exception ex)
            {
                info.ErrorStatus = ResponseStatus.ServerError;
                info.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return info;
        }

        /// <summary>
        /// Makes the flash tag interaction.
        /// </summary>
        /// <param name="flashTagInteractionParams">The flash tag interaction parameters.</param>
        /// <returns></returns>
        public UserFlashtagFeedDataList MakeFlashTagInteraction(FlashTagInteractionParams flashTagInteractionParams)
        {
            UserFlashtagFeedDataList userFlashtagFeedDataList = new UserFlashtagFeedDataList();

            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("FlashTagUserInteraction"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, flashTagInteractionParams.UserId);
                    myDatabase.AddInParameter(dbCommand, "@p_FlashTagImageId", DbType.Int32, flashTagInteractionParams.ImageId);
                    myDatabase.AddInParameter(dbCommand, "@p_FlashTagId", DbType.Int32, flashTagInteractionParams.FeedId);
                    myDatabase.AddInParameter(dbCommand, "@p_Like", DbType.Boolean, flashTagInteractionParams.IsLike);
                    myDatabase.AddInParameter(dbCommand, "@p_DisLike", DbType.Boolean, flashTagInteractionParams.Dislike);
                    myDatabase.AddInParameter(dbCommand, "@p_Rating", DbType.Int32, flashTagInteractionParams.Rating);
                    myDatabase.AddInParameter(dbCommand, "@p_Comment", DbType.String, flashTagInteractionParams.Comment);
                    myDatabase.AddInParameter(dbCommand, "@p_FlashInteractionParam", DbType.Int32, flashTagInteractionParams.UserInteraction);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));


                    using (var reader = myDatabase.ExecuteReader(dbCommand))
                    {
                        userFlashtagFeedDataList.UserFlashtagFeedList = new List<UserFlashtagFeedData>();
                        while (reader.Read())
                        {
                            GetFlashTagFeedData(reader, userFlashtagFeedDataList,FeedType.Activity);
                        }
                    }

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        userFlashtagFeedDataList.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        userFlashtagFeedDataList.ErrorDescription = Helper.GetErrorMessage(userFlashtagFeedDataList.ErrorCode);
                        userFlashtagFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                userFlashtagFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                userFlashtagFeedDataList.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return userFlashtagFeedDataList;
        }

        /// <summary>
        /// Makes the user feed interaction.
        /// </summary>
        /// <param name="feedInteractionParams">The feed interaction parameters.</param>
        /// <returns></returns>
        public UserFeedDataList MakeUserFeedInteraction(FeedInteractionParams feedInteractionParams)
        {
            UserFeedDataList userFeedDataList = new UserFeedDataList();
            
            try
            {
                 using (var dbCommand = myDatabase.GetStoredProcCommand("FeedUserInteraction"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, feedInteractionParams.UserId);
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedInteractionParams.FeedId);
                    myDatabase.AddInParameter(dbCommand, "@p_Like", DbType.Boolean, feedInteractionParams.IsLike);
                    myDatabase.AddInParameter(dbCommand, "@p_Rating", DbType.Int32, feedInteractionParams.Rating);
                    myDatabase.AddInParameter(dbCommand, "@p_Comment", DbType.String, feedInteractionParams.Comment);
                    myDatabase.AddInParameter(dbCommand, "@p_FeedInteractionParam", DbType.Int32, feedInteractionParams.UserInteraction);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    using (var reader = myDatabase.ExecuteReader(dbCommand))
                    {
                        userFeedDataList.UserFeedsList = new List<UserFeedsData>();
                        while (reader.Read())
                        {
                            GetFeedData(feedInteractionParams.UserId, reader, userFeedDataList, FeedType.Activity);
                        }
                    }

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        userFeedDataList.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        userFeedDataList.ErrorDescription = Helper.GetErrorMessage(userFeedDataList.ErrorCode);
                        userFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                    }
                   
                }
            }
            catch (Exception ex)
            {
                  userFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                userFeedDataList.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return userFeedDataList;
        }

        public UserFlashtagFeedDataList DeleteCommentForFlashTag(int commentId, int userId, int flashTagId)
        {
            UserFlashtagFeedDataList userFeedDataList = new UserFlashtagFeedDataList();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("DeleteCommentForFeed"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, flashTagId);
                    myDatabase.AddInParameter(dbCommand, "@p_CommentId", DbType.Int32, commentId);
                    myDatabase.AddInParameter(dbCommand, "@p_UserID", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_IsFlashTag", DbType.Boolean, true);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    using (var reader = myDatabase.ExecuteReader(dbCommand))
                    {
                        userFeedDataList.UserFlashtagFeedList = new List<UserFlashtagFeedData>();
                        while (reader.Read())
                        {
                            GetFlashTagFeedData(reader, userFeedDataList,FeedType.Activity);
                        }
                    }

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        userFeedDataList.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        userFeedDataList.ErrorDescription = Helper.GetErrorMessage(userFeedDataList.ErrorCode);
                        userFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        userFeedDataList.ErrorDescription = "Deleted comment.";
                        userFeedDataList.ErrorStatus = ResponseStatus.Success;
                    }

                }
            }
            catch (Exception ex)
            {
                userFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                userFeedDataList.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return userFeedDataList;
        }

        /// <summary>
        /// Deletes the comment for feed.
        /// </summary>
        /// <param name="commentId">The comment identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public UserFeedDataList DeleteCommentForFeed(int commentId, int userId, int feedId)
        {
            UserFeedDataList userFeedDataList = new UserFeedDataList();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("DeleteCommentForFeed"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedId);
                    myDatabase.AddInParameter(dbCommand, "@p_CommentId", DbType.Int32, commentId);
                    myDatabase.AddInParameter(dbCommand, "@p_UserID", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_IsFlashTag", DbType.Boolean, false);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    using (var reader = myDatabase.ExecuteReader(dbCommand))
                    {
                        userFeedDataList.UserFeedsList = new List<UserFeedsData>();
                        while (reader.Read())
                        {
                            GetFeedData(userId, reader, userFeedDataList,FeedType.Activity);
                        }
                    }

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        userFeedDataList.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        userFeedDataList.ErrorDescription = Helper.GetErrorMessage(userFeedDataList.ErrorCode);
                        userFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else
                    {
                        userFeedDataList.ErrorDescription = "Deleted comment.";
                        userFeedDataList.ErrorStatus = ResponseStatus.Success;
                    }

                }
            }
            catch (Exception ex)
            {
                userFeedDataList.ErrorStatus = ResponseStatus.ServerError;
                userFeedDataList.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return userFeedDataList;
        }

        /// <summary>
        /// Gets the comments for feed.
        /// </summary>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public CommentList GetCommentsForFeed(int feedId, int userId, bool isFlashTag, int pageIndex, int pageSize, string lastUpdatedTime)
        {
            CommentList commentList = new CommentList();

            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand("GetCommentsForFeed"))
                {
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedId);
                    myDatabase.AddInParameter(dbCommand, "@p_IsFlashTag", DbType.Boolean, isFlashTag);
                    myDatabase.AddInParameter(dbCommand, "@p_PageIndex", DbType.Int32, pageIndex);
                    myDatabase.AddInParameter(dbCommand, "@p_PageSize", DbType.Int32, pageSize);
                    myDatabase.AddInParameter(dbCommand, "@p_LastUpdatedTime", DbType.DateTime,lastUpdatedTime);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    var datSet = myDatabase.ExecuteDataSet(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        commentList.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        commentList.ErrorDescription = Helper.GetErrorMessage(commentList.ErrorCode);
                        commentList.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else if (datSet != null)
                    {
                        commentList.Comments = new List<FeedComment>();
                        GetComments(datSet, commentList.Comments);

                    }
                }
            }
            catch (Exception ex)
            {
                commentList.ErrorStatus = ResponseStatus.ServerError;
                commentList.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return commentList;
        }

        /// <summary>
        /// Gets the interacted users from feed.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="feedId">The feed identifier.</param>
        /// <param name="imageId">The image identifier.</param>
        /// <param name="isLike">if set to <c>true</c> [is like].</param>
        /// <param name="isFlashTag">if set to <c>true</c> [is flash tag].</param>
        /// <returns></returns>
        public InteractedUsers GetInteractedUsersFromFeed(int userId, int feedId, int imageId, bool isLike, bool isFlashTag, int pageIndex, int pageSize)
        {
            var procName = !isFlashTag ? "GetLikedUsersFromFeed" : "GetLikedOrDislikedUsersFromFlashTag";
            InteractedUsers interactedUsers = new InteractedUsers();
            try
            {
                using (var dbCommand = myDatabase.GetStoredProcCommand(procName))
                {
                    if (isFlashTag)
                    {
                        myDatabase.AddInParameter(dbCommand, "@p_IsLike", DbType.Boolean, isLike);
                        myDatabase.AddInParameter(dbCommand, "@p_ImageId", DbType.Int32, imageId);
                    }
                    myDatabase.AddInParameter(dbCommand, "@p_PageIndex", DbType.Int32, pageIndex);
                    myDatabase.AddInParameter(dbCommand, "@p_PageSize", DbType.Int32, pageSize);
                    myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                    myDatabase.AddInParameter(dbCommand, "@p_FeedId", DbType.Int32, feedId);
                    myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                    var datSet = myDatabase.ExecuteDataSet(dbCommand);

                    if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                    {
                        interactedUsers.ErrorCode = myDatabase.GetParameterValue(dbCommand, "@p_Error").ToString();
                        interactedUsers.ErrorDescription = Helper.GetErrorMessage(interactedUsers.ErrorCode);
                        interactedUsers.ErrorStatus = ResponseStatus.ServerError;
                    }
                    else if (datSet != null)
                    {
                        interactedUsers.UsersList = Helper.GetUSers(datSet.Tables[0]);

                    }
                }
            }
            catch (Exception ex)
            {
                interactedUsers.ErrorStatus = ResponseStatus.ServerError;
                interactedUsers.ErrorDescription = ex.Message;
                Logger.Error(ex);
            }
            return interactedUsers;
        }

        private static void GetComments(DataSet datSet, IList<FeedComment> comments)
        {
            foreach (var comment in from DataRow row in datSet.Tables[0].Rows select new FeedComment
            {
                Id = Convert.ToInt32(row["Id"].GetValueOrNull()),
                CommentedUser = new LoginInfo
                {
                    UserId = Convert.ToInt32(row["UserId"]),
                    FirstName = row["FirstName"].GetValueOrNull(),
                    LastName = row["LastName"].GetValueOrNull(),
                    UserName = row["UserName"].GetValueOrNull(),
                    EmailAddress = row["EmailAddress"].GetValueOrNull(),
                    ProfilePicUrl = string.IsNullOrEmpty(row["ProfilePic"].GetValueOrNull()) ? string.Empty : Path.Combine(Constants.Server_ProfilePicPath, row["ProfilePic"].ToString()),
                    BannerImageUrl = string.IsNullOrEmpty(row["BannerImage"].GetValueOrNull()) ? string.Empty : Path.Combine(Constants.Server_BannerPicPath, row["BannerImage"].ToString()),
                    Location = row["Location"].GetValueOrNull()
                },
                Comment = row["Comment"].GetValueOrNull(),
                CommentedDate = Convert.ToDateTime(row["CommentDate"].GetValueOrNull())
            })
            {
                comments.Add(comment);
            }
        }


        private SearchResult FillAllSearchResults(DataSet dataSet)
        {
            SearchResult searchResult = new SearchResult();
            searchResult.Users = Helper.GetUSers(dataSet.Tables[0]);
            searchResult.Inventory = GetFeedData(dataSet.Tables[1]);
            searchResult.WishList = GetFeedData(dataSet.Tables[2]);
            return searchResult;
        }

        

        private IEnumerable<UserFeedsData> GetFeedData(DataTable dataTable)
        {
            IList<UserFeedsData> userFeedList = new List<UserFeedsData>();
            if (dataTable != null)
                foreach (DataRow row in dataTable.Rows)
                {
                    userFeedList.Add(ParseUserFeeds(row));
                }
            return userFeedList;
        }

        private static UserFeedsData ParseUserFeeds(DataRow feedRow)
        {
            if (feedRow == null) throw new ArgumentNullException("feedRow");
            var userFeeds = new UserFeedsData
            {
                FeedId = Convert.ToInt32(feedRow["FeedId"]),
                Title = feedRow["Title"].ToString(),
                Description = feedRow["Description"].ToString(),
                Location = feedRow["Location"].ToString(),
                Brand = feedRow["Brand"].ToString(),
                Model = feedRow["Model"].ToString(),
                PurchasedFrom = feedRow["PurchasedFrom"].ToString(),
                Serial = feedRow["SerialNumber"].ToString(),
                PurchasePrice = feedRow["Price"].ToString(),
                Warranty = feedRow["Warranty"].ToString(),
                ExpirationDate = !Helper.IsNullOrZero(feedRow["ExpiryDate"])
                    ? Convert.ToDateTime(feedRow["ExpiryDate"])
                    : DateTime.MinValue,
                CreatedTime = Convert.ToDateTime(feedRow["CreatedTime"]),
                UserId = Convert.ToInt32(feedRow["Id"]),
                FirstName = feedRow["FirstName"].ToString(),
                LastName = feedRow["LastName"].ToString(),
                ProfilePic = !string.IsNullOrEmpty(feedRow["ProfilePic"].ToString())
                    ? Path.Combine(Constants.Server_ProfilePicPath, feedRow["ProfilePic"].ToString())
                    : string.Empty,
                Post = (PostType)Convert.ToInt32(feedRow["PostTypeId"]),
                PrivacySetting = (PrivacySettings)Convert.ToInt32(feedRow["PrivacySettingsId"])

            };

            if (feedRow["FeedImages"] != null && feedRow["FeedImages"].ToString().Length > 0)
            {
                var doc = XDocument.Parse(feedRow["FeedImages"].ToString());
                var xElement = doc.Element("FeedImages");
                if (xElement != null)
                    userFeeds.FeedImages = (from images in xElement.Elements("Images")
                                            let receipt = images.Element("IsReceipt")
                                            let url = images.Element("ImageUrl")
                                            where url != null
                                            select new FeedImage
                                            {
                                                ImageId = Convert.ToInt32(images.Element("Id").Value),
                                                ImageUrl = Path.Combine(Constants.Server_FeedPicPath, url.Value),
                                                IsReceipt = receipt != null && (receipt.Value == "True")
                                            }).ToList();
            }
            else
                userFeeds.FeedImages = null;

            if (feedRow["FeedTags"] != null && feedRow["FeedTags"].ToString().Length > 0)
            {
                var doc = XDocument.Parse(feedRow["FeedTags"].ToString());
                var feedTagEle = doc.Element("FeedTags");
                if (feedTagEle != null)
                    userFeeds.FeedTags = (from tags in feedTagEle.Elements("Tag")
                                          let tagElement = tags.Element("Tags")
                                          where tagElement != null
                                          let idElement = tags.Element("Id")
                                          where idElement != null
                                          select new Tag
                                          {
                                              Id = Convert.ToInt32(idElement.Value),
                                              Name = tagElement.Value
                                          }).ToList();
            }
            else
                userFeeds.FeedTags = null;

            if (feedRow.Table.Columns.Contains("FeedComments") && feedRow["FeedComments"] != null && feedRow["FeedComments"].ToString().Length > 0)
            {
                var doc = XDocument.Parse(feedRow["FeedComments"].ToString());
                var feedCommentEle = doc.Element("FeedComments");
                userFeeds.Comments = new List<FeedComment>();
                foreach (var feedComment in feedCommentEle.Elements("Comments"))
                {
                    var comment = new FeedComment();
                    comment.Comment =  feedComment.Element("Comment").Value;
                    comment.CommentedDate = Convert.ToDateTime(feedComment.Element("CommentDate").Value);
                    comment.Id = Convert.ToInt32(feedComment.Element("Id").Value);
                    var profilePic = feedComment.Element("ProfilePic");
                    var bannerPic = feedComment.Element("BannerImage");
                        
                    comment.CommentedUser = new LoginInfo()
                    {
                        UserId = Convert.ToInt32(feedComment.Element("UserId").Value) ,
                        UserName = feedComment.Element("UserName").Value,
                        FirstName = feedComment.Element("FirstName").Value,
                        LastName = feedComment.Element("LastName").Value,
                        Location = feedComment.Element("Location").Value,
                        ProfilePicUrl = profilePic != null ? Path.Combine(Constants.Server_ProfilePicPath, profilePic.Value) : string.Empty,
                        BannerImageUrl = bannerPic != null ? Path.Combine(Constants.Server_BannerPicPath, bannerPic.Value) : string.Empty,
                        EmailAddress = feedComment.Element("EmailAddress").Value

                    };
                    userFeeds.Comments.Add(comment);
                }
            }
            else
                userFeeds.FeedTags = null;


            userFeeds.LikedCount = Convert.ToInt32(feedRow["LikedCount"]);
            userFeeds.CommentCount = Convert.ToInt32(feedRow["CommentCount"]);
            userFeeds.Rating = Convert.ToInt32(feedRow["Rating"]);
            userFeeds.CommentCountByLoggedUser = Convert.ToInt32(feedRow["CommentCountByUser"]);
            userFeeds.IsLikedByLoggedUser = Convert.ToBoolean(feedRow["IsLikedByUser"]);
            userFeeds.RatingByLoggedUser = Convert.ToInt32(feedRow["RatingByUser"]);
            return userFeeds;
        }

        public UserFeedDataList GetUserInventoryByTag(int userId, int tagId)
        {
            UserFeedDataList dataList = new UserFeedDataList();
            List<UserFeedsData> userFeedData = null;
            try
            {
                var dbCommand = myDatabase.GetStoredProcCommand("GetUserInventoryByTagId");

                myDatabase.AddInParameter(dbCommand, "@p_UserId", DbType.Int32, userId);
                myDatabase.AddInParameter(dbCommand, "@p_TagId", DbType.Int32, tagId);

                myDatabase.AddOutParameter(dbCommand, "@p_Error", DbType.Int32, sizeof(int));

                using (var reader = myDatabase.ExecuteReader(dbCommand))
                {
                    userFeedData = new List<UserFeedsData>();
                    while (reader.Read())
                    {
                        var userFeeds = new UserFeedsData();
                        userFeeds.FeedId = Convert.ToInt32(reader["FeedId"]);
                        userFeeds.Title = reader["Title"].ToString();
                        userFeeds.Description = reader["Description"].ToString();
                        userFeeds.Location = reader["Location"].ToString();
                        userFeeds.Brand = reader["Brand"].ToString();
                        userFeeds.Model = reader["Model"].ToString();
                        userFeeds.PurchasedFrom = reader["PurchasedFrom"].ToString();
                        userFeeds.Serial = reader["SerialNumber"].ToString();
                        userFeeds.PurchasePrice = reader["Price"].ToString();
                        userFeeds.Warranty = reader["Warranty"].ToString();
                        userFeeds.ExpirationDate = !Helper.IsNullOrZero(reader["ExpiryDate"]) ? Convert.ToDateTime(reader["ExpiryDate"]) : DateTime.MinValue;
                        userFeeds.CreatedTime = Convert.ToDateTime(reader["CreatedTime"]);
                        userFeeds.UserId = Convert.ToInt32(reader["Id"]);
                        userFeeds.FirstName = reader["FirstName"].ToString();
                        userFeeds.LastName = reader["LastName"].ToString();
                        userFeeds.ProfilePic = !string.IsNullOrEmpty(reader["ProfilePic"].ToString()) ? Path.Combine(Constants.Server_ProfilePicPath, reader["ProfilePic"].ToString()) : string.Empty;
                        userFeeds.PrivacySetting = (PrivacySettings)Convert.ToInt32(reader["PrivacySettingsId"]);
                        userFeeds.Post = (PostType)Convert.ToInt32(reader["PostTypeId"]);

                        if (reader["FeedImages"] != null && reader["FeedImages"].ToString().Length > 0)
                        {
                            var doc = XDocument.Parse(reader["FeedImages"].ToString());
                            var xElement = doc.Element("FeedImages");
                            if (xElement != null)
                                userFeeds.FeedImages = (from images in xElement.Elements("Images")
                                                        let receipt = images.Element("IsReceipt")
                                                        let url = images.Element("ImageUrl")
                                                        where url != null
                                                        select new FeedImage
                                                        {
                                                            ImageId = Convert.ToInt32(images.Element("Id").Value),
                                                            ImageUrl = Path.Combine(Constants.Server_FeedPicPath, url.Value),
                                                            IsReceipt = receipt != null && (receipt.Value == "True")
                                                        }).ToList();
                        }
                        else
                            userFeeds.FeedImages = null;

                        if (reader["FeedTags"] != null && reader["FeedTags"].ToString().Length > 0)
                        {
                            var doc = XDocument.Parse(reader["FeedTags"].ToString());
                            var feedTagEle = doc.Element("FeedTags");
                            if (feedTagEle != null)
                                userFeeds.FeedTags = (from tags in feedTagEle.Elements("Tag")
                                                      let tagElement = tags.Element("Tags")
                                                      where tagElement != null
                                                      let idElement = tags.Element("Id")
                                                      where idElement != null
                                                      select new Tag
                                                      {
                                                          Id = Convert.ToInt32(idElement.Value),
                                                          Name = tagElement.Value
                                                      }).ToList();
                        }
                        else
                            userFeeds.FeedTags = null;

                        userFeeds.LikedCount = Convert.ToInt32(reader["LikedCount"]);
                        userFeeds.CommentCount = Convert.ToInt32(reader["CommentCount"]);
                        userFeeds.Rating = Convert.ToInt32(reader["Rating"]);
                        userFeeds.CommentCountByLoggedUser = Convert.ToInt32(reader["CommentCountByUser"]);
                        userFeeds.IsLikedByLoggedUser = Convert.ToBoolean(reader["IsLikedByUser"]);
                        userFeeds.RatingByLoggedUser = Convert.ToInt32(reader["RatingByUser"]);
                        userFeedData.Add(userFeeds);
                    }
                }
                if (!Helper.IsNullOrZero(myDatabase.GetParameterValue(dbCommand, "@p_Error")))
                {
                    userFeedData = null;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("GetUserFeeds: " + ex);
                userFeedData = null;
                dataList.ErrorCode = Constants.UnExpectedErrorCode.ToString();
                dataList.ErrorDescription = Helper.GetErrorMessage(Constants.UnExpectedErrorCode.ToString());
                dataList.ErrorStatus = ResponseStatus.ServerError;
            }

            dataList.UserFeedsList = userFeedData;
            return dataList;
        }
    }
}