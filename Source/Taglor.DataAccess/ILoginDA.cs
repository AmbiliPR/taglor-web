﻿using Taglor.Data.Model;

namespace Taglor.DataAccess
{
    public interface ILoginDA
    {
        /// <summary>
        ///     Validates the user for logging into Taglor system.
        ///     If Validation fails the userid will be 0.
        /// </summary>
        LoginInfo Login(string emailAddress, string password);

        /// <summary>
        ///     Validates the user which uses Faceboook to login into Taglor.
        ///     Firsttime users will be added to database and corresponding user id shall be returned.
        /// </summary>
        FacebookLoginInfo LoginWithFacebook(string facebookUserId, string emailAddress, string firstName, string lastName,
            string profilePic, string userName);
    }
}